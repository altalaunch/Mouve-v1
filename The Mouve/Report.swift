//
//  Activity.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 8/21/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation
import Parse
import Bond
import SKPhotoBrowser

enum reportKeyEnum:String {
    case userReported = "userReported"
    case activityReported = "activityReported"
    case mouveReported = "mouveReported"
}

class Report: PFObject {
    
    @NSManaged var fromUser: PFEUser
    @NSManaged var onActivity: Activity?
    @NSManaged var onMouve: Event?
    @NSManaged var reportKey: String
    @NSManaged var toUser: PFEUser?
    
    override init(){
        super.init()
    }
    init(type: reportKeyEnum, toUser: PFEUser, onMouve: Event, onActivity: Activity) {
        super.init(className: "Report")
    
        self.fromUser = PFEUser.currentUser()!
        self.toUser = toUser
        self.onActivity = onActivity
        self.onMouve = onMouve
    }
    
    override class func query() -> PFQuery? {
        //1
        let query = PFQuery(className: Report.parseClassName())
        //2
        query.includeKey("fromUser")
        //3
        query.orderByAscending("createdAt")
        return query
    }
}



extension Report: PFSubclassing{
    //setting the type key as enum
    var type:reportKeyEnum? {
        
        get { return self["typeKey"] != nil ? reportKeyEnum(rawValue: self["typeKey"] as! String) : nil }
        
        set { return self["typeKey"] = newValue?.rawValue }
        
    }
    //1
    class func parseClassName() -> String {
        return "Report"
    }
    
    //2
    override class func initialize() {
        var onceToken: dispatch_once_t = 0
        dispatch_once(&onceToken) {
            self.registerSubclass()
        }
    }
}