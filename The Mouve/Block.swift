//
//  Activity.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 8/21/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation
import Parse
import Bond

class Block: PFObject {
    
    @NSManaged var fromUser: PFEUser
    @NSManaged var toUser: PFEUser?
    
    override init(){
        super.init()
    }
    init(toUser: PFEUser) {
        super.init(className: "Block")
    
        self.fromUser = PFEUser.currentUser()!
        self.toUser = toUser
    }
    
    override class func query() -> PFQuery? {
        //1
        let query = PFQuery(className: Block.parseClassName())
        //2
        query.includeKey("fromUser")
        //3
        query.orderByAscending("createdAt")
        return query
    }
}



extension Block: PFSubclassing{

    //1
    class func parseClassName() -> String {
        return "Block"
    }
    
    //2
    override class func initialize() {
        var onceToken: dispatch_once_t = 0
        dispatch_once(&onceToken) {
            self.registerSubclass()
        }
    }
}