//
//  AppDelegate.swift
//  The Mouve
//
//  Created by HILAL HABASHI on 4/12/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import Parse
import ParseFacebookUtilsV4
import ParseTwitterUtils
import Bolts
import Fabric
import SVProgressHUD
import Crashlytics
import IQKeyboardManagerSwift
import FBSDKCoreKit
import Reachability

let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UITabBarControllerDelegate {
    var window: UIWindow?
    var location = Location()
    let placeHolderBg = UIImage(named: "addImage.png")
    let dummyProfPic = UIImage(named: "dummyProfile.png")
    let dummyEventPic = UIImage(named: "dummyMouve.png")
    let gpaAPIKey = "AIzaSyBhSL-vMg7MfAe4kXukMDlGy-nv8UMlOno"
    var currentConfig: PFConfig?
    var hostReach:Reachability? = nil

    func setupLDS(){
        PFEUser.initialize()
        Parse.enableLocalDatastore()
    }
    func setupParse(launchOptions: [NSObject: AnyObject]?){
        Parse.setApplicationId("h5smZAkhhQ8MYAFJY3P4U9rFs6kjz0MLSurD76tL",
            clientKey: "8A4eqYwOATOIxM634S2Hf3oTVvbbjnUYclcFjhrT")
        PFTwitterUtils.initializeWithConsumerKey("wDvj5z6dRIMBJImmkJq6qao7J",  consumerSecret:"noU2DCn6ACTZiwP0xl66VZUT2bIDGJkGZDoKR3lfn7AgRixhEs")
        PFFacebookUtils.initializeFacebookWithApplicationLaunchOptions(launchOptions)
    }
    
    func application(application: UIApplication, willFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool {
        setupLDS()
        return true
    }
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        self.setupParse(launchOptions)
        self.registerApplicationForPushNotifications(application)
        self.trackLaunchFromPush(application, launchOptions:launchOptions)
        changeNavBar()
        IQKeyboardManager.sharedManager().enable = true


//        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    func registerApplicationForPushNotifications(application: UIApplication) {
        // Set up push notifications
        // For more information about Push, check out:
        // https://developer.layer.com/docs/guides/ios#push-notification
        
        // Register device for iOS8
        let notificationSettings: UIUserNotificationSettings = UIUserNotificationSettings(forTypes: [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound], categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
        application.registerForRemoteNotifications()
    }

    func trackLaunchFromPush(application: UIApplication,launchOptions: [NSObject: AnyObject]?){
        if application.applicationState != UIApplicationState.Background {
            // Track an app open here if we launch with a push, unless
            // "content_available" was used to trigger a background push (introduced
            // in iOS 7). In that case, we skip tracking here to avoid double
            // counting the app-open.
            let oldPushHandlerOnly = !self.respondsToSelector(Selector("application:didReceiveRemoteNotification:fetchCompletionHandler:"))
            let noPushPayload: AnyObject? = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey]
            if oldPushHandlerOnly || noPushPayload != nil {
                PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
            }
        }
    }
    func monitorReachability() {
//        self.hostReach = Reachability.reachabilityForInternetConnection()
//        hostReach.reachableBlock = {
//            (let reach: Reachability!) -> Void in
//            LocalMessage.observe(kReachabilityChangedNotification, classFunction: "reachabilityChanged:", inClass: self)
//        }
    
        
//        hostReach.reachableBlock = ^(Reachability*reach) {
//        _networkStatus = [reach currentReachabilityStatus];
        
//        if ([self isParseReachable] && [PFUser currentUser] && self.homeViewController.objects.count == 0) {
        // Refresh home timeline on network restoration. Takes care of a freshly installed app that failed to load the main timeline under bad network conditions.
        // In this case, they'd see the empty timeline placeholder and have no way of refreshing the timeline unless they followed someone.
//        [self.homeViewController loadObjects];
    }

    func reachabilityChanged(notification: NSNotification) {
        if (self.hostReach!.isReachableViaWiFi() || self.hostReach!.isReachableViaWWAN()) {
            print("Service avalaible!!!")
        } else {
            print("No service avalaible!!!")
        }
    }

    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        print("didRegisterForRemoteNotificationsWithDeviceToken")
        
        let currentInstallation = PFInstallation.currentInstallation()
        currentInstallation.channels = ["invites", "attendings", "follows", "comments", "global"]
        currentInstallation.setDeviceTokenFromData(deviceToken)
        currentInstallation.saveInBackground()
    }
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        if (error.code != 3010) {
            print("failed to register for remote notifications:  (error)\(error.localizedDescription)")
        }
        else if(error.code == 3010){
            print("Simulator doesn't support push")
        }
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
      print("didReceiveRemoteNotification")
        if application.applicationState == .Inactive  {
            // The application was just brought from the background to the foreground,
            // so we consider the app as having been "opened by a push notification."
            PFAnalytics.trackAppOpenedWithRemoteNotificationPayload(userInfo)
            NSNotificationCenter.defaultCenter().postNotificationName("pushNotification", object: nil, userInfo: userInfo)
        }
        PFPush.handlePush(userInfo)
        NSNotificationCenter.defaultCenter().postNotificationName("pushNotification", object: nil, userInfo: userInfo)
        
    }
    

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        IQKeyboardManager.sharedManager().enable = true
        FBSDKAppEvents.activateApp()
//        Call location check
//        FBAppCall.handleDidBecomeActiveWithSession(PFFacebookUtils.session())
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

extension AppDelegate {
    func deleteCurrentUser(){
        if((PFUser.currentUser()) != nil){
            PFUser.currentUser()?.deleteInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                appDel.logOut()
            })
        }
    }
    func segueToMain(){
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window!.rootViewController = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateInitialViewController()
        window!.makeKeyAndVisible()
    }
    
    func changeNavBar() {
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName : UIFont(name: "HalisGR-Regular", size: 14)!, NSForegroundColorAttributeName : UIColor.blackColor()]
        
        // Added code here to change Tab Bar Tint & Text Colour. Delete to restore default settings.
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.darkGrayColor() ], forState: .Selected)
        
        UITabBar.appearance().tintColor = UIColor.seaFoamGreen()
        
    }
   	
    
    func checkLogin() {
        if(!PFUser.currentUser().isNil){
        PFUser.currentUser()?.fetchIfNeededInBackgroundWithBlock({ (object:PFObject?, error:NSError?) -> Void in
            self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
//            Step 1*: Fetch user from LocalDataStore in case it doesn't appear to exist.
            if (object != nil){
//                Step 1: Check whether session is valid
//                Step 2: Check whether current user is finished or missing important details -- IF missing delete user
                    if(PFUser.currentUser()!.email == nil){
                        self.deleteCurrentUser()
                    }else{
                        //                Step 3: Pull latest config and set crash report details
                        PFConfig.getConfigInBackgroundWithBlock({ (config: PFConfig?, error: NSError?) -> Void in
                            if((error) != nil){
                                print(error!.userInfo)
                            }
                            else{
                                self.currentConfig = PFConfig.currentConfig()
                                PFInstallation.currentInstallation()["user"] = PFUser.currentUser()
                                PFInstallation.currentInstallation().saveInBackground()
                                Crashlytics.sharedInstance().setUserEmail(PFUser.currentUser()?.email)
                                Crashlytics.sharedInstance().setUserIdentifier(PFUser.currentUser()?.objectId)
                                Crashlytics.sharedInstance().setUserName(PFUser.currentUser()?.username)
                            }
                        })
                        //                Step 4: Mouve to main screen upon success
                        self.window!.rootViewController = (UIStoryboard(name: "Main"
                            , bundle:
                            NSBundle.mainBundle()).instantiateInitialViewController()) as UIViewController?
                        self.window!.makeKeyAndVisible()
                    }
            }else{
//                *In case of some object exists but is corrupted
                appDel.logOut()
            }
        })
        }else{
            //                Else: Go to Login Screen
            self.window!.rootViewController = (UIStoryboard(name: "Login", bundle:
                NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("LoginFlow")
//                NSBundle.mainBundle()).instantiateInitialViewController()) as UIViewController?
                )
            self.window!.makeKeyAndVisible()
        }
//        })
//        window!.rootViewController = (UIStoryboard(name: loggedIn ? "Main" : "Login", bundle: NSBundle.mainBundle()).instantiateInitialViewController()) as UIViewController?
//        window!.makeKeyAndVisible()
    }
    func newLocation(){
        print("You're now located at", terminator: "")
    }
    func logOut() {

//        [[PAPCache sharedCache] clear];
        
        // clear NSUserDefaults
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kPAPUserDefaultsCacheFacebookFriendsKey];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kPAPUserDefaultsActivityFeedViewControllerLastRefreshKey];
//        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // Unsubscribe from push notifications by removing the user association from the current installation.
        PFInstallation.currentInstallation().removeObjectForKey("user")
        PFInstallation.currentInstallation().saveInBackground()
//        PFInstallation.currentInstallation().deleteInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
        PFQuery.clearAllCachedResults()
        PFUser.logOutInBackground()
        ParseLoginUtility.deleteFromKeychain()
        self.checkLogin()
//        }

        
        // Clear all caches

    }
}
import Foundation

class SimulatorUtility
{
    class var isRunningSimulator: Bool
        {
        get
    {
        return TARGET_OS_SIMULATOR != 0 // Use this line in Xcode 7 or newer
        }
    }
}