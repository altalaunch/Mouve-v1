//
//  PFEUser.swift
//  The Mouve
//
//  Created by Hilal Habashi on 9/27/15.
//  Copyright © 2015 The Mouve. All rights reserved.
//

import Foundation
import Parse
import Bond

public class PFEUser: PFUser {
    // MARK: PFUser Subclassing
    var followers: Observable<[PFEUser]?> = Observable(nil)
    var blocked: Observable<[PFEUser]?> = Observable(nil)
    var blockingMe: Observable<[PFEUser]?> = Observable(nil)
    var profileImg: Observable<UIImage?> = Observable(nil)
    var thumbProfImg: Observable<UIImage?> = Observable(nil)
    @NSManaged var fullName: String
    @NSManaged var profileImage: PFFile?
    @NSManaged var thumbnailImg: PFFile?
    @NSManaged var numOfFollowers: Int
    @NSManaged var numOfFollowing: Int
    @NSManaged var numOfEvents: Int
    @NSManaged var fbId: String?
    @NSManaged var disabled: Bool
    
    override public class func initialize() {
        struct Static {
            static var onceToken : dispatch_once_t = 0;
        }
        
        dispatch_once(&Static.onceToken) {
            self.registerSubclass()
        }
    }
    

    func dlPfImg(){
        self.fetchIfNeededInBackgroundWithBlock { (result: PFObject?, error: NSError?) -> Void in
            if let user = result as? PFEUser{
//        let user = self
                ParseUtility.getImgFile(user.thumbnailImg){(img: UIImage?,error: NSError?) in
                    if(((error) != nil) || (img == nil)){
                        user.thumbProfImg.value = appDel.dummyProfPic!
                    }
                    else{
//                        print("Download Prof Pic for \(self.username)", terminator: "")
                        user.thumbProfImg.value = img!
                    }
                }
            }
        }
    }
    func dlFullPfImg(){
        self.fetchIfNeededInBackgroundWithBlock { (result: PFObject?, error: NSError?) -> Void in
            if let user = result as? PFEUser{
                //        let user = self
                ParseUtility.getImgFile(user.profileImage){(img: UIImage?,error: NSError?) in
                    if(((error) != nil) || (img == nil)){
                        user.profileImg.value = appDel.dummyProfPic!
                    }
                    else{
//                        print("Download Prof Pic for \(self.username)", terminator: "")
                        user.profileImg.value = img!
                    }
                }
            }
        }
    }

    override public class func query() -> PFQuery? {
        //1
        let query = PFQuery(className: PFEUser.parseClassName())
        //3
//        query.orderByDescending("username")
        return query
    }

    func fetchFollowing() {
                if (followers.value != nil) {
                    return
                }
                ParseUtility.queryActivities(toUser: self,activityType: typeKeyEnum.Follow){ (var results: [PFObject]?, error: NSError?) -> Void in
                    results = results?.filter { result in result["fromUser"] != nil }
                    
                    // 4
                    self.followers.value = results?.map { result in
                        let fromUser = result["fromUser"] as! PFEUser
                        return fromUser
                    }
                }
    }
    
    func isFollowedByMe() -> Bool {
        if let followers = followers.value {
            return followers.contains(PFEUser.currentUser()!)
        } else {
            return false
        }
    }
    func isFollowingMe() -> Bool {
        if let followers = PFEUser.currentUser()!.followers.value {
            return followers.contains(self)
        } else {
            return false
        }
    }
    func toggleFollowUser() {
        if (isFollowedByMe()) {
            // if image is liked, unlike it now
            // 1
            ParseUtility.deleteActivityInBackground(toUser: self,activityType: .Follow, onCompletion: { (succeeded, error) -> () in
                if(succeeded){
                    self.followers.value = self.followers.value?.filter { $0 != PFEUser.currentUser()! }
                }
            })
        } else {
            // if this image is not liked yet, like it now
            // 2
            
            ParseUtility.createActivityInBackground(toUser:self, activityType: .Follow, onCompletion: { (succeeded, error) -> () in
                if(succeeded){
                    self.followers.value?.append(PFEUser.currentUser()!)
                }
            })
        }
    }

    
    func fetchBlocked() {
//        if (blocked.value != nil) {
//            return
//        }
        ParseUtility.queryBlocking(PFEUser.currentUser()!) { (var results: [PFObject]?, error: NSError?) -> Void in
            results = results?.filter { result in result["toUser"] != nil }
            
            // 4
            self.blocked.value = results?.map { result in
                let toUser = result["toUser"] as! PFEUser
                return toUser
            }
        }
    }
    
    func fetchBlockingMe(){
//        if (blockingMe.value != nil) {
//            return
//        }
        ParseUtility.queryBlocking(toUser: PFEUser.currentUser()!) { (var results: [PFObject]?, error: NSError?) -> Void in
            results = results?.filter { result in result["fromUser"] != nil }
            
            // 4
            self.blockingMe.value = results?.map { result in
                let fromUser = result["fromUser"] as! PFEUser
                return fromUser
            }
        }
        
    }
    
    func isBlockedByMe() -> Bool {
        if let blocked = PFEUser.currentUser()!.blocked.value {
            return blocked.contains(self)
        } else {
            return false
        }
    }
    
    func isBlockingMe() -> Bool {
        if let blockingMe = PFEUser.currentUser()!.blockingMe.value {
            return blockingMe.contains(self)
        } else {
            return false
        }
    }
    
    func toggleBlockUser() {
        if (isBlockedByMe()) {
            // if image is liked, unlike it now
            // 1
            ParseUtility.deleteBlockInBackground(toUser: self, onCompletion: { (succeeded, error) -> () in
                if(succeeded){
                    PFEUser.currentUser()!.blocked.value = PFEUser.currentUser()!.blocked.value?.filter { $0 != self }
                }
            })
            

        } else {
            // if this image is not liked yet, like it now
            // 2

            ParseUtility.createBlockInBackground(toUser: self, onCompletion: { (succeeded, error) -> () in
                if(succeeded){
                    PFEUser.currentUser()!.blocked.value?.append(self)
                }
            })
            
        }
    }
    
}
