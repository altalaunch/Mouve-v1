//
//  HomeVC.swift
//  The Mouve
//
//  Created by Hilal Habashi on 12/30/15.
//  Copyright © 2015 The Mouve. All rights reserved.
//


import UIKit
import SVProgressHUD

class HomeVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        appDel.checkLogin()
    }
}
