//
//  DetailPostTableViewCell.swift
//  The Mouve
//
//  Created by Hilal Habashi on 5/4/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import FontAwesome
import Bond
import Parse

class DetailPostTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var attributedLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var timeCreatedlabel: UILabel!
    
    var posterDisposable: DisposableType?
    var deleteBtnDisposable: DisposableType?
    var eventDel: DetailPostTableViewCellDelegate?
    var delegate: DetailPostTableViewCellDelegate?
    var activity: Activity! {
        didSet {
            posterDisposable?.dispose()
            deleteBtnDisposable?.dispose()
            if let oldValue = oldValue where oldValue != activity{
                oldValue.profilePic.value = nil
            }
            self.setActivity()
        }
    }
    
    func setActivity(){
        attributedLabel.text = activity.stringContent
        usernameLabel.text = activity.fromUser.username!
        timeCreatedlabel.text = NSDate().offsetFrom(self.activity!.createdAt!)
        posterDisposable = profileImageView.bindAndRound(activity.profilePic)
        self.attributedLabel!.numberOfLines = 0
        self.attributedLabel!.lineBreakMode = NSLineBreakMode.ByWordWrapping
    }
    
}


public enum collectType: String {
    case ViewPic
    case WatchVid
    case None
}

class DetailMediaCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mediaImageView: UIImageView!
    
    var thumbnailDisposable: DisposableType?
    var mediaType:collectType!
    var media: Activity! {
        didSet {
            thumbnailDisposable?.dispose()
            if let oldValue = oldValue where oldValue != media{
                oldValue.thumbnailImg.value = nil
                mediaType = collectType.None
            }
            if let media = media {
                print("\(media.fromUser.username!)")
                setThumbnail()
            }
        }
    }
    
    func setThumbnail(){
        if(self.media != nil){
            if(uploadIsImage(media.mediaFile!)){
                thumbnailDisposable = media.thumbnailImg.bindTo(mediaImageView.bnd_image)
                mediaType = collectType.ViewPic
                mediaImageView.subviews.forEach({ $0.removeFromSuperview() })
            }
            else{
                let thumbLabel = UILabel()
                let imageView = UIImageView()
                thumbLabel.font = UIFont.fontAwesomeOfSize(50)
                thumbLabel.text = String.fontAwesomeIconWithCode("fa-play-circle")
                thumbLabel.textColor = UIColor.whiteColor()
                imageView.image = UIImage(named: "addImage.png")

                thumbnailDisposable = self.mediaImageView.bindAndBlur(media.thumbnailImg,label: thumbLabel)
                mediaType = collectType.WatchVid
//                self.mediaImageView.addSubview(thumbLabel)
//                self.mediaImageView.bringSubviewToFront(thumbLabel)
            }
        }
    }
}

protocol DetailPostTableViewCellDelegate {
    func belongsToUser(cell: DetailPostTableViewCell) -> Bool
//    func didTapDeletePost(cell: DetailPostTableViewCell)

}


