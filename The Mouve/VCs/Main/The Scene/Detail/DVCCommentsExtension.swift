//
//  DVCCommentsExtension.swift
//  The Mouve
//
//  Created by Hilal Habashi on 9/22/15.
//  Copyright © 2015 The Mouve. All rights reserved.
//

import Foundation
import Parse
import SVProgressHUD


extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    

    func belongsToUser(cell: DetailPostTableViewCell) -> Bool{
        if(cell.activity.fromUser == PFEUser.currentUser()){
            return true
        }
        else{
            return false
        }
    }
    
    func deletePost(cell: DetailPostTableViewCell){
        SVProgressHUD.show()
        
        let query = Activity.query()
        query!.whereKey("objectId", equalTo:cell.activity!.objectId!)
        query!.findObjectsInBackgroundWithBlock {
            (objects:[PFObject]?, error:NSError?) -> Void in
            SVProgressHUD.dismiss()
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) objects.", terminator: "")
                // Do something with the found objects
                if let objects = objects {
                    for object  in objects {
                        let parseObj = object
                        parseObj.deleteInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
                            if (success) {
                                self.setEventData()
                            } else {
                                
                            }
                        }
                    }
                }
            }else {
                print("\(error)", terminator: "")
            }
        }
    }
    
    func reportComment(cell: DetailPostTableViewCell){
        SVProgressHUD.show()
        
        let existingReportQuery = Report.query()!
        existingReportQuery.whereKey("fromUser", equalTo: PFEUser.currentUser()!)
        existingReportQuery.whereKey("onActivity", equalTo: cell.activity!)
        existingReportQuery.findObjectsInBackgroundWithBlock{ (data: [PFObject]?, error: NSError?) -> Void in
            let reports = data as? [Report] ?? []
            if(reports.count == 0){
                
                let report = Report()
                report.fromUser = PFEUser.currentUser()!
                report.onActivity = cell.activity
                report.reportKey = "activityReported"
                
                let reportACL = PFACL(user: PFEUser.currentUser()!)
                reportACL.publicReadAccess = true
                report.ACL = reportACL;
                
//                activityACL.publicWriteAccess = false
//                activityACL.setWriteAccess(true, forUser: PFEUser.currentUser()!)
//                activityACL.setWriteAccess(true, forRoleWithName: "admins")
//                activityACL.setReadAccess(false, forRoleWithName: "blocked_\(PFEUser.currentUser()!.objectId)")
                
                
                
                report.saveInBackgroundWithBlock{ (succeeded: Bool, error: NSError?) -> Void in
                    if let error = error {
                        SVProgressHUD.dismiss()
                        
                        let errorString = error.userInfo["error"] as? NSString
                        self.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
                        
                    }else if (succeeded){
                        // end spinner
                        SVProgressHUD.dismiss()
                        self.presentViewController(UIAlertController(title: "Thank You!", message: "A member of our editorial team will take a look at this comment."), animated: true, completion: nil)
                        
                    }
                }
                
            }else{
                SVProgressHUD.dismiss()
                self.presentViewController(UIAlertController(title: "Thank You!", message: "Thanks for reporting. You have already reported this comment and a member of our editorial team is on it."), animated: true, completion: nil)
                
            }
        }
    }
    


    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellID") as? DetailPostTableViewCell
        let data = self.feedComponent.content[indexPath.section]
        data.dlPfImg()
        cell!.activity = data
//        cell!.eventDel = self
//        cell!.delegate = self

        return cell!
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellID") as? DetailPostTableViewCell
        let data = self.feedComponent.content[indexPath.section]
        cell!.activity = data

        
        if (cell!.activity.fromUser == PFEUser.currentUser()){
            let deleteAction = UITableViewRowAction(style: .Default, title: "Delete") { (action: UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
                
                let deleteAlert = UIAlertController(title: "Delete Comment?", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
                deleteAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
                    self.deletePost(cell!)
                    
                }))
                
                deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
                    deleteAlert.dismissViewControllerAnimated(true, completion: nil)

                }))
                self.presentViewController(deleteAlert, animated: true, completion: nil)
                
            }
            return [deleteAction]
        }else{
            let reportAction = UITableViewRowAction(style: .Normal, title: "Report") { (action: UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
                self.reportComment(cell!)
                
            }
            reportAction.backgroundColor = UIColor.seaFoamGreen()
            return [reportAction]
        }
        
    }

    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {

            // update some UI

        return self.feedComponent.content.count
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        feedComponent.targetWillDisplayEntry(indexPath.section)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return 1
    }
}
