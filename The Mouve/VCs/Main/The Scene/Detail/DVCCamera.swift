//
//  DVCCamera.swift
//  The Mouve
//
//  Created by Hilal Habashi on 9/24/15.
//  Copyright © 2015 The Mouve. All rights reserved.
//

import Foundation
import MobileCoreServices
import AVFoundation
import UIKit
import Parse

extension DetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    func generateThumbnail(videoUrl: NSURL) -> UIImage{

        var uiImage: UIImage = appDel.placeHolderBg!
        let asset = AVURLAsset(URL: videoUrl, options: nil)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        do{
            let midtime = Int(asset.duration.seconds / 2.0)
            let cgImage = try imgGenerator.copyCGImageAtTime(CMTimeMake(Int64(midtime-1), Int32(midtime)), actualTime: nil)
            uiImage = UIImage(CGImage: cgImage)
        }
        catch {
            
            print("Oops. Couldn't generate a thumbnail for your video")
        }
        return uiImage
        
    }
    
    @IBAction func addMediaAction(sender: AnyObject)
    {
        
        if(dateNow.compare(self.event.endTime) == .OrderedAscending){
            imagePicker = UIImagePickerController()
            imagePicker!.delegate = self
            print("Popped up..")
            let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
            
            
            let galleryAction = UIAlertAction(title: "Choose from Gallery", style: UIAlertActionStyle.Default){
                UIAlertAction in
                self.openGallery()
            }
            let cameraAction = UIAlertAction(title: "Take a Photo or Video", style: UIAlertActionStyle.Default){
                UIAlertAction in
                self.openCamera()
                
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel){
                UIAlertAction in
            }
            // Add the actions
            alert.addAction(cameraAction)
            alert.addAction(galleryAction)
            alert.addAction(cancelAction)
            // Present the actionsheet
            if UIDevice.currentDevice().userInterfaceIdiom == .Phone
            {
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                popoverMenu=UIPopoverController(contentViewController: alert)
                popoverMenu!.presentPopoverFromRect(feedCollectionView!.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
            }
        }else{
            self.presentViewController(UIAlertController(title: "Mouve Ended", message: "You cannot add photos or videos to a mouve that has ended."), animated: true, completion: nil)
        }
        
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            imagePicker!.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker!.mediaTypes = [kUTTypeMovie as String,kUTTypeImage as String]
            imagePicker!.videoMaximumDuration = 11.0
            imagePicker?.videoQuality = .TypeLow
            imagePicker!.allowsEditing = true
            imagePicker!.delegate = self
            self .presentViewController(imagePicker!, animated: true, completion: nil)
        }
        else
        {
            openGallery()
        }
    }
    func openGallery()
    {
        imagePicker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imagePicker!.mediaTypes = [kUTTypeMovie as String,kUTTypeImage as String]
        imagePicker!.delegate = self
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(imagePicker!, animated: true, completion: nil)
        }
        else
        {
            popoverMenu=UIPopoverController(contentViewController: imagePicker!)
            popoverMenu!.presentPopoverFromRect(feedCollectionView!.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        }
    }
    func imagePickerController(imagePicker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        //        If media is an image...
        if(mediaType.isEqualToString(kUTTypeImage as String))
        {
            self.pickedPic.value = ImageCropper.squareCropImageToSideLength((info[UIImagePickerControllerOriginalImage] as? UIImage)!, toSize: 1000)
            newMediaFile = PFFile(name: "attached.jpg", data:UIImageJPEGRepresentation(self.pickedPic.value!, CGFloat(0.7))!)
            let thumbnail = ImageCropper.squareCropImageToSideLength((info[UIImagePickerControllerOriginalImage] as? UIImage)!, toSize: 100)
            thumbnailFile = PFFile(name: "thumbnail.jpg", data:UIImageJPEGRepresentation(thumbnail, CGFloat(0.3))!)
            //        If media is a video...
        }else if(mediaType.isEqualToString(kUTTypeMovie as String)){
            let url:NSURL = info[UIImagePickerControllerMediaURL] as! NSURL
            //            let videoFilePath: String = url.path!
            let fileData:NSData = NSData(contentsOfURL: url)!
            //                NSData.dataWithContentsOfMappedFile(videoFilePath) as! NSData
            self.pickedPic.value = generateThumbnail(url)
            
            newMediaFile = PFFile(name:"attached.mov", data: fileData)
            let thumbnail = ImageCropper.squareCropImageToSideLength(self.pickedPic.value!, toSize: 100)
            thumbnailFile = PFFile(name: "thumbnail.jpg", data:UIImageJPEGRepresentation(thumbnail, CGFloat(0.3))!)
        }
    }
    func imagePickerControllerDidCancel(imagePicker: UIImagePickerController)
    {
        imagePicker .dismissViewControllerAnimated(true, completion: nil)
        print("picker cancel.")
    }

    
}