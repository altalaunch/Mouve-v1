//
//  DetailViewController.swift
//  The Mouve
//
//  Created by Hilal Habashi on 4/15/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit


import Parse
import MapKit
import Bond
import SVProgressHUD
import SwiftDate
import SKPhotoBrowser
import MWPhotoBrowser




class DetailViewController: UIViewController, EditObjectDelegate, FeedComponentTarget {
    @IBOutlet weak var feedTableView: UITableView?
    @IBOutlet weak var feedCollectionView: UICollectionView?
    @IBOutlet weak var tableViewHeaderView: UIView!
    @IBOutlet weak var editButton: smallTextOutlinedButton!
    @IBOutlet weak var attendButton: GreyGreenButton!
    @IBOutlet weak var inviteButton: smallTextOutlinedButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var eventNameLabel: UILabel! // Old name label
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addressButton: UIButton!
    @IBOutlet weak var attendingList: UIButton!
    @IBOutlet weak var addPostBtn: UIButton!
    @IBOutlet weak var postTableView: UITableView!
    @IBOutlet weak var mouveNameLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var attendToPostLabel: UILabel!
    @IBOutlet weak var headerImageView: UIImageView?
    @IBOutlet weak var creatorPfPicture: UIImageView!
    
    
    var event: Event!
    let defaultRange = 0...4
    let additionalRangeSize = 2
    var type: SceneType!
    var photos: [MWPhoto]?
    var feedComponent: FeedComponent<Activity, DetailViewController>!
    var mediaFeedComponent: FeedComponent<Activity, DetailViewController>!
    var pickedPic:Observable<UIImage?> = Observable(nil)
    var attendDisposable: DisposableType?
    var invitesDisposable: DisposableType?
    var delegate: EditObjectDelegate?
    let offset_HeaderStop:CGFloat = 180 // At this offset the Header stops its transformations
    var popoverMenu: UIPopoverController?=nil
    var imagePicker: UIImagePickerController?=UIImagePickerController()
    var newMediaFile: PFFile?
    var thumbnailFile: PFFile?
    var dateNow = NSDate()
    var dayLabel:String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.postTableView.estimatedRowHeight = 70
        self.postTableView.rowHeight = UITableViewAutomaticDimension
                
        feedComponent = FeedComponent(target: self, feedMode: FeedMode.RefreshableTable)
        mediaFeedComponent = FeedComponent(target: self, feedMode: FeedMode.RefreshableCollection)
        styleViewProgrammatically()
        addNavControllerLikePan()
        headerImageView?.userInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("mainImageWasTapped:"))
        headerImageView?.addGestureRecognizer(tapGestureRecognizer)
        self.pickedPic.value = appDel.placeHolderBg
        backButton.tintColor = UIColor.whiteColor()
        backButton.bringSubviewToFront(self.view)
        UIApplication.sharedApplication().statusBarStyle = .Default
        self.tabBarController?.hidesBottomBarWhenPushed = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
        self.tabBarController?.hidesBottomBarWhenPushed = true
        isDeleted()
        if(DetailViewController.ShouldRefresh.refresh == true){
            setEventData()
        }else{
            DetailViewController.ShouldRefresh.refresh = true
        }

    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = false
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
    }
    
    func setEventData(){
        setDayLabel()
        mouveNameLabel.text = event!.name
        descriptionLabel.text = event!.about
        timeLabel.text = " \(dayLabel) | \(event!.startTime.toShortTimeString()) - \(event!.endTime.toShortTimeString())"
        event.thumbBgImg.bindTo(headerImageView!.bnd_image)
        headerImageView!.clipsToBounds = true
        headerImageView!.contentMode = .ScaleAspectFill
        creatorPfPicture.bindAndRound(event!.creator.thumbProfImg)
        creatorPfPicture.layer.borderWidth = 1
        creatorPfPicture.layer.borderColor = UIColor.seaFoamGreen().CGColor
        event.fetchAttendees()
        addressButton.setTitle(event!.address, forState: .Normal)
        addressButton.titleLabel!.numberOfLines = 0 // Dynamic number of lines
        addressButton.titleLabel!.lineBreakMode = NSLineBreakMode.ByWordWrapping
        addressButton.addTarget(self, action: "openMapForPlace:", forControlEvents: UIControlEvents.TouchUpInside)
        attendingList.setTitle(((event!.invitees.value == nil) ? "Nobody invited" : "\(event?.invitees.value?.count) invited"), forState: .Normal)
        attendDisposable = event.attendees.observe { (value: [PFEUser]?) -> () in
            // 3
            if let value = value {
                self.attendButton.completed = value.contains(PFEUser.currentUser()!)
                self.attendingList.setTitle("\(self.event.attendees.value!.count) Attending", forState: UIControlState.Normal)
            }
            else{
                self.attendingList.setTitle("\(self.event.attendees.value?.count) Attending", forState: UIControlState.Normal)
                self.attendButton.completed = false
            }
        }
        
        if(event.privacy && event.creator == PFEUser.currentUser()){
            //Hiding the button for now since "Invite more" feature is disabled
            inviteButton.hidden = false
        }
        else{
            inviteButton.hidden = true
            inviteButton.userInteractionEnabled = false
        }
        
        if(dateNow.compare(event.endTime) == .OrderedAscending){
        
            if((event.isAttending(PFEUser.currentUser()!)) || (event.creator == PFEUser.currentUser()!)){
                attendToPostLabel.hidden = true
                attendButton.setTitle("Attending", forState: UIControlState.Normal)
            } else{
                addPostBtn.hidden = true
                addPostBtn.userInteractionEnabled = false
                attendButton.setTitle("Attend", forState: UIControlState.Normal)

            }
        }else{
            addPostBtn.hidden = true
            addPostBtn.userInteractionEnabled = false
            attendToPostLabel.text = "Mouve ended, cannot add posts"
            
            if(event.isAttending(PFEUser.currentUser()!)){
                attendButton.setTitle("Attended", forState: UIControlState.Normal)
            }else{
                attendButton.setTitle("Attend", forState: UIControlState.Normal)
            }
        }
        
        feedComponent.loadInitialIfRequired()
        feedComponent.refresh(self)
        mediaFeedComponent.loadInitialIfRequired()
        mediaFeedComponent.refresh(self)
    }
    
    func setDayLabel(){
        if (event.startTime.isInToday()){
            dayLabel = "Today"
            if(event.endTime.isInTomorrow()){
                dayLabel+=" - Tomorrow"
            }
        }
        else if(event.startTime.isInTomorrow()){
            dayLabel = "Tomorrow"
        }
        else if(event.startTime.isInYesterday()){
            dayLabel = "Yesterday"
            if(event.endTime.isInToday()){
                dayLabel+=" - Today"
            }
        }
        else{
            var yearFormat=""
            if(NSDate().year != event.endTime.year){
                yearFormat="YY\''"
            }
            if(event.startTime.day == event.endTime.day){
                dayLabel = "\(event.startTime.toString(DateFormat.Custom("MMM dd "+yearFormat))!)"
            }
            else if(event.startTime.month != event.endTime.month){
                dayLabel = "\(event.startTime.toString(DateFormat.Custom("MMM dd"))!) - \(event.endTime.toString(DateFormat.Custom("MMM dd "+yearFormat))!)"
            }
            else{
                dayLabel = "\(event.startTime.toString(DateFormat.Custom("MMM dd"))!)-\(event.endTime.toString(DateFormat.Custom("dd "+yearFormat))!)"
            }
            
        }
    }
    
    func loadInRange(range: Range<Int>,feedMode: FeedMode, completionBlock: ([Activity]?) -> Void) {
        // 1
        switch(feedMode){
        case .EndlessTable, .RefreshableTable:
            ParseUtility.queryPostsFeed(PostType.Text,targetEvent: self.event!,range: range){(result: [PFObject]?, error: NSError?) -> Void in
                let posts = result as? [Activity] ?? []
                print("Posts available:\(posts.count)")
                if(posts.count == 0){
                    self.feedTableView!.tableFooterView = UIView()
                }
                
                
                // 3
                completionBlock(posts)
            }
        case .EndlessCollection, .RefreshableCollection:
            ParseUtility.queryPostsFeed(PostType.Media,targetEvent: self.event!,range: range){(result: [PFObject]?, error: NSError?) -> Void in
                let posts = result as? [Activity] ?? []
                print("\n\n\n\n\n\n\n Media files available:\(posts.count)\n\n\n\n\n\n\n\n")
                
                completionBlock(posts)
                
            }
        }
    }
    
    func updateIfModified(){
        event.fetchInBackgroundWithBlock{
            (object:PFObject?, error:NSError?) -> Void in
            let newData = object as! Event
            if ((error == nil) && (newData.updatedAt!.isAfter(self.event.updatedAt!))){
                self.event = newData
                self.setEventData()
            }
        }
        
    }
    
    func isDeleted(){
        let query = PFQuery(className:"Event")
        query.whereKey("objectId", equalTo:event!.objectId!)
        query.findObjectsInBackgroundWithBlock {
            (objects:[PFObject]?, error:NSError?) -> Void in
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) objects.")
                if(objects?.count <= 0){
                    self.navigationController?.popViewControllerAnimated(true)
                }
            }
        }
    }
    
    func didTapAddPost() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("addPostVC") as! AddPostViewController
        
        vc.event   = self.event
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func addPostBtnClicked(sender: AnyObject) {
        didTapAddPost()
    }
    
    func viewAttendees(){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("userList") as! PeopleFeedViewController
        vc.pageType = PeopleType.Attendees
        vc.event = event
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func editButtonWasHit(sender: AnyObject) {
        performSegueWithIdentifier("segueToEdit", sender: self.event)
    }
    @IBAction func attendButtonWasHit(sender: AnyObject) {
        if(dateNow.compare(event.endTime) == .OrderedAscending){
        event.toggleAttendEvent(PFEUser.currentUser()!)
            if ((attendButton.completed) == true){
                attendButton.setTitle("Attend", forState: UIControlState.Normal)
            }else{
                attendButton.setTitle("Attending", forState: UIControlState.Normal)
            }
            
            if(attendToPostLabel.hidden == true){
                attendToPostLabel.hidden = false
            }else{
                attendToPostLabel.hidden = true
            }
            
            if(addPostBtn.hidden == true){
                addPostBtn.hidden = false
                addPostBtn.userInteractionEnabled = true
                
            }else{
                addPostBtn.hidden = true
                addPostBtn.userInteractionEnabled = false
                
            }
        }else{
            self.presentViewController(UIAlertController(title: "Mouve Ended", message: "You cannot attend or Unattend a mouve that has ended."), animated: true, completion: nil)
        }
        
    }
    
    @IBAction func attendeesBtnWasHit(sender: AnyObject) {
        viewAttendees()
    }
    
    
    
    @IBAction func shareButtonWasHit(sender: AnyObject) {

        print("Popped up..", terminator: "")
        let alert:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let reportAction = UIAlertAction(title: "Report Mouve", style: UIAlertActionStyle.Destructive){
            UIAlertAction in
            self.reportMouve()
            
        }
        
        let facebookAction = UIAlertAction(title: "Share to Facebook", style: UIAlertActionStyle.Default){
            UIAlertAction in
            SVProgressHUD.show()
            self.shareToFaceBook(self.event)

        }
        let twitterAction = UIAlertAction(title: "Tweet", style: UIAlertActionStyle.Default){
            UIAlertAction in
            SVProgressHUD.show()
            self.shareToTwitter(self.event)

        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel){
            UIAlertAction in
        }
        // Add the actions
        if((dateNow.compare(event.endTime) == .OrderedAscending) && event.privacy){
            alert.addAction(reportAction)
            alert.addAction(cancelAction)
            
        }else if((dateNow.compare(event.endTime) == .OrderedAscending) && !event.privacy){
            if (event.creator != PFEUser.currentUser()!){
                alert.addAction(reportAction)
                alert.addAction(facebookAction)
                alert.addAction(twitterAction)
                alert.addAction(cancelAction)
            }else{
                alert.addAction(facebookAction)
                alert.addAction(twitterAction)
                alert.addAction(cancelAction)
            }
        }else{
            alert.addAction(reportAction)
            alert.addAction(cancelAction)
        }
        // Present the actionsheet
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            popoverMenu=UIPopoverController(contentViewController: alert)
            popoverMenu!.presentPopoverFromRect(shareButton!.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        }
    }
    
    @IBAction func inviteButtonWasHit(sender: AnyObject) {

        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("inviteVC") as! InviteFeedViewController

        vc.event = self.event
        vc.newEvent = false

        self.navigationController?.pushViewController(vc, animated: true)
        return
    }
    
    @IBAction func mainImageWasTapped(recognizer: UITapGestureRecognizer){
        didTapMainImage()
    }
    
    func didTapMainImage() {
        var images = [SKPhoto]()
        let photoURL = event.backgroundImage?.url
        let photo = SKPhoto.photoWithImageURL(photoURL!)
        images.append(photo)
        let browser = SKPhotoBrowser(photos: images)
        browser.displayToolbar = false
        
        presentViewController(browser, animated: true, completion: {})
    }
    
    @IBAction func backButtonWasHit(sender: AnyObject) {
        print("Popping out")
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        if let des = segue.destinationViewController as? FullImageViewController {
            des.media = sender as? Activity
        }
        if let des = segue.destinationViewController as? VideoPlayerController {
            des.video = sender as? Activity
        }
        if let des = segue.destinationViewController as? AddMouveViewController{
            des.event = sender as? Event
            des.editMode = true
            des.delegate = self
            tabBarController?.hidesBottomBarWhenPushed = true
        }
    }
    
    @IBAction func openMapForPlace(sender: UIButton) {
        let latitute:CLLocationDegrees! =  event?.location.latitude
        let longitute:CLLocationDegrees! =  event?.location.longitude
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitute, longitute)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(MKCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(MKCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(event!.name)"
        mapItem.openInMapsWithLaunchOptions(options)
    }
    
    struct ShouldRefresh {
        static var refresh: Bool = true
    }
    
}

func uploadIsImage (file: PFFile) -> Bool{
    let fileURL = file.url
    let string2 = fileURL as NSString!
    let trimmedString: String = (string2 as NSString).substringFromIndex(max(string2.length-4,0))
    
    if (trimmedString == ".jpg" || trimmedString == ".png"){
        print("\(trimmedString)")
        return true
    }
    else{
        return false
    }
}

