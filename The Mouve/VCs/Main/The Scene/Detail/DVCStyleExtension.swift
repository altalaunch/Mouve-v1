//
//  DetailViewControllerViewExtension.swift
//  The Mouve
//
//  Created by Hilal Habashi on 5/5/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import Social
import Parse
import SVProgressHUD
import FBSDKShareKit
import FBSDKCoreKit

extension DetailViewController { // View code and actions
    
    
    func shareToFaceBook(event : Event!){
        let isFbInstalled: Bool = UIApplication.sharedApplication().canOpenURL(NSURL(string: "fb://")!)
        var photoToShare : UIImage!
        let sharePhoto = event.backgroundImage as PFFile?
        
        sharePhoto?.getDataInBackgroundWithBlock({ (imageData: NSData?, error: NSError?) -> Void in
            SVProgressHUD.show()
            if (error == nil){
                photoToShare = UIImage(data:imageData!)
                if (isFbInstalled){
                    let content: FBSDKShareLinkContent = FBSDKShareLinkContent()
                    content.contentURL = NSURL(string: "https://themouveapp.com")
                    content.contentTitle = "\(event!.name)"
                    content.contentDescription = "The Mouve at \(event!.startTime.toShortTimeString()) is '\(event!.name)' in \(event!.address)"
                    let dialog: FBSDKShareDialog = FBSDKShareDialog()
                    dialog.mode = .Native
                    dialog.shareContent = content
                    dialog.fromViewController = self
                    dialog.show()
                    SVProgressHUD.dismiss()

                }else{
                
                    let shareToFacebook : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                    shareToFacebook.setInitialText("The Mouve at \(event!.startTime.toShortTimeString()) is '\(event!.name)' in \(event!.address)")
                    shareToFacebook.addImage( (photoToShare))
                    shareToFacebook.addURL(NSURL(string: "https://themouveapp.com"))

                    self.presentViewController(shareToFacebook, animated: true, completion: nil )
                    SVProgressHUD.dismiss()

                }

            }else{
                print("Could not download image")
            }
        })
    }
    
    func shareToTwitter(event : Event!){
        SVProgressHUD.show()

        var photoToShare : UIImage!
        let sharePhoto = event.backgroundImage as PFFile?
        sharePhoto?.getDataInBackgroundWithBlock({ (imageData: NSData?, error: NSError?) -> Void in
            if (error == nil){
                photoToShare = UIImage(data:imageData!)
                let shareToTwitter : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                shareToTwitter.setInitialText("#TheMouve at \(event!.startTime.toShortTimeString()) is \(event!.name) in \(event!.address)")
                shareToTwitter.addURL(NSURL(string: "https://themouveapp.com"))
                shareToTwitter.addImage( (photoToShare))
                self.presentViewController(shareToTwitter, animated: true, completion: nil )
                SVProgressHUD.dismiss()
            }else{
                print("Could not download image")
            }
        })

    }
    
    func reportMouve(){
        SVProgressHUD.show()
        
        let existingReportQuery = Report.query()!
        existingReportQuery.whereKey("fromUser", equalTo: PFEUser.currentUser()!)
        existingReportQuery.whereKey("onMouve", equalTo: event!)
        existingReportQuery.findObjectsInBackgroundWithBlock{ (data: [PFObject]?, error: NSError?) -> Void in
            let reports = data as? [Report] ?? []
            if(reports.count == 0){
                
                let report = Report()
                report.fromUser = PFEUser.currentUser()!
                report.onMouve = self.event!
                report.reportKey = "mouveReported"
                
                let reportACL = PFACL(user: PFEUser.currentUser()!)
                reportACL.publicReadAccess = true
                report.ACL = reportACL;
                
                report.saveInBackgroundWithBlock{ (succeeded: Bool, error: NSError?) -> Void in
                    if let error = error {
                        SVProgressHUD.dismiss()
                        
                        let errorString = error.userInfo["error"] as? NSString
                        self.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
                        
                    }else if (succeeded){
                        // end spinner
                        SVProgressHUD.dismiss()
                        self.presentViewController(UIAlertController(title: "Thank You!", message: "A member of our editorial team will take a look at this Mouve, and it will be removed if it violates our community giudelines."), animated: true, completion: nil)
                        
                    }
                }
                
            }else{
                self.presentViewController(UIAlertController(title: "Thank You!", message: "Thanks for reporting. You have already reported this Mouve and a member of our editorial team is on it."), animated: true, completion: nil)
                SVProgressHUD.dismiss()
                
            }
        }
    }

    
    func styleViewProgrammatically() {
        eventNameLabel.hidden = true
        addPostBtn.layer.cornerRadius = addPostBtn.frame.height / 5
        if(PFEUser.currentUser()?.objectId != event.creator.objectId){
            self.editButton.hidden = true
            self.editButton.userInteractionEnabled = false
            self.attendButton.hidden = false
            self.attendButton.userInteractionEnabled = true
        }
        else{
            self.editButton.hidden = false
            self.editButton.userInteractionEnabled = true
            self.attendButton.hidden = true
            self.attendButton.userInteractionEnabled = false
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
        var headerTransform = CATransform3DIdentity
        var headerImageRect = CGRect(x: 0, y: -(headerView.bounds.height), width: (feedTableView?.bounds.width)!, height: (headerView.bounds.height))

        print(offset, terminator: "")
        
        if offset < 0 { // PULL DOWN
            headerImageRect.origin.y = offset
            
            let headerScaleFactor:CGFloat = -(offset) / headerView.bounds.height
            let headerSizevariation = ((headerView.bounds.height * (1.0 + headerScaleFactor)) - headerView.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            headerView.frame = headerImageRect

        }
            
        else { // PUSH UP
            
        }
        
        // Apply Transformations
        headerView.layer.transform = headerTransform
    }
    



}
