//
//  DetailViewController.swift
//  The Mouve
//
//  Created by Hilal Habashi on 4/15/15.
//  Copyright (c) 2015 Hilal Habashi. All rights reserved.
//

import UIKit


import Parse
import MapKit
import Bond
import SVProgressHUD


class AddPostViewController: UIViewController,FeedComponentTarget{
    var event: Event!
    @IBOutlet weak var feedTableView: UITableView?
    @IBOutlet weak var feedCollectionView: UICollectionView?
    let defaultRange = 0...4
    let additionalRangeSize = 2
    var type: SceneType!
    var feedComponent: FeedComponent<Activity, AddPostViewController>!
//    var mediaFeedComponent: FeedComponent<Activity, AddPostViewController>!
    var pickedPic:Observable<UIImage?> = Observable(nil)
    var attendDisposable: DisposableType?
    var invitesDisposable: DisposableType?


    let offset_HeaderStop:CGFloat = 180 // At this offset the Header stops its transformations

    
    
    
    @IBOutlet weak var tableViewHeaderView: UIView!
    
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var postTextField: UITextField!

    
    var popoverMenu: UIPopoverController?=nil
    var imagePicker: UIImagePickerController?=UIImagePickerController()
    var newMediaFile: PFFile?
    var thumbnailFile: PFFile?
    var dateNow = NSDate()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.feedTableView?.rowHeight = UITableViewAutomaticDimension
        self.feedTableView?.estimatedRowHeight = 70
        
        feedComponent = FeedComponent(target: self, feedMode: FeedMode.RefreshableTable)
//        mediaFeedComponent = FeedComponent(target: self, feedMode: FeedMode.EndlessCollection)
//        styleHeader()
//        styleViewProgrammatically()
        
        addNavControllerLikePan()
        self.pickedPic.value = appDel.placeHolderBg
        addImageButton.layer.cornerRadius = addImageButton.frame.height/20
        addImageButton.clipsToBounds = true
        backButton.tintColor = UIColor.whiteColor()
        backButton.bringSubviewToFront(self.view)
        commentsLabel.hidden = true
        UIApplication.sharedApplication().statusBarStyle = .Default

    }
    func setEventData(){
//        feedComponent.loadInitialIfRequired()
//        feedComponent.refresh(self)
//        mediaFeedComponent.loadInitialIfRequired()
//        mediaFeedComponent.refresh(self)
    }
    func loadInRange(range: Range<Int>,feedMode: FeedMode, completionBlock: ([Activity]?) -> Void) {
        // 1
        switch(feedMode){
        case .EndlessTable, .RefreshableTable:
            ParseUtility.queryPostsFeed(PostType.Text,targetEvent: self.event!,range: range){(result: [PFObject]?, error: NSError?) -> Void in
                let posts = result as? [Activity] ?? []
                print("Posts available:\(posts.count)")
//                if(posts.count == 0){
//                    self.feedTableView!.tableFooterView = UIView()
//                }
                
                
                // 3
                completionBlock(posts)
            }
        case .EndlessCollection, .RefreshableCollection:
            print("Not available currently")
//            ParseUtility.queryPostsFeed(PostType.Media,targetEvent: self.event!,range: range){(result: [PFObject]?, error: NSError?) -> Void in
//                let posts = result as? [Activity] ?? []
//                print("Media files available:\(posts.count)")
//                completionBlock(posts)
//                
//            }
        }
    }
//    func updateIfModified(){
//        event.fetchInBackgroundWithBlock{
//            (object:PFObject?, error:NSError?) -> Void in
//            let newData = object as! Event
//            if ((error == nil) && (newData.updatedAt!.isAfter(self.event.updatedAt!))){
//                self.event = newData
//                self.setEventData()
//            }
//        }
//        
//    }
    
    func uploadMedia(){
        if(self.postTextField.text == "" && self.newMediaFile == nil){
            SVProgressHUD.dismiss()
            self.presentViewController(UIAlertController(title: "Attention", message: "Write some text please or upload a picture"), animated: true, completion: nil)
        }
        else{
            // add a post
            print("Adding comment")            
            //                MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            let post = Activity()
            post.fromUser = PFEUser.currentUser()!
            post.toUser = self.event.creator
            post.onMouve = self.event
            post.type = typeKeyEnum.Comment
            post.stringContent = self.postTextField.text!
            post.reported = 0
            if(self.newMediaFile != nil){
                post.mediaFile = self.newMediaFile!
                post.thumbnailFile = self.thumbnailFile!
                post.type = typeKeyEnum.AddMedia
            }else{
                post.type = typeKeyEnum.Comment
            }
            post.saveInBackgroundWithBlock
                { (succeeded: Bool, error: NSError?) -> Void in
                    if let error = error {
                        SVProgressHUD.dismiss()
                        
                        self.postTextField.resignFirstResponder()
                        let errorString = error.userInfo["error"] as? NSString
                        self.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
                        
                    }
                    else{
                        //                        self.pickedPic.value = appDel.placeHolderBg
                        SVProgressHUD.dismiss()
                        self.navigationController?.popViewControllerAnimated(true)

                        self.newMediaFile = nil
                        self.pickedPic.value = appDel.placeHolderBg
                        self.newMediaFile = nil
                        self.thumbnailFile = nil
                        self.postTextField.text = ""
                        self.postTextField.resignFirstResponder()

//                        self.setEventData()
                    }
            }
        }
    }
    
    @IBAction func doneBtnWasClicked(sender: AnyObject) {
        SVProgressHUD.show()
        uploadMedia()
    }
    
    @IBAction func backButtonWasHit(sender: AnyObject) {
        print("Popping out")
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
        self.setEventData()
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = false
        self.pickedPic.observe { (value: UIImage?) -> () in
            self.addImageButton?.setBackgroundImage(value, forState: .Normal)
        }
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default

    }
}
extension AddPostViewController: UITableViewDelegate, UITableViewDataSource, AddPostTableViewCellDelegate {
    
    
    func belongsToUser(cell: AddPostTableViewCell) -> Bool{
        if(cell.activity.fromUser == PFEUser.currentUser()){
            return true
        }
        else{
            return false
        }
    }
    
    func didTapDeletePost(cell: AddPostTableViewCell){
        func deletePost(){
            SVProgressHUD.show()
            
            //            MBProgressHUD.showHUDAddedTo(self.feedTableView, animated: true)
            let query = Activity.query()
            query!.whereKey("objectId", equalTo:cell.activity!.objectId!)
            query!.findObjectsInBackgroundWithBlock {
                (objects:[PFObject]?, error:NSError?) -> Void in
                SVProgressHUD.dismiss()
                
                //                MBProgressHUD.hideHUDForView(self.feedTableView, animated: true)
                if error == nil {
                    // The find succeeded.
                    print("Successfully retrieved \(objects!.count) objects.", terminator: "")
                    // Do something with the found objects
                    if let objects = objects {
                        for object  in objects {
                            let parseObj = object
                            parseObj.deleteInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
                                if (success) {
                                    self.setEventData()
                                } else {
                                    
                                }
                            }
                        }
                    }
                }else {
                    print("\(error)", terminator: "")
                }
            }
        }
        
        let deleteAlert = UIAlertController(title: "Delete Comment?", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        deleteAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
            deletePost()
            print("Handle Ok logic here")
        }))
        
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
            deleteAlert.dismissViewControllerAnimated(true, completion: nil)
            
            print("Handle Cancel Logic here")
        }))
        presentViewController(deleteAlert, animated: true, completion: nil)
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellID") as? AddPostTableViewCell
        let data = self.feedComponent.content[indexPath.section]
        data.dlPfImg()
        cell!.activity = data
        cell!.delegate = self
        
        
        
        
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.feedComponent.content.count
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        feedComponent.targetWillDisplayEntry(indexPath.section)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        //        return (posts != nil) ? (posts?.count)! : 0
    }
}

