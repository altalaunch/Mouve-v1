//
//  DetailPostTableViewCell.swift
//  The Mouve
//
//  Created by Hilal Habashi on 5/4/15.
//  Copyright (c) 2015 Hilal Habashi. All rights reserved.
//

import UIKit
import FontAwesome
import Bond
import Parse

class AddPostTableViewCell: UITableViewCell {
    var posterDisposable: DisposableType?
    var deleteBtnDisposable: DisposableType?
    var delegate: AddPostTableViewCellDelegate?

    @IBOutlet weak var usernameLabel: UILabel!

    var activity: Activity! {
        didSet {
            posterDisposable?.dispose()
            deleteBtnDisposable?.dispose()
            if let oldValue = oldValue where oldValue != activity{
                oldValue.profilePic.value = nil
            }
            
            self.setActivity()
            if((delegate) != nil){
                if(delegate!.belongsToUser(self)){
                    self.deleteButton.hidden = false
                }
                else{
                    self.deleteButton.hidden = true
                    self.deleteButton.userInteractionEnabled = false
                }
            }
        }
    }

    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var attributedLabel: UILabel!
    func setActivity(){
        attributedLabel.text = activity.stringContent
        posterDisposable = profileImageView.bindAndRound(activity.profilePic)
        self.attributedLabel!.numberOfLines = 0
        self.attributedLabel!.lineBreakMode = NSLineBreakMode.ByWordWrapping
    }
    
    @IBAction func deleteButtonWasHit(sender: AnyObject) {
        delegate?.didTapDeletePost(self)
//        deletePost()
    }

    
}


protocol AddPostTableViewCellDelegate {
    func belongsToUser(cell: AddPostTableViewCell) -> Bool
    func didTapDeletePost(cell: AddPostTableViewCell)

}


