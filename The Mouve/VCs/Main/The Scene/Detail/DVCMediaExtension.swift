//
//  DVCMediaExtension.swift
//  The Mouve
//
//  Created by Hilal Habashi on 9/22/15.
//  Copyright © 2015 The Mouve. All rights reserved.
//

import Foundation
import UIKit
import Parse
import MobileCoreServices
import SVProgressHUD
import MWPhotoBrowser

extension DetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MWPhotoBrowserDelegate {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.section == 1){
            mediaFeedComponent.targetWillDisplayEntry(indexPath.item)
        }
    }

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        switch ((collectionView.cellForItemAtIndexPath(indexPath) as! DetailMediaCollectionViewCell).mediaType! ){
        default:
//            print("Viewing an image...")
//            event.getPhotoForBrowser(indexPath.item)
            event.getPhotoForBrowser() {(media: [MWPhoto]?, error: NSError?) -> () in
                if error != nil{
                    self.presentViewController(UIAlertController(title: "Oops", message: "\(error!.localizedDescription)" as String), animated: true, completion:  nil)
                }else{
                    if let media = media as [MWPhoto]! {
                        print(media)
                        self.photos = media
                        let browser = MWPhotoBrowser(delegate: self)
                        
                        browser.setCurrentPhotoIndex(UInt(indexPath.item))
                        
                        
                        //                    browser.setCurrentPhotoIndex()
                        
//                        let vc = appDel.window?.rootViewController
                        //                    let browser = MWPhotoBrowser(photos: mediaArray)
                        browser.displayActionButton = true // Show action button to allow sharing, copying, etc (defaults to true)
                        browser.displayNavArrows = true // Whether to display left and right nav arrows on toolbar (defaults to false)
                        browser.displaySelectionButtons = false // Whether selection buttons are shown on each image (defaults to false)
                        browser.zoomPhotosToFill = true // Images that almost fill the screen will be initially zoomed to fill (defaults to true)
                        browser.alwaysShowControls = true // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to false)
                        browser.enableGrid = true // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to true)
                        browser.startOnGrid = false // Whether to start on the grid of thumbnails instead of the first photo (defaults to false)
                        browser.autoPlayOnAppear = true // Auto-play first video
                        
                        browser.enableSwipeToDismiss = true
                        self.navigationController!.pushViewController(browser, animated: true)
//                        vc?.presentViewController(browser, animated: true, completion: nil)
                    }
                }
            }
//            performSegueWithIdentifier("segueToFullScreen", sender: mediaFeedComponent.content[indexPath.item])
//        case .WatchVid:
//            print("Watching a video...")
//            performSegueWithIdentifier("segueToVideo", sender: mediaFeedComponent.content[indexPath.item])
//        default: break
//            print("nonthing happens here")
        }

    }
    
    func photoBrowserDidFinishModalPresentation(photoBrowser: MWPhotoBrowser!) {
//        photoBrowser.dismissViewControllerAnimated(true, completion: nil)
        self.dismissViewControllerAnimated(true, completion: nil)
//        self.navigationController!.popViewControllerAnimated(true)
    }
    func numberOfPhotosInPhotoBrowser(photoBrowser: MWPhotoBrowser) -> UInt {
        return UInt(self.photos!.count)
    }
    
    func photoBrowser(photoBrowser: MWPhotoBrowser!, photoAtIndex index: UInt) -> MWPhotoProtocol! {
        if Int(index) < self.photos!.count {
            return self.photos![Int(index)] as MWPhoto
        }
        
        return nil
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell: DetailMediaCollectionViewCell?
            cell = collectionView.dequeueReusableCellWithReuseIdentifier("cellID", forIndexPath: indexPath) as? DetailMediaCollectionViewCell
            cell!.layer.cornerRadius = (cell!.frame.height / 20)
            let media = mediaFeedComponent.content[indexPath.item]
            media.dlMediaThumb()
            cell!.media = media
        return cell!
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (mediaFeedComponent.content.count)

    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
}

