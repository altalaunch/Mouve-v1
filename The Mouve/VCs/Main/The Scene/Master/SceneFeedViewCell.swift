
//
//  SceneFeedViewCell.swift
//  The Mouve
//
//  Created by Hilal Habashi on 4/8/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation
import UIKit
import Parse
import Bond
import SwiftDate
class SceneFeedViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var goingButton: GreyGreenButton!
    @IBOutlet weak var inviteButton: UIButton!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    
    var delegate: SceneFeedViewCellDelegate?
    var profileDel: SceneProfileCellDelegate?
    var dateNow = NSDate()
    var feedCollectionView: UICollectionView? = nil
    var eventDisposable: DisposableType?
    var creatorDisposable: DisposableType?
    var attendDisposable: DisposableType?
    var isInProfile = false
    var event: Event! {
        didSet {
            creatorDisposable?.dispose()
            eventDisposable?.dispose()
            attendDisposable?.dispose()
            if let oldValue = oldValue where oldValue != event {
                oldValue.thumbBgImg.value = nil
                oldValue.creator.thumbProfImg.value = nil
            }
            if let event = event {
                eventDisposable = backgroundImageView.bindAndBlur(event.thumbBgImg)
                creatorDisposable = profileImageView.bindAndRound(event.creator.thumbProfImg)
                profileImageView.layer.borderColor = UIColor.seaFoamGreen().CGColor
                profileImageView.layer.borderWidth = 1
                profileImageView.userInteractionEnabled = true
                let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("profileImageWasTapped:"))
                profileImageView.addGestureRecognizer(tapGestureRecognizer)
                nameLabel.text = event.name
                descriptionLabel.text = event.about
                if((profileDel) != nil){
                    if(profileDel!.belongsToUser(self)){
                        self.profileImageView.hidden = true
                    }
                    else{
                        self.profileImageView.hidden = false
                    }
                }
                
                var dayLabel:String
                if (event.startTime.isInToday()){
                    dayLabel = "Today"
                    if(event.endTime.isInTomorrow()){
                        dayLabel+=" - Tomorrow"
                    }
                }
                else if(event.startTime.isInTomorrow()){
                    dayLabel = "Tomorrow"
                }
                else if(event.startTime.isInYesterday()){
                    dayLabel = "Yesterday"
                    if(event.endTime.isInToday()){
                        dayLabel+=" - Today"
                    }
                }
                else{
                    var yearFormat=""
                    if(NSDate().year != event.endTime.year){
                        yearFormat="YY\''"
                    }
                    if(event.startTime.day == event.endTime.day){
                        dayLabel = "\(event.startTime.toString(DateFormat.Custom("MMM dd "+yearFormat))!)"
                    }
                    else if(event.startTime.month != event.endTime.month){
                        dayLabel = "\(event.startTime.toString(DateFormat.Custom("MMM dd"))!) - \(event.endTime.toString(DateFormat.Custom("MMM dd "+yearFormat))!)"
                    }
                    else{
                        dayLabel = "\(event.startTime.toString(DateFormat.Custom("MMM dd"))!)-\(event.endTime.toString(DateFormat.Custom("dd "+yearFormat))!)"
                    }
                    
                }
                
                dateAndTimeLabel.text = "\(dayLabel) | \(event.startTime.toShortTimeString()) - \(event.endTime.toShortTimeString())"
                
                distanceLabel.text = (String(format: "%.2f",event.location.distanceInMilesTo(PFGeoPoint(location: UserDefaults.lastLocation))))+" Miles Away"
                
                _ = ["Beach-Chillin", "Coffee-Hour", "Espresso-Lesson", "Fire-Works", "Food-Festival", "Football-Game", "San-Francisco-Visit", "State-Fair", "Study-Sesh", "Surf-Lesson"]
                //            loadImages(event)
                attendDisposable = event.attendees.observe { (value: [PFEUser]?) -> () in
                    // 3
                    if let value = value {
                        self.goingButton.shaded = value.contains(PFEUser.currentUser()!)
                    }
                    else{
                        self.goingButton.shaded? = false
                    }
                }
            }
        }
    }
    
    @IBAction func buttonTapped(sender: AnyObject) {

    }
    override func awakeFromNib() {
        super.awakeFromNib()

    }

//    func processEvent(indexPath: NSIndexPath){

    @IBAction func profileImageWasTapped(recognizer: UITapGestureRecognizer){
        delegate?.didTapProfileImage(self)
    }
    
    @IBAction func goingButtonWasHit(sender: AnyObject) {
        delegate?.didTapAttendEvent(self)
    }
    
    @IBAction func shareButtonWasHit(sender: AnyObject) {
        delegate?.didTapShareEvent(self)
    }
    
    
}



protocol SceneFeedViewCellDelegate {
    func didTapProfileImage(cell: SceneFeedViewCell)
    func didTapAttendEvent(cell: SceneFeedViewCell)
    func didTapShareEvent(cell: SceneFeedViewCell)
}
protocol SceneProfileCellDelegate {
    func belongsToUser(cell: SceneFeedViewCell) -> Bool
}