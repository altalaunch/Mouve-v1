//
//  SceneFeedViewController.swift
//  The Mouve
//
//  Created by Hilal Habashi on 4/8/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import CoreLocation
import Parse
import Bolts
import Bond
import DZNEmptyDataSet
import SVProgressHUD


class SceneFeedViewController: UIViewController, FeedComponentTarget{
    
    @IBOutlet weak var feedTableView: UITableView?
    var feedCollectionView: UICollectionView? = nil
    let defaultRange = 0...9
    let additionalRangeSize = 10
    var type: SceneType!
    var feedMode = FeedMode.RefreshableTable
    var feedComponent: FeedComponent<Event, SceneFeedViewController>!
    
    
    convenience init(type: SceneType) {
        self.init()
        self.type = type
        LocalMessage.observe(.NewLocationRegistered, classFunction: "newLocation", inClass: appDel)
    }
    
    func loadInRange(range: Range<Int>,feedMode: FeedMode, completionBlock: ([Event]?) -> Void) {
        // 1
        ParseUtility.queryFeed(self.type!,range: range){(result: [PFObject]?, error: NSError?) -> Void in
            let posts = result as? [Event] ?? []
            // 3
            if(posts.count == 0){
                self.feedTableView!.tableFooterView = UIView()
                self.feedTableView!.emptyDataSetDelegate = self
                self.feedTableView!.emptyDataSetSource = self
            }
            completionBlock(posts)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        switch(self.type as SceneType){
            case .Scene:
                LocalMessage.post(.HomeTitlePageOne)
            case .Explore:
                LocalMessage.post(.HomeTitlePageTwo)
            default:
                break
        }
        
//        feedComponent.loadInitialIfRequired()
        feedComponent.refresh(self,changeOffset: false)
        self.feedTableView!.contentInset = UIEdgeInsets(top: 44+22, left: 0, bottom: 44, right: 0)
        
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        LocalMessage.post(type!.hashValue == 0 ? .HomeFeedPageOne : .HomeFeedPageTwo)
        feedComponent = FeedComponent(target: self,feedMode: FeedMode.RefreshableTable)

    }
    
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        feedComponent.loadInitialIfRequired()
        feedComponent.refresh(self,changeOffset: false)
        SVProgressHUD.dismiss()

    }
    
}
extension SceneFeedViewController: SceneFeedViewCellDelegate {
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        if let des = segue.destinationViewController as? DetailViewController {
            des.event = sender as? Event
        }
        if let des = segue.destinationViewController as? ProfileViewController {
            let user = sender as! PFEUser
            des.user = user
        }
        if let des = segue.destinationViewController as? AddMouveViewController {
            des.hidesBottomBarWhenPushed = true;
        }
        
    }
    func didTapProfileImage(cell: SceneFeedViewCell) {
        
        performSegueWithIdentifier("segueToProfile", sender: cell.event.creator)
        
    }
    func didTapAttendEvent(cell: SceneFeedViewCell) {
        let dateNow = NSDate()
        
        if(dateNow.compare(cell.event.endTime) == .OrderedAscending){
            cell.event.toggleAttendEvent(PFEUser.currentUser()!)
        }else{
            self.presentViewController(UIAlertController(title: "Mouve Ended", message: "You cannot attend or Unattend a mouve that has ended."), animated: true, completion: nil)
        }
        
        
    }
    func didTapShareEvent(cell: SceneFeedViewCell) {
//        load share screen and HUD over here
    }
}
extension SceneFeedViewController : DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
//        print("trying to add mouve", terminator: "")
        performSegueWithIdentifier("addMouve", sender: self)
    }
    
    func emptyDataSetWillAppear(scrollView: UIScrollView!) {
        self.feedTableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: " \(type!.rawValue)")
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "#WhatsTheMouve"
        
        return NSAttributedString(string: text, attributes: [
            NSForegroundColorAttributeName: UIColor.grayColor()
            ])
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "mouve-icon")
    }
}
extension SceneFeedViewController: UITableViewDataSource{
    // MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.feedComponent.content.count

    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellID", forIndexPath: indexPath)  as? SceneFeedViewCell

        let event = self.feedComponent.content[indexPath.section]
        event.dlBgThumb()
        event.creator.dlPfImg()
        event.fetchAttendees()
        cell!.event = event
        cell!.delegate = self

        return cell!
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        feedComponent.targetWillDisplayEntry(indexPath.section)
    }
}
extension SceneFeedViewController:  UITableViewDelegate{
    
    
    //MARK: UITableViewDelegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        performSegueWithIdentifier("segueToDetail", sender: self.feedComponent.content[indexPath.section])
        
    }
    

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0 : 4
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}