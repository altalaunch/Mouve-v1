//
//  TheScenePageViewController.swift
//  The Mouve
//
//  Created by Hilal Habashi on 4/24/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
public enum SceneType: String {
    case Explore = "Explore"
    case Scene = "My Scene" 
    case Newsfeed = "Newsfeed"
    case Invites = "Invites"
}
class TheScenePageViewController: UIPageViewController {

    let badgeView = PGTabBadge()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        ParseErrorHandler.handleBlockedUser()
        
        LocalMessage.observe(.HomeFeedPageOne, classFunction: "pageOne", inClass: self)
        LocalMessage.observe(.HomeFeedPageTwo, classFunction: "pageTwo", inClass: self)

        navigationItem.titleView = SceneTitleView(type: .Scene, frame: CGRect(x: 0, y: 0, width: 140, height: 44))
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "myNotificationReceived:", name: "pushNotification", object: nil)
        
        pageViewControllerDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default

    }

    func myNotificationReceived(notification : NSNotification) {
        print("Observer method called")
        updateActivityBadge()
        
    }

    
    func updateActivityBadge(){
//        let tabArray = self.tabBarController?.tabBar.items as NSArray!
//        let tabItem = tabArray.objectAtIndex(1) as! UITabBarItem
        
//        tabItem.badgeValue = ""
        
        let itemWidth:CGFloat = (self.tabBarController?.tabBar.frame.width)! / CGFloat((self.tabBarController?.tabBar.items!.count)!)
        let itemHeight:CGFloat = (self.tabBarController?.tabBar.frame.height)!
        let itemPosition = CGFloat(2)
        let bgColor = UIColor.seaFoamGreen()
        let badgeView = PGTabBadge()
        
        badgeView.frame.size = CGSizeMake(10, 10)
        badgeView.center = CGPointMake((itemWidth * itemPosition)-(itemWidth/2) + (itemWidth * 0.1), (itemHeight * 0.2))
        badgeView.layer.cornerRadius = badgeView.bounds.width/2
        badgeView.clipsToBounds = true
        badgeView.backgroundColor = bgColor
        badgeView.tag = 10

        self.tabBarController?.tabBar.addSubview(badgeView)
        
    }
    
    
    func pageOne() {
        if let vc = self.viewControllers![0] as? SceneFeedViewController{
            if vc.type == SceneType.Explore {
                self.setViewControllers([sceneVCWithType(.Scene)], direction: .Reverse, animated: true, completion: nil)
                LocalMessage.post(.HomeTitlePageOne)
            }
        }
    }
    
    func pageTwo() {
        if let vc = self.viewControllers![0] as? SceneFeedViewController{
            if vc.type == SceneType.Scene {
                self.setViewControllers([sceneVCWithType(.Explore)], direction: .Forward, animated: true,completion: nil)
            LocalMessage.post(.HomeTitlePageTwo)
            }
        }
    }
    
    @IBAction func hamburgerButtonWasHit(sender: AnyObject) {
    }
    
    @IBAction func activityButtonWasHit(sender: AnyObject) {
    }
}

extension TheScenePageViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewControllerDidLoad() {
        self.setViewControllers([sceneVCWithType(.Scene)], direction: .Forward, animated: true, completion: nil)
    }
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if(completed){
            (previousViewControllers[0] as!
                SceneFeedViewController).type == .Scene ? LocalMessage.post(.HomeTitlePageTwo) : LocalMessage.post(.HomeTitlePageOne)
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        return (viewController as! SceneFeedViewController).type == .Scene ? sceneVCWithType(.Explore) : nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        return (viewController as! SceneFeedViewController).type == SceneType.Scene ? nil : sceneVCWithType(.Scene)
    }
    
    
    func sceneVCWithType(type: SceneType) -> SceneFeedViewController {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("sceneFeedVC") as! SceneFeedViewController
        vc.type = type
        
        return vc
    }
    
    @IBAction func unwindToTheSceneVC(segue: UIStoryboardSegue) {}
}