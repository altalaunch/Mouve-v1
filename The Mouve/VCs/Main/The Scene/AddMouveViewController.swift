//
//  AddMouveViewController.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 5/25/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import Parse
import Bond
import MapKit
import SVProgressHUD


class AddMouveViewController: UIViewController, UIAlertViewDelegate, UIPopoverControllerDelegate {
    @IBOutlet weak var titleEventTextField: UnderlinedTextField!
    @IBOutlet weak var detailInfoTextField: UnderlinedTextField!
    @IBOutlet weak var locationTextField: UnderlinedTextField!
    @IBOutlet weak var mouveTitleLabel: UILabel!
    @IBOutlet weak var postMouveButton: UIButton!
    @IBOutlet weak var eventImageButton: UIButton?
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel?
    @IBOutlet weak var publicPrivateSwitch: UISwitch!
    @IBOutlet weak var publicPrivateLabel: UILabel!
    
    
    var imagePicker: UIImagePickerController?=UIImagePickerController()
    var popoverMenu: UIPopoverController?=nil
    var pickedPic:Observable<UIImage?> = Observable(nil)
    var pickedPoint: PFGeoPoint?
    var actualAddress: String?
    var editMode = false
    var picChanged = false
    var delegate: EditObjectDelegate?
    var event: Event?
    let rangeSlider = TimeRangeSlider(frame: CGRectZero)
    let gpaViewController = GooglePlacesAutocomplete(
        apiKey: appDel.gpaAPIKey,
        placeType: .All
    )
    
    
    //  Switch to toggle between public and private
    
    @IBAction func flipSwitch(sender: AnyObject) {
                let invitesAllowed = appDel.currentConfig!["invitesAllowed"] as! Bool
                if(invitesAllowed){
                    publicPrivateSwitch.on = !publicPrivateSwitch.on
                    publicPrivateLabel.text = publicPrivateSwitch.on ? "Private" : "Public"
                    let buttonLabel = publicPrivateSwitch.on ? "Invite People" : "Create Mouve"
                    postMouveButton.setTitle(buttonLabel, forState: UIControlState.Normal)
                }
                else{
                    self.presentViewController(UIAlertController(title: "Oops", message: "Unfortunately we do not support private events currently" as String), animated: true, completion:  nil)
                    publicPrivateSwitch.on = false
                }
    }
    
    
    func createNewEvent(){
        SVProgressHUD.show()
        event = Event()
        event!.creator = PFEUser.currentUser()!
        event!.name  = titleEventTextField.text!.uppercaseString
        event!.about = detailInfoTextField.text!
        event!.address = actualAddress!
        event!.location = pickedPoint!
        event!.startTime = rangeSlider.timeDates().startDate
        event!.endTime = rangeSlider.timeDates().endDate
        event!.privacy = publicPrivateSwitch.on
        event!.disabled = false
        if(self.pickedPic.value == appDel.placeHolderBg){
            self.pickedPic.value = appDel.dummyEventPic
        }
        let imgData = generateImageData(self.pickedPic.value!, fullSize: 1000, thumbSize: 400)
        event!.backgroundImage = PFFile(name: "bg.jpg", data: imgData.full)
        event!.thumbnailImg = PFFile(name: "thumbnail.jpg", data:imgData.thumbnail)
        event!.thumbBgImg.value = UIImage(data: imgData.thumbnail)
        if (event!.privacy){
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("inviteVC") as! InviteFeedViewController
            vc.event = event
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        else{
            event!.saveInBackgroundWithBlock
                { (succeeded: Bool, error: NSError?) -> Void in
                    if let error = error {
                        SVProgressHUD.dismiss()

                        let errorString = error.userInfo["error"] as? NSString
                        self.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
                    }
                    else if (succeeded){
                        // end spinner
                        SVProgressHUD.dismiss()
                        self.navigationController?.popViewControllerAnimated(true)
                    }
            }
        }
    }

}

extension AddMouveViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(textField: UITextField){
        
        if textField == locationTextField{
            // Google API for location field
            gpaViewController.placeDelegate = self // Conforms to GooglePlacesAutocompleteDelegate
            
            presentViewController(gpaViewController, animated: true, completion: nil)
        }
        else{
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField {
        case titleEventTextField:
            titleEventTextField.resignFirstResponder()
            detailInfoTextField.becomeFirstResponder()
        case detailInfoTextField:
            detailInfoTextField.resignFirstResponder()
            locationTextField.becomeFirstResponder()
        case locationTextField:
            locationTextField.resignFirstResponder()
        default: ()
        }
        
        return true
    }
}

extension AddMouveViewController: GooglePlacesAutocompleteDelegate {
    func placeSelected(place: Place) {
        print(place.description, terminator: "")
        
        place.getDetails { details in
            self.locationTextField.text = details.name
            self.actualAddress = place.description
            self.pickedPoint = PFGeoPoint(latitude: details.latitude, longitude: details.longitude)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        
    }
    func placeViewClosed() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}