////
////  InviteViewController.swift
////  The Mouve
////
////  Created by Aadil Ali on 9/3/15.
////  Copyright (c) 2015 Andrew Breckenridge. All rights reserved.
////
//
//import UIKit
//import Parse
//
//class InviteViewController: UIViewController {
//    
//    var selectedArray : [Int] = []
//    var parseObjectArray : [PFObject] = []
//    var selectedInvitations : [PFObject] = []
//    var mouvePFObject: PFObject!
//    var myFollowers: [Activity]? {
//        didSet {
//            self.inviteTableView.reloadData()
//        }
//    }
//    
//    @IBOutlet weak var selectAllButton: UIButton!
//    @IBAction func selectAllInvites(sender: AnyObject) {
//        if(selectAllButton.selected){
//            selectAllButton.selected = false
//            self.markAllArrayUnSelected()
//             self.inviteTableView.reloadData()
//        }
//        else{
//            selectAllButton.selected = true
//            self.markAllArraySelected()
//             self.inviteTableView.reloadData()
//        }
//    }
//    func markAllArraySelected(){
//        for index in 0..<selectedArray.count{
//            selectedArray[index] = 1
//        }
//    
//    }
//    func markAllArrayUnSelected(){
//        for index in 0..<selectedArray.count{
//            selectedArray[index] = 0
//        }
//        
//    }
//  
//    
//    @IBAction func addMouveBtnClicked(sender: AnyObject) {
//        var counter : Int = 0
//        for object in selectedArray{
//            if(object == 1){
//                let obj : PFObject = parseObjectArray[counter]
//                selectedInvitations.append(obj)
//            }
//            counter = counter + 1
//        }
//        if(selectedInvitations.count > 0){
//            self.mouvePFObject["Invitees"] = self.selectedInvitations
//        }
//        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
//        self.mouvePFObject.saveInBackgroundWithBlock
//            { (succeeded: Bool, error: NSError?) -> Void in
//                if let error = error {
//                    MBProgressHUD.hideHUDForView(self.view, animated: true)
//                    let errorString = error.userInfo["error"] as? NSString
//                    self.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
//                }else{
//                    MBProgressHUD.hideHUDForView(self.view, animated: true)
//                    self.navigationController?.popToRootViewControllerAnimated(true)
//                }
//        }
//    }
//    
//    @IBOutlet weak var inviteTableView: UITableView!
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
//        getFollowers()
//        // Do any additional setup after loading the view.
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    @IBAction func cancelButtonHit(sender: AnyObject) {
//        self.navigationController?.popViewControllerAnimated(true)
//    }
//    func getFollowers(){
////        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
////        let query = PFQuery(className:"Follow")
////        query.whereKey("follower", equalTo:PFUser.currentUser()!)
////        query.includeKey("following")
////        query.findObjectsInBackgroundWithBlock {
////            (objects:[AnyObject]?, error:NSError?) -> Void in
////            if error == nil {
////                var serverData = [Activity]()
////                // The find succeeded.
////                print("Successfully retrieved \(objects!.count) objects.", terminator: "")
////                // Do something with the found objects
////                if let objects = objects {
////                    self.parseObjectArray = objects as! [PFObject]
////                    for object in objects {
////                         let invite =  Invite(parseObject: object as! PFObject i, isfollow: true)
////                         serverData.append(invite)
////                        self.selectedArray.append(0)
////                        
////                        }
////                    }
////                    self.myFollowers = serverData
////                    MBProgressHUD.hideHUDForView(self.view, animated: true)
////                }else{
////                    MBProgressHUD.hideHUDForView(self.view, animated: true)
////                }
////            }
//    }
//    func checkBoxClicked(sender:UIButton){
//        
//         let tag = sender.tag
//            print(tag, terminator: "")
//            if(self.selectedArray[tag]==0){
//                self.selectedArray[tag] = 1
//                sender.selected = true
//    
//            }
//            else{
//                self.selectedArray[tag] = 0
//                sender.selected = false
//            }
//        if selectedArray.contains(1){
//            self.selectAllButton.selected = true
//        }
//        else{
//            self.selectAllButton.selected = false
//        }
//    }
//    
//}
//
//extension InviteViewController: UITableViewDelegate, UITableViewDataSource {
//    
//    func tableViewDidLoad() {
//        self.inviteTableView.delegate = self
//        self.inviteTableView.dataSource = self
//    }
//    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.myFollowers == nil ? 0 : self.myFollowers!.count
//    }
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCellWithIdentifier("cellID") as! InviteTableViewCell
//        
////        let inviteObject : Invite = myFollowers![indexPath.row]
////        cell.attributedLabel.text = inviteObject.name
////        cell.userName.text = "@"+inviteObject.userName
////        cell.calendarButton.tag = indexPath.row
////        if(selectedArray[indexPath.row]==1){
////            cell.calendarButton.selected = true
////        }
////        else{
////             cell.calendarButton.selected = false
////        }
////        cell.calendarButton.addTarget(self, action:"checkBoxClicked:", forControlEvents: UIControlEvents.TouchUpInside)
////        if let image: PFFile = inviteObject.profileImage{
////        image.getDataInBackgroundWithBlock({
////            (imageData: NSData?, error: NSError?) -> Void in
////            if (error == nil) {
////                let image = UIImage(data:imageData!)
////                cell.profileImageView.image = image
////                
////            }
////            
////        })
////        }
////        
//        return cell
//    }
//    
//    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        return UIView(frame: .zero)
//    }
//}
//
//    
//
