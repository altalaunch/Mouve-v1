//
//  AddMouveVCStyle.swift
//  The Mouve
//
//  Created by Hilal Habashi on 9/24/15.
//  Copyright © 2015 The Mouve. All rights reserved.
//

import Foundation
import UIKit

extension AddMouveViewController{
    
    override func viewDidLoad() {
        ParseErrorHandler.handleBlockedUser()
        addTextDismiss()
        if(editMode){
            showViewForEditing()
        }else{
            self.pickedPic.value = appDel.placeHolderBg
            self.deleteButton.hidden = true
            self.deleteButton.userInteractionEnabled = false
        }
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = false
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.pickedPic.observe { (value: UIImage?) -> () in
            self.eventImageButton?.setBackgroundImage(value, forState: .Normal)
        }


    }
    
    @IBAction func backButtonWasHit(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    // Alignments for the range-slider
    override func viewDidLayoutSubviews() {
        
        //        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 150
        
        [titleEventTextField, detailInfoTextField, locationTextField].map({ $0.delegate = self })
        
        eventImageButton!.layer.cornerRadius = eventImageButton!.frame.height / 2
        
        //Switch color
        publicPrivateSwitch.tintColor = UIColor.lightSeaFoamGreen()
        publicPrivateSwitch.onTintColor = UIColor.lightSeaFoamGreen()
        publicPrivateSwitch.thumbTintColor = UIColor.seaFoamGreen()
        //switch color ends
        
        // Time slider
        if (!editMode){
            rangeSlider.trackHighlightTintColor = UIColor.seaFoamGreen()
            rangeSlider.trackTintColor = UIColor.lightNicePaleBlue()
            rangeSlider.curvaceousness = 0.3
            rangeSlider.thumbThicknessPercent = 0.5
            rangeSlider.thumbTintColor = UIColor.seaFoamGreen()
            
            rangeSlider.addTarget(self, action: "rangeSliderValueChanged:", forControlEvents: .ValueChanged)
            
            self.view.addSubview(rangeSlider)
            rangeSliderValueChanged(self.rangeSlider)
        }
        // Time Slider ends
        statusBar(.LightContent)
        self.tabBarController?.hidesBottomBarWhenPushed = true
        self.navigationController?.navigationBarHidden = true
//        let margin: CGFloat = 20.0
//        let width = view.bounds.width - 2.0 //* margin
        rangeSlider.frame = CGRect(x: 40, y: timeLabel!.frame.origin.y + 20, width: view.frame.width - 80, height: 30)
        self.eventImageButton?.contentMode = .ScaleAspectFit
        self.eventImageButton?.layer.borderWidth = 1
        self.eventImageButton?.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    // Changes labels as you drag slider
    func rangeSliderValueChanged(rangeSlider: TimeRangeSlider) {
        let currentTimes = rangeSlider.timeDates()
        if(currentTimes.startDate.isInToday() && currentTimes.endDate.isInToday() ){
            timeLabel!.text = "Today: \(currentTimes.startDate.toShortTimeString()) - \(currentTimes.endDate.toShortTimeString())"
        }
        else if(currentTimes.startDate.isInToday() && currentTimes.endDate.isInTomorrow()){
            timeLabel!.text = "Today: \(currentTimes.startDate.toShortTimeString()) - Tomorrow:\(currentTimes.endDate.toShortTimeString())"
        }
        else if(currentTimes.startDate.isInTomorrow()){
            timeLabel!.text = "Tomorrow: \(currentTimes.startDate.toShortTimeString()) - \(currentTimes.endDate.toShortTimeString())"
        }

    }
}