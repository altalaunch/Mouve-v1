//
//  AddMouveEdit.swift
//  The Mouve
//
//  Created by Hilal Habashi on 9/24/15.
//  Copyright © 2015 The Mouve. All rights reserved.
//

import Foundation
import UIKit
import Parse
import SVProgressHUD

protocol EditObjectDelegate {
    func updateIfModified()
}
extension AddMouveViewController {
    func showViewForEditing(){
        self.postMouveButton.setTitle("Save", forState: UIControlState.Normal)
        self.mouveTitleLabel.text = "Edit Mouve"
        self.titleEventTextField.text = event?.name
        self.detailInfoTextField.text = event?.about
        self.locationTextField.text = event?.address
        self.actualAddress = event?.address
        self.pickedPoint = event?.location
        self.publicPrivateSwitch.on = (event?.privacy)!
        self.publicPrivateLabel.text = publicPrivateSwitch.on ? "Private" : "Public"
        event?.thumbBgImg.bindTo(self.pickedPic)
        if(event!.endTime.isBefore(NSDate())){
            timeLabel!.text = "\(self.event!.startTime.toShortTimeString()) - \(self.event!.endTime.toShortTimeString())"
        }else{
            rangeSlider.trackHighlightTintColor = UIColor.seaFoamGreen()
            rangeSlider.trackTintColor = UIColor.lightNicePaleBlue()
            rangeSlider.curvaceousness = 0.3
            rangeSlider.thumbThicknessPercent = 0.5
            rangeSlider.thumbTintColor = UIColor.seaFoamGreen()
            
            rangeSlider.addTarget(self, action: "rangeSliderValueChanged:", forControlEvents: .ValueChanged)
            
            self.view.addSubview(rangeSlider)
            rangeSliderValueChanged(self.rangeSlider)   
        }

    }
    
    func generateImageData(originalImg: UIImage, fullSize: CGFloat, thumbSize: CGFloat) -> (full: NSData,thumbnail: NSData){
        let full = ImageCropper.squareCropImageToSideLength(originalImg, toSize: fullSize)
        let thumbnail = ImageCropper.squareCropImageToSideLength(originalImg, toSize: thumbSize)
        return (UIImageJPEGRepresentation(full,0.7)!,UIImageJPEGRepresentation(thumbnail, CGFloat(0.3))!)
    }
    
    @IBAction func deleteButtonAction(sender: AnyObject) {
        let deleteAlert = UIAlertController(title: "Delete Mouve", message: "All photos, videos and comments associated with this Mouve will also be deleted. Are you sure you want to delete this Mouve?", preferredStyle: UIAlertControllerStyle.Alert)
        deleteAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
            self.deleteMouve()
            print("Handle Ok logic here")
        }))
        
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
            deleteAlert.dismissViewControllerAnimated(true, completion: nil)
            
            print("Handle Cancel Logic here")
        }))
        presentViewController(deleteAlert, animated: true, completion: nil)
    }
    
    func backTwo() {
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: false);
        
    }
    
    func deleteMouve(){
        SVProgressHUD.show()
        let query = Event.query()
        query!.whereKey("objectId", equalTo:self.event!.objectId!)
        query!.findObjectsInBackgroundWithBlock {
            (objects:[PFObject]?, error:NSError?) -> Void in
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) objects.", terminator: "")

                // Do something with the found objects
                if let objects = objects {
                    for object  in objects {
                        let parseObj = object 
                        parseObj.deleteInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
                            if (success) {
                                SVProgressHUD.dismiss()
                                self.backTwo()
                            } else {
                                print("\(error)", terminator: "")
                            }
                        }
                    }
                }
            }else {
                SVProgressHUD.dismiss()
                print("\(error)", terminator: "")
            }
        }
    }


    func findAndEdit(){
        self.event!.name = self.titleEventTextField.text!.uppercaseString
        self.event!.about = self.detailInfoTextField.text!
        self.event!.address = self.actualAddress!
        self.event!.location = self.pickedPoint!
        if(self.event!.endTime.isAfter(NSDate())){
            self.event!.startTime = self.rangeSlider.timeDates().startDate
            self.event!.endTime = self.rangeSlider.timeDates().endDate
        }
        self.event!.privacy = self.publicPrivateSwitch.on
        if(self.picChanged){
            let imgData = generateImageData(self.pickedPic.value!, fullSize: 1000, thumbSize: 400)
            event!.backgroundImage = PFFile(name: "bg.jpg", data: imgData.full)
            event!.thumbnailImg = PFFile(name: "thumbnail.jpg", data:imgData.thumbnail)
            event?.thumbBgImg.value = UIImage(data: imgData.thumbnail)
        }
        self.event?.saveInBackgroundWithBlock
            { (succeeded: Bool, error: NSError?) -> Void in
                if let error = error {
                    SVProgressHUD.dismiss()

//                    MBProgressHUD.hideHUDForView(self.view, animated: true)
                    let errorString = error.userInfo["error"] as? NSString
                    self.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
                }else if (succeeded){
                    SVProgressHUD.dismiss()

//                    MBProgressHUD.hideHUDForView(self.view, animated: true)
                    self.delegate?.updateIfModified()
                    self.navigationController?.popViewControllerAnimated(true)
//                                                }
            }
        }
    }
    
    
}


//Mouve Post / Edit Action
extension AddMouveViewController {
    
    @IBAction func postMouveButtonWasHit(sender: AnyObject) {
        
        func validateData() -> Bool{
            if((self.titleEventTextField.text != "") && (self.detailInfoTextField.text != "") && (self.locationTextField.text != "")){
                return true
            }else{
                return false
            }
        }
        if(validateData() == true){
            SVProgressHUD.show()
            if(editMode){
                findAndEdit()
            }else{
                createNewEvent()
            }
        }
        else{
            self.presentViewController(UIAlertController(title: "Warning", message: "Please fill all the fields" as String), animated: true, completion: nil)
        }

    }
    
    
}