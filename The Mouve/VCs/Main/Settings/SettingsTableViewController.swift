//
//  SettingsTableViewController.swift
//  The Mouve
//
//  Created by Hilal Habashi on 4/28/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import Parse
import SVProgressHUD
import ParseFacebookUtilsV4
import ParseTwitterUtils
import FBSDKLoginKit
import Bond

class SettingsTableViewController: UITableViewController {

    @IBOutlet weak var radiusSlider: UISlider!
    @IBOutlet weak var radiusLabel: UILabel!
    @IBOutlet weak var followersNotifySwitch: UISwitch!
    @IBOutlet weak var commentNotifySwitch: UISwitch!
    @IBOutlet weak var attendNotifySwitch: UISwitch!
    @IBOutlet weak var inviteNotifySwitch: UISwitch!
    @IBAction func sliderValueChanged(sender: UISlider) {
        UserDefaults.radiusLimit = Int(sender.value)
        radiusLabel.text = "\(UserDefaults.radiusLimit!) mi"
    }
    @IBAction func unwindToTheSettingsVC(segue: UIStoryboardSegue) {}
    
    var fbLinked: Observable<Bool?> = Observable(nil)
    var twitterLinked: Observable<Bool?> = Observable(nil)
    var subscribedChannels: Observable<NSSet?> = Observable(nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        ParseErrorHandler.handleBlockedUser()
        radiusSlider.tintColor = UIColor.seaFoamGreen()
        radiusSlider.thumbTintColor = UIColor.seaFoamGreen()
        radiusSlider.value = Float(UserDefaults.radiusLimit!)
        radiusLabel.text = "\(UserDefaults.radiusLimit!) mi"
        [commentNotifySwitch, inviteNotifySwitch, followersNotifySwitch,attendNotifySwitch].map(
            { $0.tintColor = UIColor.lightSeaFoamGreen()
              $0.onTintColor = UIColor.lightSeaFoamGreen()
              $0.thumbTintColor = UIColor.seaFoamGreen()
            }
        )
    }
    
    func obtainChannels(){
        PFPush.getSubscribedChannelsInBackgroundWithBlock() { (channels: Set<NSObject>?, error: NSError?) -> () in
            if(error != nil){
                print(error?.localizedDescription)
            }else{
                self.subscribedChannels.value = channels
                print(self.subscribedChannels.value)
            }
        }
    }
    
    @IBAction func switchToggled(sender: UISwitch){
        var channel=""
        switch(sender){
            case inviteNotifySwitch:
                channel = "invites"
            case commentNotifySwitch:
                channel = "comments"
            case attendNotifySwitch:
                channel = "attendings"
            case followersNotifySwitch:
                channel = "follows"
            default:()
        }
        toggleNotificationChannel(channel)
    }
    
    override func viewDidAppear(animated: Bool) {

        self.fbLinked.value = PFFacebookUtils.isLinkedWithUser(PFEUser.currentUser()!)
        let fbRow = NSIndexPath(forRow: 0, inSection: 1)
        fbLinked.observe { (value: Bool?) -> () in
            if let value = value {
                self.tableView.cellForRowAtIndexPath(fbRow)?.textLabel?.text = (value ? "Unlink Facebook Account":"Link Facebook Account")
            }else{
                self.tableView.cellForRowAtIndexPath(fbRow)?.textLabel?.text = "Link Facebook Account"
            }
        }
        
        
        twitterLinked.value = PFTwitterUtils.isLinkedWithUser(PFEUser.currentUser()!)
        let twitterRow = NSIndexPath(forRow: 1, inSection: 1)
        twitterLinked.observe { (value: Bool?) -> () in
            if let value = value {
                self.tableView.cellForRowAtIndexPath(twitterRow)?.textLabel?.text = (value ? "Unlink Twitter Account":"Link Twitter Account")
            }
            else{
                self.tableView.cellForRowAtIndexPath(twitterRow)?.textLabel?.text = "Link Twitter Account"
            }
        }
        
        obtainChannels()
        subscribedChannels.observe{(value: NSSet?) -> () in
            if let value = value{
                self.attendNotifySwitch.on = value.containsObject("attendings")
                self.commentNotifySwitch.on = value.containsObject("comments")
                self.followersNotifySwitch.on = value.containsObject("follows")
                self.inviteNotifySwitch.on = value.containsObject("invites")
            }else{
                [self.commentNotifySwitch, self.inviteNotifySwitch, self.followersNotifySwitch,self.attendNotifySwitch].map(
                    { $0.on = false})
            }
        }
        
    }
    func toggleNotificationChannel(channel: String){
        if(self.subscribedChannels.value != nil){
            if(self.subscribedChannels.value!.containsObject(channel)){
                PFPush.unsubscribeFromChannelInBackground(channel)  {(success: Bool, error: NSError?) -> () in
                    if(error != nil){
                        self.presentViewController(UIAlertController(title: "Oops...", message: error!.localizedDescription), animated: true, completion: nil)
                    }else if(success){
                        print("Unsubscribed from \(channel)")
                        self.obtainChannels()
                    }
                }
            }
            else{
                PFPush.subscribeToChannelInBackground(channel)  {(success: Bool, error: NSError?) -> () in
                    if(error != nil){
                        self.presentViewController(UIAlertController(title: "Oops...", message: error!.localizedDescription), animated: true, completion: nil)
                    }else if(success){
                        print("Subscribed to \(channel)")
                        self.obtainChannels()
                    }
                }
            }
        }else{
            self.presentViewController(UIAlertController(title: "Oops...", message: "Something doesn't line up check again later"), animated: true, completion: nil)
            [self.commentNotifySwitch, self.inviteNotifySwitch, self.followersNotifySwitch,self.attendNotifySwitch].map(
                { $0.on = false})
            
        }
    }
    func processGraphInfo(){
        FBSDKGraphRequest(graphPath: "me", parameters:["fields":"email,name"]).startWithCompletionHandler({ (connection, result, error) in
            if error != nil {
                self.presentViewController(UIAlertController(title: "Whoops!", message: error!.localizedDescription), animated: true, completion: nil)
            } else {
                if let loginResult = result as? Dictionary<String,AnyObject> {
//                    dispatch_async(dispatch_get_main_queue(), {
//                        let userID = FBSDKAccessToken.currentAccessToken().userID
                        PFFacebookUtils.linkUserInBackground(PFUser.currentUser()!, withAccessToken: FBSDKAccessToken.currentAccessToken()) { (success:Bool, error:NSError?) -> Void in
                            if(error != nil){
                                self.presentViewController(UIAlertController(title: "Oops...", message: error!.localizedDescription), animated: true, completion: nil)
                            }else if(success){
//                                PFEUser.currentUser()!.fbId = FBSDKAccessToken.currentAccessToken().userID
                                self.fbLinked.value = PFFacebookUtils.isLinkedWithUser(PFUser.currentUser()!)
                            }
                            
                        }
//                    })
                }
            }
        })

    }
    func toggleFacebookLink(){
    dispatch_async(dispatch_get_main_queue(), {
        if(PFFacebookUtils.isLinkedWithUser(PFUser.currentUser()!)){
            PFFacebookUtils.unlinkUserInBackground(PFUser.currentUser()!) {(success: Bool, error: NSError?) -> () in
                if(error != nil){
                    self.presentViewController(UIAlertController(title: "Oops...", message: error!.localizedDescription), animated: true, completion: nil)
                }else if(success){
                    self.fbLinked.value = PFFacebookUtils.isLinkedWithUser(PFUser.currentUser()!)
                }
            }
        }else{
            let permissions = ["public_profile"]
            if((FBSDKAccessToken.currentAccessToken()) != nil){
                self.processGraphInfo()
            }else{
                FBSDKLoginManager().logInWithReadPermissions(permissions, fromViewController: self.parentViewController, handler: { (result, error) in
                    if error != nil {
                        self.presentViewController(UIAlertController(title: "Oops...", message: error!.localizedDescription), animated: true, completion: nil)
                    }
                    if((FBSDKAccessToken.currentAccessToken()) != nil){
                        self.processGraphInfo()
                    }
                })
            }
        }
        })
    }
    
    func toggleTwitterLink(){
        if(PFTwitterUtils.isLinkedWithUser(PFUser.currentUser()!)){
            PFTwitterUtils.unlinkUserInBackground(PFUser.currentUser()!) {(success: Bool, error: NSError?) -> Void in
                if error != nil {
                    self.presentViewController(UIAlertController(title: "Oops...", message: error!.localizedDescription), animated: true, completion: nil)
                }else {
                    
                    if(success){
                    print("The user is no longer associated with their Twitter account.")
                    self.twitterLinked.value = false
                    }
                }
            }
        }else{
            PFTwitterUtils.linkUser(PFUser.currentUser()!) {(success: Bool?, error: NSError?) -> Void in
                if error != nil {
                    self.presentViewController(UIAlertController(title: "Oops...", message: error!.localizedDescription), animated: true, completion: nil)
                }else{
                    if PFTwitterUtils.isLinkedWithUser(PFUser.currentUser()!) {
                        print("Woohoo, user logged in with Twitter!")
                        self.twitterLinked.value = true

                    }
                }
            }
        }
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                print("Radius", terminator: "")
            default: ()
            }
        case 1:
            switch indexPath.row {
            case 0:
                toggleFacebookLink()
                
            case 1:
                toggleTwitterLink()
            default: ()
            }
            
        case 2:
            switch indexPath.row {
            case 0:
                print("Followers", terminator: "")

            case 1:
                print("Attendees", terminator: "")
                
            case 2:
                print("Invites", terminator: "")

            default: ()
            }
            
        case 3:
            switch indexPath.row {
            case 0:
                print("Contact us", terminator: "")
                didTapContactUs()
            case 1:
                print("Privacy Policy", terminator: "")
                didTapPrivacyPolicy()
                
            case 2:
                print("Terms of use", terminator: "")
                didTapTermsOfUse()
                
            default: ()
            }

        case 4:
            switch indexPath.row {
            case 0:
                let deleteAlert = UIAlertController(title: "Log Out?", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
                deleteAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
                    SVProgressHUD.show()
                    appDel.logOut()
                    print("Handle Ok logic here")
                }))
                
                deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
                    deleteAlert.dismissViewControllerAnimated(true, completion: nil)
                    
                    print("Handle Cancel Logic here")
                }))
                presentViewController(deleteAlert, animated: true, completion: nil)
            default: ()
            }
        default: ()
        }
    }
    
    func didTapContactUs() {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("webPageVC") as! WebPageViewController
        
        vc.websiteURL   = "http://themouveapp.com/contactus.html"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func didTapPrivacyPolicy() {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("webPageVC") as! WebPageViewController
        
        vc.websiteURL   = "http://themouveapp.com/privacypolicy.html"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func didTapTermsOfUse() {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("webPageVC") as! WebPageViewController
        
        vc.websiteURL   = "http://themouveapp.com/termsofservice.html"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

}
