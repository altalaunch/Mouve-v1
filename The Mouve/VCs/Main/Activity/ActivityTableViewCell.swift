//
//  ActivityTableViewCell.swift
//  The Mouve
//
//  Created by Hilal Habashi on 5/5/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import TTTAttributedLabel
import Parse
import Bond

class ActivityTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var attendButton: UIButton!
    @IBOutlet weak var thumbActivity: UIImageView!
    @IBOutlet weak var attributedLabel: TTTAttributedLabel!
    
    
    var feedCollectionView: UICollectionView? = nil
    var thumbDisposable: DisposableType?
    var creatorDisposable: DisposableType?
    var delegate: ActivityTableViewCellDelegate?
    var type: SceneType!
    var activity: Activity! {
        didSet {
            self.attendButton.hidden = true
            creatorDisposable?.dispose()
            thumbDisposable?.dispose()
            if let oldValue = oldValue where oldValue != activity {
                oldValue.profilePic.value = nil
                oldValue.eventBg.value = nil
            }
            if let activity = activity {
                self.creatorDisposable = self.profileImageView.bindAndRound(activity.profilePic)
                if(activity.thumbnailFile != nil){
                    self.thumbActivity.hidden = false
                    self.thumbActivity.userInteractionEnabled = true
                    self.thumbDisposable = self.thumbActivity.bindAndBlur(activity.thumbnailImg)
                    let tapEventImageGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("eventImageWasTapped:"))
                    self.thumbActivity.addGestureRecognizer(tapEventImageGestureRecognizer)
                }
                else if(activity.onMouve != nil){
                    self.thumbActivity.hidden = false
                    self.thumbActivity.userInteractionEnabled = true
                    self.thumbDisposable = self.thumbActivity.bindAndBlur(activity.eventBg)
                    let tapEventImageGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("eventImageWasTapped:"))
                    self.thumbActivity.addGestureRecognizer(tapEventImageGestureRecognizer)
                }
                else{
                    self.thumbActivity.hidden = true
                    self.thumbActivity.userInteractionEnabled = false
                }
                self.profileImageView.userInteractionEnabled = true
                let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("profileImageWasTapped:"))
                self.profileImageView.addGestureRecognizer(tapGestureRecognizer)
                    switch(activity.type!){
                        case .Invite:
                            self.attendButton.hidden = false
                            if((self.activity.onMouve) != nil){
                                let string = "@\(self.activity!.fromUser.username!) invited you to '\(self.activity!.onMouve!.name)'. \(NSDate().offsetFrom(self.activity!.createdAt!))" as NSString
                                self.generateLabel(string)
                            }

    //                        }
    //                    }

                        case .Attend:
                            print("\nAttend\n")

                            if((self.activity.onMouve) != nil){
                                let string = "@\(self.activity!.fromUser.username!) is attending \(self.activity!.onMouve!.name). \(NSDate().offsetFrom(self.activity!.createdAt!))" as NSString
                                self.generateLabel(string)
                            }

                        case .Comment, .AddMedia:
//                            if((activity.onMouve) != nil){
                            if(activity.thumbnailFile != nil){
                                let string = "@\(self.activity!.fromUser.username!) added media to \(self.activity!.onMouve!.name). \(NSDate().offsetFrom(self.activity!.createdAt!))" as NSString
                                self.generateLabel(string)
                            }else{
                                let string = "@\(self.activity!.fromUser.username!) commented: \(self.activity!.stringContent) \(NSDate().offsetFrom(self.activity!.createdAt!))" as NSString
                                self.generateLabel(string)

                            }
                        
//                            }
                        default:
                            let string = "@\(self.activity!.fromUser.username!) started following you. \(NSDate().offsetFrom(self.activity!.createdAt!))" as NSString
                            self.generateLabel(string)

                        
                        
                            break
                    }
//                })
            }
        }
    }

    func generateLabel(string: NSString){
        let attributedString = NSMutableAttributedString(string: string as String)

        
        let firstAttributes = [NSForegroundColorAttributeName: UIColor.nicePaleBlue()]
        let secondAttributes = [NSForegroundColorAttributeName: UIColor.lightGrayColor()]
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2.0
        
        attributedString.addAttributes(firstAttributes, range: string.rangeOfString("@\(self.activity!.fromUser.username!)"))
        attributedString.addAttributes(secondAttributes, range: string.rangeOfString("\(NSDate().offsetFrom(self.activity!.createdAt!))"))
        attributedString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        
        
        self.attributedLabel.attributedText = attributedString
        self.attributedLabel!.numberOfLines = 0
        self.attributedLabel!.lineBreakMode = NSLineBreakMode.ByWordWrapping
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.thumbActivity.layer.cornerRadius = 4
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func profileImageWasTapped(recognizer: UITapGestureRecognizer){
        delegate?.didTapProfileImage(self)
    }
    
    @IBAction func eventImageWasTapped(recognizer: UITapGestureRecognizer){
        delegate?.didTapEventImage(self)
    }
    
    @IBAction func attendButtonWasHit(sender: AnyObject) {
        print("youre going", terminator: "")
    }
}

protocol ActivityTableViewCellDelegate {
    func didTapProfileImage(cell: ActivityTableViewCell)
    func didTapEventImage(cell: ActivityTableViewCell)
}