//
//  ActivityTableViewController.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 4/25/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Parse
import UIKit
import CoreLocation

import Parse
import Bolts
import Bond
import DZNEmptyDataSet
import SVProgressHUD

class ActivityTableViewController: UIViewController, FeedComponentTarget {
    @IBOutlet weak var feedTableView: UITableView?
    var feedCollectionView: UICollectionView? = nil
    let defaultRange = 0...19
    let additionalRangeSize = 10
    var type: SceneType! 
    var feedComponent: FeedComponent<Activity, ActivityTableViewController>!

    
    
    func loadInRange(range: Range<Int>,feedMode: FeedMode, completionBlock: ([Activity]?) -> Void) {
        // 1
        ParseUtility.queryFeed(self.type!,range: range){(result: [PFObject]?, error: NSError?) -> Void in
            let posts = result as? [Activity] ?? []
            // 3
            if(posts.count == 0){
                self.feedTableView!.tableFooterView = UIView()
                self.feedTableView!.emptyDataSetDelegate = self
                self.feedTableView!.emptyDataSetSource = self
            }
            completionBlock(posts)
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.dismissViewControllerAnimated(animated, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        feedTableView?.rowHeight = UITableViewAutomaticDimension
        feedTableView?.estimatedRowHeight = 70
        feedTableView?.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        switch(self.type as SceneType){
        case .Newsfeed:
            LocalMessage.post(.ActivityTitlePageOne)
        case .Invites:
            LocalMessage.post(.ActivityTitlePageTwo)
        default:
            break
        }
        feedComponent = FeedComponent(target: self,feedMode: .RefreshableTable)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        SVProgressHUD.show()
//        MBProgressHUD.showHUDAddedTo(self.feedTableView, animated: true)
                feedComponent.loadInitialIfRequired()
        //        self.feedTableView!.contentInset = UIEdgeInsets(top: 44+22, left: 0, bottom: 44, right: 0)
        feedComponent.refresh(self,changeOffset: false)
        SVProgressHUD.dismiss()
//        MBProgressHUD.hideHUDForView(self.feedTableView, animated: true)
        //        LocalMessage.post(type.hashValue == 1 ? .ActivityFeedPageOne : .ActivityFeedPageTwo)
    }
}

extension ActivityTableViewController: UITableViewDelegate {
    
    //    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    //        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    //
    //        performSegueWithIdentifier("segueToDetail", sender: self.feedComponent.content[indexPath.section])
    //
    //    }
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
}
extension ActivityTableViewController: ActivityTableViewCellDelegate {
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        if let des = segue.destinationViewController as? DetailViewController {
            des.event = sender as? Event
        }
        if let des = segue.destinationViewController as? ProfileViewController {
            let user = sender as! PFEUser
            des.user = user
        }
        
    }
    func didTapProfileImage(cell: ActivityTableViewCell) {
        print("\n\n\n**********Jumping to \(cell.activity.fromUser.username!)'s profile************\n\n\n", terminator: "")
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("profileVC") as! ProfileViewController
        vc.user = cell.activity.fromUser
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func didTapEventImage(cell: ActivityTableViewCell) {
        //        load attending HUD over here
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("detailVC") as! DetailViewController
        
        vc.event = cell.activity.onMouve
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension ActivityTableViewController: UITableViewDataSource{
    // MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.feedComponent.content.count
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellID", forIndexPath: indexPath)  as? ActivityTableViewCell
        
        let activity = feedComponent.content[indexPath.section]
        
        if(activity.thumbnailFile != nil){
            activity.dlMediaThumb()
        }
        activity.onMouve?.dlBgThumb()
        activity.onMouve?.thumbBgImg.bindTo(activity.eventBg)
        activity.fromUser.dlPfImg()
        activity.fromUser.thumbProfImg.bindTo(activity.profilePic)
        
        cell!.type = self.type
        cell!.activity = activity
        cell!.delegate = self
        
        return cell!
    }
    
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        feedComponent.targetWillDisplayEntry(indexPath.section)
    }
}

extension ActivityTableViewController : DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        print("trying to add mouve", terminator: "")
        performSegueWithIdentifier("addMouve", sender: self)
    }
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: " \(type!.rawValue)")
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let text = (type == SceneType.Newsfeed) ? "No Notifications" : "No Invites"
        
        return NSAttributedString(string: text, attributes: [
            NSForegroundColorAttributeName: UIColor.grayColor()
            ])
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return (type == SceneType.Newsfeed) ? UIImage(named: "notification-bell") : 
            UIImage(named: "message")
    }
}

