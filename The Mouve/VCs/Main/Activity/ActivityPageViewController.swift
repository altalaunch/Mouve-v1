//
//  ActivityPageViewController.swift
//  The Mouve
//
//  Created by Hilal Habashi on 4/25/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit

class ActivityPageViewController: UIPageViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        pageViewControllerDidLoad()
        
        LocalMessage.observe(.ActivityFeedPageOne, classFunction: "pageOne", inClass: self)
        LocalMessage.observe(.ActivityFeedPageTwo, classFunction: "pageTwo", inClass: self)
        navigationItem.titleView = SceneTitleView(type: .Newsfeed, frame: CGRect(x: 0, y: 0, width: 140, height: 44))
//        self.navigationController?.tabBarItem.badgeValue = nil


    }
    
    func pageOne() {
        if let vc = self.viewControllers![0] as? ActivityTableViewController{
            if vc.type == SceneType.Invites {
                self.setViewControllers([activityVCWithType(.Newsfeed)], direction: .Reverse, animated: true,completion: nil)
//                LocalMessage.post(.ActivityTitlePageOne)
            }
        }
    }

    func pageTwo() {
        if let vc = self.viewControllers![0] as? ActivityTableViewController{
            if vc.type == SceneType.Newsfeed {
                self.setViewControllers([activityVCWithType(.Invites)], direction: .Forward, animated: true,completion: nil)
//                LocalMessage.post(.ActivityTitlePageTwo)
            }
        }
    }

}

extension ActivityPageViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewControllerDidLoad() {
        self.setViewControllers([activityVCWithType(.Newsfeed)], direction: .Forward, animated: true, completion: nil)
    }
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if(completed){
            (previousViewControllers[0] as!
                SceneFeedViewController).type == .Newsfeed ? LocalMessage.post(.ActivityTitlePageTwo) : LocalMessage.post(.ActivityTitlePageOne)
        }
    }
    

    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        return (viewController as! ActivityTableViewController).type == .Newsfeed ? activityVCWithType(.Invites) : nil
    }
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        return (viewController as! ActivityTableViewController).type == .Newsfeed ? nil : activityVCWithType(.Newsfeed)
    }
    
    func activityVCWithType(type: SceneType) -> ActivityTableViewController {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("activityTableVC") as! ActivityTableViewController
        vc.type = type
        
        return vc
    }
}