//
//  ActivityViewController.swift
//  The Mouve
//
//  Created by Hilal Habashi on 4/26/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit

class ActivityViewController: UIViewController {
    @IBOutlet weak var activityView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        ParseErrorHandler.handleBlockedUser()
        TheScenePageViewController().badgeView.opaque = false
        if (self.tabBarController?.tabBar.viewWithTag(10) != nil){
            self.tabBarController?.tabBar.viewWithTag(10)?.removeFromSuperview()
        }
        
        activityView.addSubview(SceneTitleView(type: .Newsfeed, frame: CGRect(origin: .zero, size: CGSize(width: activityView.frame.width, height: activityView.frame.height))))
    }
    override func viewDidAppear(animated: Bool) {
        self.navigationController?.tabBarItem.badgeValue = nil
        if (self.tabBarController?.tabBar.viewWithTag(10) != nil){
            self.tabBarController?.tabBar.viewWithTag(10)?.removeFromSuperview()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(false)
        self.dismissViewControllerAnimated(false, completion: nil)

    }
}
