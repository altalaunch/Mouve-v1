//
//  ProfileViewController.swift
//  The Mouve
//
//  Created by Hilal Habashi on 4/28/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import CoreGraphics
import Parse
import Bond
import SVProgressHUD
import SKPhotoBrowser


enum ProfileType{
    case MyProfile, OtherUser
}

class ProfileViewController: UIViewController,FeedComponentTarget{
    let offset_HeaderStop:CGFloat = 180 // At this offset the Header stops its transformations
    let offset_B_LabelHeader:CGFloat = 120 // At this offset the Black label reaches the Header
    let distance_W_LabelHeader:CGFloat = 180 // The distance between the bottom of the Header and the top of the White Label
    
    @IBOutlet weak var feedTableView: UITableView?
    @IBOutlet weak var headerImgView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var profilePicView: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var separationLabel: UIView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var editProfileOrFollow: smallTextOutlinedButton!
    @IBOutlet weak var mouveButton: UIButton!
    @IBOutlet weak var followingButton: UIButton!
    @IBOutlet weak var followersButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    
    var feedCollectionView: UICollectionView? = nil
    let defaultRange = 0...9
    let additionalRangeSize = 5
    var popoverMenu: UIPopoverController?=nil
    var type: SceneType!
    var feedMode = FeedMode.RefreshableTable
    var feedComponent: FeedComponent<Event, ProfileViewController>!
    var pfType = ProfileType.MyProfile
    var followersDisposable: DisposableType?
    
    var user: PFEUser?{
        didSet{
            user?.fetchFollowing()
            user?.dlPfImg()
            PFEUser.currentUser()?.fetchBlocked()
            PFEUser.currentUser()?.fetchBlockingMe()
            if(user == PFEUser.currentUser()){
                pfType = .MyProfile
                print("My Profile")
            }
            else{
                pfType = .OtherUser
                print("Other User")
            }
        }
    }


    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        SVProgressHUD.show()
        setEditFollowButton()
        feedComponent.loadInitialIfRequired()
        feedComponent.refresh(self,changeOffset: false)
        setProfileData()
        SVProgressHUD.dismiss()
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        ParseErrorHandler.handleBlockedUser()
        feedComponent = FeedComponent(target: self, feedMode: FeedMode.RefreshableTable)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        //        Fixes stuck image bug
        self.headerImgView.hidden = true
        self.navigationController?.navigationBarHidden=false
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.headerImgView.hidden = false
        self.navigationController?.navigationBarHidden=true
    }
    
    @IBAction func profButtonTapped(sender: AnyObject) {
        
        var images = [SKPhoto]()
        let photoURL = user!.profileImage!.url
        let photo = SKPhoto.photoWithImageURL(photoURL!)
        images.append(photo)
        let browser = SKPhotoBrowser(photos: images)
        browser.displayToolbar = false
        
        presentViewController(browser, animated: true, completion: {})

    }
    
    func loadInRange(range: Range<Int>, feedMode: FeedMode, completionBlock: ([Event]?) -> Void) {
        // 1
        ParseUtility.queryProfileMouves(self.user!,range: range){(result: [PFObject]?, error: NSError?) -> Void in
            if((error) != nil){
                print(error?.userInfo)
            }
            let posts = result as? [Event] ?? []
            print("Posts available:\(posts.count)")
            if(posts.count == 0){
                self.feedTableView!.tableFooterView = UIView()
                self.feedTableView!.emptyDataSetDelegate = self
                self.feedTableView!.emptyDataSetSource = self
            }

            // 3
            completionBlock(posts)
        }
        
    }
    
    func updateIfModified(){
        user = PFEUser.currentUser()!
        user!.fetchInBackgroundWithBlock{
            (object:PFObject?, error:NSError?) -> Void in
            let newData = object as! PFEUser
            if ((error == nil) && (newData.updatedAt!.isAfter(self.user!.updatedAt!))){
                self.user = newData
                self.setProfileData()
            }
        }
    }
    
    func setProfileData(){
        self.profileButton.bindAndRound(user!.thumbProfImg)
        self.profileButton!.layer.borderWidth = 1
        self.profileButton!.layer.borderColor = UIColor.seaFoamGreen().CGColor

        headerImgView.bindAndBlur(user!.thumbProfImg,alpha: CGFloat(0.9))
        setCounts()
        nameLabel.text! = user!.fullName
        usernameLabel.text! = "@" + user!.username!
        if (user == PFEUser.currentUser()!){
            moreButton.hidden = true
            moreButton.userInteractionEnabled = false
        }else{
            moreButton.hidden = false
            moreButton.userInteractionEnabled = true
        }
    }
    
    func setEditFollowButton(){
        print("1. Setting according to new user...")
        if(user == nil){
            user = PFEUser.currentUser()
            print("2. No user was passed...")
        }
//        backButton.bringSubviewToFront(self.headerView)
        if(self.tabBarController?.selectedIndex == 3 && self.navigationController?.viewControllers.count == 1 && pfType == .MyProfile){
            print("3. No back button for you...")
            self.backButton.hidden = true
            self.backButton.userInteractionEnabled = false
        }
        else{
            print("2. Other user so we need back button")
            self.backButton.hidden = false
            self.backButton.userInteractionEnabled = true
        }
        if(pfType == .OtherUser){
            followersDisposable = user?.followers.observe { (value: [PFEUser]?) -> () in
                // 3
                if let value = value {
                    self.editProfileOrFollow.completed = value.contains(PFEUser.currentUser()!)
                    if(self.editProfileOrFollow.completed!){
                        self.editProfileOrFollow.setTitle("Following", forState: UIControlState.Normal)
                    }
                    else{
                        self.editProfileOrFollow.setTitle("Follow", forState: UIControlState.Normal)
                    }
                }
                else{
                    self.editProfileOrFollow.completed = false
                    self.editProfileOrFollow.setTitle("Follow", forState: UIControlState.Normal)
                }
            }
        }
       editProfileOrFollow.hidden = false
    }
//    }
    func setCounts(){
        let userQuery : PFQuery = PFEUser.query()!
        userQuery.whereKey("disabled", notEqualTo: true)
        
        let blockQuery : PFQuery = Block.query()!
        blockQuery.whereKey("fromUser", equalTo: user!)

        //        Followers
        let query = Activity.query()
        query!.whereKey("typeKey", equalTo:typeKeyEnum.Follow.rawValue)
        query!.whereKey("toUser", equalTo:user!)
        query!.whereKey("fromUser", matchesQuery: userQuery)
        query!.whereKey("fromUser", doesNotMatchKey: "toUser", inQuery: blockQuery)

        query!.countObjectsInBackgroundWithBlock(){(count: Int32, error: NSError?) -> Void in
            if ((error) == nil) {
                self.followersButton.setTitle("\(count)\nFollowers", forState: UIControlState.Normal)
//                self.followersButton.setTitle("\(user!.numOfFollowers)\nFollowers", forState: UIControlState.Normal)
    
            }else{
                self.presentViewController(UIAlertController(title: "Uh oh", message: error!.localizedDescription), animated: true, completion: nil)
            }
            
        }

        ParseUtility.countProfileMouves(user!) { (num: Int32, error:NSError?) -> Void in
            if(error != nil){
                self.presentViewController(UIAlertController(title: "Uh oh", message: error!.localizedDescription), animated: true, completion: nil)
            }else{
                self.mouveButton.setTitle("\(num)\nMouves", forState: UIControlState.Normal)
            }
        }

        
        let followingQuery = Activity.query()
        followingQuery!.whereKey("typeKey", equalTo:typeKeyEnum.Follow.rawValue)
        followingQuery!.whereKey("fromUser", equalTo:user!)
        followingQuery!.whereKey("toUser", matchesQuery: userQuery)
        followingQuery!.countObjectsInBackgroundWithBlock(){(count: Int32, error: NSError?) -> Void in
            if ((error) == nil) {
                self.followingButton.setTitle("\(count)\nFollowing", forState: UIControlState.Normal)
             
//                self.followingButton.setTitle("\(user!.numOfFollowing)\nFollowing", forState: UIControlState.Normal)
            }
        }
    }
    
    func reportUser(){
        SVProgressHUD.show()
        
        
        let existingReportQuery = Report.query()!
        existingReportQuery.whereKey("fromUser", equalTo: PFEUser.currentUser()!)
        existingReportQuery.whereKey("toUser", equalTo: user!)
        existingReportQuery.findObjectsInBackgroundWithBlock{ (data: [PFObject]?, error: NSError?) -> Void in
            let reports = data as? [Report] ?? []
            if(reports.count == 0){
                
                let report = Report()
                report.fromUser = PFEUser.currentUser()!
                report.toUser = self.user!
                report.reportKey = "userReported"
                
                let reportACL = PFACL(user: PFEUser.currentUser()!)
                reportACL.publicReadAccess = true
                report.ACL = reportACL;
                
                report.saveInBackgroundWithBlock{ (succeeded: Bool, error: NSError?) -> Void in
                    if let error = error {
                        SVProgressHUD.dismiss()
                        
                        let errorString = error.userInfo["error"] as? NSString
                        self.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
                        
                    }else if (succeeded){
                        // end spinner
                        SVProgressHUD.dismiss()
                        self.presentViewController(UIAlertController(title: "Thank You!", message: "A member of our editorial team will take a look at this user's profile, and it will be removed if it violates our community giudelines."), animated: true, completion: nil)
                        
                    }
                }
                
            }else{
                self.presentViewController(UIAlertController(title: "Thank You!", message: "Thanks for reporting. You have already reported this user and a member of our editorial team is on it."), animated: true, completion: nil)
                SVProgressHUD.dismiss()
                
            }
        }
    }
    

    
    func blockAlert(){
        let blockTitle: String = (user!.isBlockedByMe() ? "Unblock User" : "Block User" )
        let blockMessage: String = (user!.isBlockedByMe() ? "Are you sure you want to unblock this User?" : "Are you sure you want to block this User?" )

        let deleteAlert = UIAlertController(title: blockTitle, message: blockMessage, preferredStyle: UIAlertControllerStyle.Alert)
        deleteAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
            SVProgressHUD.showSuccessWithStatus(self.user!.isBlockedByMe() ? "Unblocking User..." : "Blocking User..." )
            self.user?.toggleBlockUser()
            
            if(self.user!.isFollowedByMe()){
                self.user!.toggleFollowUser()
            }
            print("Handle Ok logic here")
        }))
        
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
            deleteAlert.dismissViewControllerAnimated(true, completion: nil)
            
            print("Handle Cancel Logic here")
        }))
        presentViewController(deleteAlert, animated: true, completion: nil)
    }
    
    @IBAction func followersButtonWasHit(sender: AnyObject) {
        if(user!.isBlockingMe() == false){
            performSegueWithIdentifier("segueToPeople", sender: followersButton)
        }else{
            
        }
    }
    
    @IBAction func followingButtonWasHit(sender: AnyObject) {
        if(user!.isBlockingMe() == false){
            performSegueWithIdentifier("segueToPeople", sender: followingButton)
        }else{
        
        }
    }


    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        if let des = segue.destinationViewController as? DetailViewController {
            des.event = sender as? Event
        }
        if let des = segue.destinationViewController as? ProfileViewController {
            let user = sender as! PFEUser
            des.user = user
        }
        if let des = segue.destinationViewController as? AddMouveViewController {
            des.hidesBottomBarWhenPushed = true;
        }

        if let des = segue.destinationViewController as? SignupViewController {
            des.editMode = true
        }
        if let des = segue.destinationViewController as? PeopleFeedViewController {
            des.pageType = ((sender as! UIButton) == followingButton ? PeopleType.Following : PeopleType.Followers)
            des.user = user
        }
 
    }
    
    @IBAction func editProfileOrFollowButtonWasHit(sender: AnyObject) {
        if (user != PFEUser.currentUser()){
            if(!(user!.isBlockingMe()) && !(user!.isBlockedByMe())){
                SVProgressHUD.showSuccessWithStatus(user!.isFollowedByMe() ? "Unfollowing..." : "Following..." )
                self.user!.toggleFollowUser()
                
                
            }else if(!(user!.isBlockingMe()) && user!.isBlockedByMe()){
                let unblockAlert = UIAlertController(title: "You Blocked This User", message: "Following this user will unblock them.", preferredStyle: UIAlertControllerStyle.Alert)
                unblockAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
                    self.user?.toggleBlockUser()
                    self.user!.toggleFollowUser()
                    print("Handle Ok logic here")
                }))
                unblockAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
                    unblockAlert.dismissViewControllerAnimated(true, completion: nil)
                    
                    print("Handle Cancel Logic here")
                }))
                presentViewController(unblockAlert, animated: true, completion: nil)
                
            }else{
                
            }
        }else{
            performSegueWithIdentifier("segueToEdit", sender: self)
        }
    }

    @IBAction func moreBtnWasHit(sender: AnyObject) {
        print("Popped up..", terminator: "")
        let alert:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let reportAction = UIAlertAction(title: "Report User", style: UIAlertActionStyle.Destructive){
            UIAlertAction in
            self.reportUser()
            
        }
        let blocktitle: String = (user!.isBlockedByMe() ? "Unblock User" : "Block User" )
        let blockAction = UIAlertAction(title: blocktitle, style: UIAlertActionStyle.Destructive){
            UIAlertAction in
//            SVProgressHUD.show()
            self.blockAlert()
        }

        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel){
            UIAlertAction in
        }
        // Add the actions
        alert.addAction(reportAction)
        alert.addAction(blockAction)
        alert.addAction(cancelAction)
        // Present the actionsheet
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            popoverMenu=UIPopoverController(contentViewController: alert)
            popoverMenu!.presentPopoverFromRect(moreButton!.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        }
        
    }
    
    
    @IBAction func backButtonHit(sender: AnyObject) {
        print("\n going back \n")
        self.navigationController?.popViewControllerAnimated(true)
    }
}




    
