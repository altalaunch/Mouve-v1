//
//  FollowersTableViewCell.swift
//  The Mouve
//
//  Created by Samuel Ifeanyi Ojogbo Jr. on 7/18/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation
import UIKit
import Parse
import Bond
import SVProgressHUD



class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var followButton: smallTextOutlinedButton!
    @IBOutlet weak var profileImage: UIImageView!
    
    var feedCollectionView: UICollectionView? = nil
    var userDisposable: DisposableType?
    var followDisposable: DisposableType?
    var delegate: SearchTableViewCellDelegate?
    var isMyUser = false
    var user: PFEUser! {
        didSet {
            user!.fetchIfNeededInBackgroundWithBlock { (result: PFObject?, error: NSError?) -> Void in
                if let user = result as? PFEUser{
                    self.fullNameLabel.text = user.fullName
                    self.usernameLabel.text = "@\(user.username!)"
                    self.userDisposable?.dispose()
                    self.followDisposable?.dispose()
                    if let oldValue = oldValue where oldValue != user {
                        oldValue.thumbProfImg.value = nil
                    }
                    self.userDisposable = self.profileImage.bindAndRound(self.user!.thumbProfImg)

                }
            }
            self.displayValue()
            profileImage.userInteractionEnabled = true
            usernameLabel.userInteractionEnabled = true
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("goToProfileWasTapped:"))
            let tapUsernameGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("goToProfileWasTapped:"))
            profileImage.addGestureRecognizer(tapGestureRecognizer)
            usernameLabel.addGestureRecognizer(tapUsernameGestureRecognizer)
        }
    }

    
    func displayValue(){

        if(user == PFEUser.currentUser()){
            followButton.hidden = true
            followButton.userInteractionEnabled = false
        }
        else{
            followButton.hidden = false
            followButton.userInteractionEnabled = true
            followDisposable = user?.followers.observe { (value: [PFEUser]?) -> () in
                // 3
                if let value = value {
                    self.followButton.completed = value.contains(PFEUser.currentUser()!)
                    if(self.followButton.completed!){
                        self.followButton.setTitle("Following", forState: UIControlState.Normal)
                        self.followButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    }
                    else{
                        self.followButton.setTitle("Follow", forState: UIControlState.Normal)
                        self.followButton.setTitleColor(UIColor.seaFoamGreen(), forState: .Normal)
                    }
                }
                else{
                    self.followButton.completed = false
                    self.followButton.setTitle("Follow", forState: UIControlState.Normal)
                }
            }

        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        usernameLabel.textColor = UIColor.nicePaleBlue()

        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func goToProfileWasTapped(recognizer: UITapGestureRecognizer){
        delegate?.didTapGoToProfile(self)
    }
    
    @IBAction func followerButtonWasHit(sender: AnyObject) {
        print("you're following", terminator: "")
        self.delegate?.didTapFollowButton(self)
    }
}

protocol SearchTableViewCellDelegate {
    func didTapGoToProfile(cell: SearchTableViewCell)
    func didTapFollowButton(cell: SearchTableViewCell)
}