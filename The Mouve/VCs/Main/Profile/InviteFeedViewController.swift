//
//  InviteFeedViewController.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 9/20/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import CoreGraphics
import Parse
import Bond
import DZNEmptyDataSet
import SVProgressHUD

class InviteFeedViewController: UIViewController,FeedComponentTarget, InviteTableViewCellDelegate {
    
    @IBOutlet weak var feedTableView: UITableView?
    @IBOutlet weak var selectAllButton: UIButton!
    @IBOutlet weak var attendingList: UIButton!
    @IBOutlet weak var selectAllLabel: UILabel!
    
    
    let feedCollectionView: UICollectionView? = nil
    let defaultRange = 0...19
    let additionalRangeSize = 10
    var newEvent = true
    var feedComponent: FeedComponent<PFEUser, InviteFeedViewController>!
    var event: Event!
    var selectedUsers : [PFEUser]? = []

    
    func loadInRange(range: Range<Int>, feedMode: FeedMode, completionBlock: ([PFEUser]?) -> Void) {
        // 1
        print("Followers Page")
        ParseUtility.findFollowersInBackground(PFEUser.currentUser()!, range: range){(data, error) -> () in
            let users = data as? [PFEUser] ?? []
            if(users.count == 0){
                self.feedTableView!.tableFooterView = UIView()
                self.feedTableView!.emptyDataSetDelegate = self
                self.feedTableView!.emptyDataSetSource = self
            }else{
                self.event!.fetchInvitees()
                
            }
            completionBlock(users)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show()
        if(!newEvent){
            attendingList.setTitle("Done", forState: .Normal)
            selectAllButton.hidden = true
            selectAllButton.userInteractionEnabled = false
            selectAllLabel.hidden = true
        }
        feedComponent = FeedComponent(target: self, feedMode: FeedMode.RefreshableTable)
        feedComponent.loadInitialIfRequired()
        SVProgressHUD.dismiss()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
            feedComponent.refresh(self, changeOffset: false)
    }

    @IBAction func selectAllCheckboxWashit(sender: AnyObject) {
        if(!newEvent){
//            Select all for existing event
        }
        else{
            selectAllForNewEvent()
        }
    }
    
    func selectAllForNewEvent(){
        if (self.selectAllButton.selected) {
            // UnInvite
            self.selectAllButton.selected = false;
            self.selectedUsers?.removeAll()
        }
        else {
            // Invite
            self.selectAllButton.selected = true;
            
            
        }
        self.feedTableView?.reloadData()
    }
    
    @IBAction func inviteBtnWasHit(sender: AnyObject) {
        if(!newEvent){
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        else{
            self.inviteToNewEvent()
        }
    }
    
    func inviteToNewEvent(){
        SVProgressHUD.show()

        self.event!.saveInBackgroundWithBlock{ (succeeded: Bool, error: NSError?) -> Void in
            if let error = error {
                SVProgressHUD.dismiss()

                let errorString = error.userInfo["error"] as? NSString
                self.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
            }else if succeeded{
                if(((self.selectedUsers.value != nil) ? self.selectedUsers.value!.count : 0)  > 0){
                    ParseUtility.inviteUsersToMouveInBackground(self.event!, targetUsers: self.selectedUsers.value!, onCompletion: { (succeeded, error) -> () in
                        if let error = error {
                            SVProgressHUD.dismiss()
                            let errorString = error.userInfo["error"] as? NSString
                            self.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
                        }else{
                            SVProgressHUD.dismiss()
                            self.navigationController?.popToRootViewControllerAnimated(true)
                        }
                    })
                }
            }
        }
    }

    func backBtnWasHit(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    

    func didTapCheckbox(cell: InviteTableViewCell){
        if(!newEvent){
            event.toggleInvite(cell.user!)
        }else{
            checkboxForNewEvent(cell)

        }
    }
    func checkboxForNewEvent(cell: InviteTableViewCell){
        if (self.selectAllButton.selected){
            selectedUsers!.removeAtIndex((selectedUsers.value?.indexOf(cell.user!)!)!)
            cell.inviteCheckbox.completed = false
            self.selectAllButton.selected = false

        }else{
            
            if (cell.inviteCheckbox.completed!){
                selectedUsers!.removeAtIndex((selectedUsers.value?.indexOf(cell.user!)!)!)
                event.invitees.value = selectedUsers
                cell.inviteCheckbox.completed = false
            }
            else{
                selectedUsers!.append(cell.user!)
                event.invitees.value = selectedUsers
                cell.inviteCheckbox.completed = true
            }
        }
    }
    func displayCheckboxForExistingEvent(cell: InviteTableViewCell) -> DisposableType?{
        var disposable: DisposableType?
            disposable =
                event.invitees.observe { (value: [PFEUser]?) -> () in
                // 3
                    if let value = value {
                        print(value)
                        cell.inviteCheckbox.completed = value.contains(cell.user!)
                    }
                    else{
                        cell.inviteCheckbox.completed = false
                    }
                }

        return disposable
    }

    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = false
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
    }

}

extension InviteFeedViewController : DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "Following"
        return NSAttributedString(string:text)
        
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "No one seems to follow you... Become more active!"
        
        return NSAttributedString(string: text, attributes: [
            NSForegroundColorAttributeName: UIColor.grayColor()
            ])
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "profile")
    }
}

extension InviteFeedViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return feedComponent.content.count
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        feedComponent.targetWillDisplayEntry(indexPath.section)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("inviteListCellID") as? InviteTableViewCell
        
        let userDetails = feedComponent.content[indexPath.section]
        userDetails.dlPfImg()
        cell!.delegate = self
        cell!.user = userDetails
        if (self.selectAllButton.selected == true){
            cell!.inviteCheckbox.completed = true
            selectedUsers!.append(cell!.user!)
        }


        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        feedTableView!.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        return 1
    }
    

}
