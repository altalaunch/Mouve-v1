//
//  ProfileTableViewController.swift
//  The Mouve
//
//  Created by Hilal Habashi on 9/30/15.
//  Copyright © 2015 The Mouve. All rights reserved.
//

import Foundation
import UIKit
import DZNEmptyDataSet

extension ProfileViewController : DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        print("trying to add mouve", terminator: "")
        performSegueWithIdentifier("addMouve", sender: self)
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let text = (self.pfType == .MyProfile ? "" : "")
            return NSAttributedString(string:text)
    }
    
    func emptyDataSetWillAppear(scrollView: UIScrollView!) {
       self.feedTableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let modelName = UIDevice.currentDevice().modelName
        
        if ((modelName == "iPhone 5")||(modelName == "iPhone 5c")||(modelName == "iPhone 5s") || (modelName == "iPod Touch 5")||(modelName == "iPod Touch 6")){
            let text = (self.pfType == .MyProfile ? "\n\n\n\n\n\n\n\n Create or attend some Mouves." : "\n\n\n\n\n\n\n\n It seems  @\(user!.username!) isn't a part of any public Mouves")

            return NSAttributedString(string: text, attributes: [
                NSForegroundColorAttributeName: UIColor.grayColor()
                ])
        }else if((modelName == "iPhone 6")||(modelName == "iPhone 6 Plus")||(modelName == "iPhone 6s")||(modelName == "iPhone 6s Plus")){
            let text = (self.pfType == .MyProfile ? "\n\n\n\n Create or attend some mouves." : "\n\n\n\n It seems  @\(user!.username!) isn't a part of any public Event")
            return NSAttributedString(string: text, attributes: [
                NSForegroundColorAttributeName: UIColor.grayColor()
                ])
        }else{
            let text = (self.pfType == .MyProfile ? "" : "")
            return NSAttributedString(string: text, attributes: [
                NSForegroundColorAttributeName: UIColor.grayColor()
                ])
        }
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "")
    }
    
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource, SceneProfileCellDelegate, SceneFeedViewCellDelegate {
    func didTapProfileImage(cell: SceneFeedViewCell) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("profileVC") as! ProfileViewController
        
        vc.user = cell.event!.creator

        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func didTapAttendEvent(cell: SceneFeedViewCell) {
        //        load attending HUD over here
        let dateNow = NSDate()
        
        if(dateNow.compare(cell.event.endTime) == .OrderedAscending){
            cell.event.toggleAttendEvent(PFEUser.currentUser()!)
        }else{
            self.presentViewController(UIAlertController(title: "Mouve Ended", message: "You cannot attend or Unattend a mouve that has ended."), animated: true, completion: nil)
        }
    }
    func didTapShareEvent(cell: SceneFeedViewCell) {
        //        load share screen and HUD over here
    }
    func belongsToUser(cell: SceneFeedViewCell) -> Bool{
        if(cell.event.creator == user){
            return true
        }
        else{
            return false
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.feedComponent.content.count

    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("detailVC") as! DetailViewController
        vc.event = self.feedComponent.content[indexPath.section]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        feedComponent.targetWillDisplayEntry(indexPath.section)
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //        let cell = SceneFeedViewCell()
        let cell = tableView.dequeueReusableCellWithIdentifier("cellID") as? SceneFeedViewCell
        let event = self.feedComponent.content[indexPath.section]
        
        event.dlBgThumb()
        event.creator.dlPfImg()
        event.fetchAttendees()
        cell!.delegate = self
        cell!.profileDel = self
        cell!.event = event
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0 : 4
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        var avatarTransform = CATransform3DIdentity
        var headerTransform = CATransform3DIdentity
        var nameLabelTransform = CATransform3DIdentity
        var separationLabelTansform = CATransform3DIdentity
        
        print(offset, terminator: "")
        
        
        if offset < 0 { // PULL DOWN
            
            let headerScaleFactor:CGFloat = -(offset) / headerView.bounds.height
            let headerSizevariation = ((headerView.bounds.height * (1.0 + headerScaleFactor)) - headerView.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
//            nameLabelTransform = CATransform3DMakeScale(1.0, 1.0, 0)

            separationLabelTansform = CATransform3DTranslate(separationLabelTansform, 1.0, headerSizevariation, 0)
            separationLabelTansform = CATransform3DScale(separationLabelTansform, 1.5 + headerScaleFactor, 1.5 + headerScaleFactor, 0)
            
            separationLabel.layer.transform = separationLabelTansform

            headerView.layer.transform = headerTransform
//            nameLabel.layer.transform = nameLabelTransform
            
            

        }
            
        else { // PUSH UP
            
            // Header -----------
            
            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
            
            // Avatar -----------
            
            let avatarScaleFactor = (min(offset_HeaderStop, offset)) / avatarImage.bounds.height / 1.4 // Slow down the animation
            let avatarSizeVariation = ((avatarImage.bounds.height * (1.0 + avatarScaleFactor)) - avatarImage.bounds.height) / 2.0
            avatarTransform = CATransform3DTranslate(avatarTransform, 0, avatarSizeVariation, 0)
            avatarTransform = CATransform3DScale(avatarTransform, 1.0 - avatarScaleFactor, 1.0 - avatarScaleFactor, 0)
            
            if offset <= offset_HeaderStop {
                
                if avatarImage.layer.zPosition < headerView.layer.zPosition{
                    headerView.layer.zPosition = 0

                    // Makes the Fullname appear
                    nameLabelTransform = CATransform3DMakeScale(1.0, 1.0, 0)
                    nameLabel.layer.transform = nameLabelTransform

                }
                
            } else {
                if avatarImage.layer.zPosition >= headerView.layer.zPosition{
                    headerView.layer.zPosition = 2

                    
                    // Makes the Fullname disappear
                    nameLabelTransform = CATransform3DMakeScale(0, 0, 0)
                    nameLabel.layer.transform = nameLabelTransform
//                    separationLabel.layer.transform = nameLabelTransform
                }
            }
        }
        
        // Apply Transformations
        headerView.layer.transform = headerTransform
        separationLabel.layer.transform = headerTransform
        
        avatarImage.layer.transform = avatarTransform

        
    }
}