//
//  FollowersTableViewCell.swift
//  The Mouve
//
//  Created by Samuel Ifeanyi Ojogbo Jr. on 7/18/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation
import UIKit
import Parse
import Bond


class InviteTableViewCell: UITableViewCell {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var inviteCheckbox: smallTextOutlinedButton!
    var userDisposable: DisposableType?
    var inviteDisposable: DisposableType?
//    var checked: Observable<Bool?> = Observable(nil)
    
    
    @IBOutlet weak var profileImage: UIImageView!
    var delegate: InviteTableViewCellDelegate?


    var user: PFEUser? {
        didSet {
            user!.fetchIfNeededInBackgroundWithBlock { (result: PFObject?, error: NSError?) -> Void in
                if let user = result as? PFEUser{
                    
                    self.fullNameLabel.text = user.fullName
                    self.usernameLabel.text = "@\(user.username!)"
                    self.userDisposable?.dispose()
                    self.inviteDisposable?.dispose()
                    if let oldValue = oldValue where oldValue != user {
                        oldValue.thumbProfImg.value = nil
                    }
                    self.userDisposable = self.profileImage.bindAndRound(self.user!.thumbProfImg)
                    self.inviteDisposable = self.delegate!.displayCheckboxForExistingEvent(self)
                }
            }
            inviteDisposable = self.delegate!.displayCheckboxForExistingEvent(self)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        inviteCheckbox.layer.borderWidth = 1
    }

    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func inviteBtnWasHit(sender: AnyObject) {
        print("you invited them", terminator: "")
        delegate?.didTapCheckbox(self)
    }
}

protocol InviteTableViewCellDelegate {
    func didTapCheckbox(cell: InviteTableViewCell)
//    func checkboxForNewEvent(cell: InviteTableViewCell)
    func displayCheckboxForExistingEvent(cell: InviteTableViewCell) -> DisposableType?
}