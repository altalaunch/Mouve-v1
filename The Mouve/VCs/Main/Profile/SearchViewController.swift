//
//  ProfileViewController.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 9/20/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import CoreGraphics
import Parse
import DZNEmptyDataSet
import SVProgressHUD


class SearchFeedViewController: UIViewController, FeedComponentTarget {
    
    @IBOutlet weak var feedTableView: UITableView?
    @IBOutlet weak var searchBar: UISearchBar!
    
    let feedCollectionView: UICollectionView? = nil
    let defaultRange = 0...19
    let additionalRangeSize = 10
    var feedMode = FeedMode.EndlessTable
    var feedComponent: FeedComponent<PFEUser, SearchFeedViewController>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Search Bar Appearance
        
        feedComponent = FeedComponent(target: self, feedMode: feedMode)
        searchBar.backgroundColor = UIColor.whiteColor()
        searchBar.tintColor = UIColor.seaFoamGreen()
        searchBar.barTintColor = UIColor.whiteColor()
        searchBar.placeholder = "Search Friends"
        PFEUser.currentUser()?.fetchBlocked()
        PFEUser.currentUser()?.fetchBlockingMe()

    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        self.navigationController?.navigationBarHidden = false

    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
    }

}

extension SearchFeedViewController : DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "Search"
        return NSAttributedString(string:text)
        
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "Nothing reults to display"
        
        return NSAttributedString(string: text, attributes: [
            NSForegroundColorAttributeName: UIColor.grayColor()
            ])
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "mouve-icon")
    }
    
}

extension SearchFeedViewController: UITableViewDataSource, UISearchBarDelegate, UITableViewDelegate {

    // MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return feedComponent.content.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("searchListCellID") as? SearchTableViewCell
        let userDetails = feedComponent.content[indexPath.section]
        userDetails.dlPfImg()
        userDetails.fetchFollowing()
        cell!.user = userDetails
        cell!.delegate = self
        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        feedTableView!.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        return 1
    }
    
    // MARK: UISearchBarDelegate
    func searchBarSearchButtonClicked(searchBar: UISearchBar) // called when keyboard search button pressed
    {
        searchBar.resignFirstResponder()
        feedComponent = FeedComponent(target: self, feedMode: feedMode)
        feedComponent.loadInitialIfRequired()
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        feedComponent.targetWillDisplayEntry(indexPath.section)
    }

    func loadInRange(range: Range<Int>, feedMode: FeedMode, completionBlock: ([PFEUser]?) -> Void) {

        ParseUtility.searchUserQuery(searchBar.text!, range: range) { (data: [PFObject]?, error: NSError?) -> Void in
            let users = data as? [PFEUser] ?? []
            if(users.count == 0){
                self.feedTableView!.tableFooterView = UIView()
            }
            completionBlock(users)
        }
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar) // called when cancel button pressed
    {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        
    }

}

extension SearchFeedViewController: SearchTableViewCellDelegate {
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)

        if let des = segue.destinationViewController as? ProfileViewController {
            let user = sender as! PFEUser
            des.user = user
        }

    }
    
    
    func didTapFollowButton(cell: SearchTableViewCell) {
        
        if(!(cell.user!.isBlockingMe()) && !(cell.user!.isBlockedByMe())){
            SVProgressHUD.showSuccessWithStatus(cell.user!.isFollowedByMe() ? "Unfollowing..." : "Following..." )
            cell.user?.toggleFollowUser()
        }else if(!(cell.user!.isBlockingMe()) && cell.user!.isBlockedByMe()){
            let unblockAlert = UIAlertController(title: "You Blocked This User", message: "Following this user will unblock them.", preferredStyle: UIAlertControllerStyle.Alert)
            unblockAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
                cell.user?.toggleBlockUser()
                cell.user!.toggleFollowUser()
                print("Handle Ok logic here")
            }))
            unblockAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
                unblockAlert.dismissViewControllerAnimated(true, completion: nil)
                
                print("Handle Cancel Logic here")
            }))
            presentViewController(unblockAlert, animated: true, completion: nil)
            
        }else{
        }
    }
    
    func didTapGoToProfile(cell: SearchTableViewCell) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("profileVC") as! ProfileViewController
        
        vc.user   = cell.user
        vc.pfType = ProfileType.OtherUser
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}