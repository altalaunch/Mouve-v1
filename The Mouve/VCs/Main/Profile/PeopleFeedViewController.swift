//
//  PeopleFeedViewController.swift
//  The Mouve
//
//  Created by Samuel Ifeanyi Ojogbo Jr. on 8/27/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation
import Parse
import Bolts
import DZNEmptyDataSet
import SVProgressHUD

enum PeopleType{
    case Followers
    case Following
    case Attendees
}

class PeopleFeedViewController: UIViewController,FeedComponentTarget{

    @IBOutlet weak var feedTableView: UITableView?
    
    let feedCollectionView: UICollectionView? = nil
    let defaultRange = 0...15
    let additionalRangeSize = 10
    var feedComponent: FeedComponent<PFEUser, PeopleFeedViewController>!
    var pageType: PeopleType!
    var toFromUser:String?
    var event:Event?
    var user:PFEUser?
    
    func loadInRange(range: Range<Int>, feedMode: FeedMode, completionBlock: ([PFEUser]?) -> Void) {
        // 1
        switch pageType{
        case PeopleType.Followers?:
            print("Followers Page")
            self.title = "Followers"
            ParseUtility.findFollowersInBackground(user!, range: range){(data, error) -> () in
                let users = data as? [PFEUser] ?? []
                if(users.count == 0){
                    self.feedTableView!.tableFooterView = UIView()
                    self.feedTableView!.emptyDataSetDelegate = self
                    self.feedTableView!.emptyDataSetSource = self
                }
                completionBlock(users)
            }
            
        case PeopleType.Following?:
            print("Following Page")
            self.title = "Following"
            ParseUtility.findFolloweesInBackground(user!, range: range){(data, error) -> () in
                let users = data as? [PFEUser] ?? []
                if(users.count == 0){
                    self.feedTableView!.tableFooterView = UIView()
                    self.feedTableView!.emptyDataSetDelegate = self
                    self.feedTableView!.emptyDataSetSource = self
                }
                completionBlock(users)
            }
        case PeopleType.Attendees?:
            self.title = "People Attending"
            
            ParseUtility.findAttendeesInBackground(event!, range: range){(data, error) -> () in
                let users = data as? [PFEUser] ?? []
                if(users.count == 0){
                    self.feedTableView!.tableFooterView = UIView()
                    self.feedTableView!.emptyDataSetDelegate = self
                    self.feedTableView!.emptyDataSetSource = self
                }

                completionBlock(users)
            }
            
        default:
            break;
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        feedComponent = FeedComponent(target: self,feedMode: FeedMode.RefreshableTable)
        feedTableView?.separatorColor = UIColor.seaFoamGreen()
        PFEUser.currentUser()?.fetchBlocked()
        PFEUser.currentUser()?.fetchBlockingMe()
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        feedComponent.loadInitialIfRequired()
        self.navigationController?.navigationBar.tintColor = UIColor.nicePaleBlue()
    }
    
    
}
extension PeopleFeedViewController : DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let text = (pageType == PeopleType.Following ? "Following" : "Followers...")
        return NSAttributedString(string:text)
        
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        if(user == PFEUser.currentUser()){
        let text = (pageType == PeopleType.Following ? "You doesn't seem to follow anyone" : "No one follows you...")
            return NSAttributedString(string: text, attributes: [
                NSForegroundColorAttributeName: UIColor.grayColor()
                ])
        }else{
            let text = (pageType == PeopleType.Following ? "This user doesn't seem to follow anyone" : "No one follows this user...")
            return NSAttributedString(string: text, attributes: [
                NSForegroundColorAttributeName: UIColor.grayColor()
                ])
        }
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "profile")
    }
}

extension PeopleFeedViewController: UITableViewDataSource,UITableViewDelegate {
    
    // MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return feedComponent.content.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        feedComponent.targetWillDisplayEntry(indexPath.section)
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:PeopleTableViewCell = tableView.dequeueReusableCellWithIdentifier("userListCellID") as! PeopleTableViewCell
        let userDetails = feedComponent.content[indexPath.section]
        userDetails.dlPfImg()
        userDetails.fetchFollowing()
        cell.user = userDetails
        cell.delegate = self

        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
}

extension PeopleFeedViewController: PeopleTableViewCellDelegate {
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        if let des = segue.destinationViewController as? ProfileViewController {
            let user = sender as! PFEUser
            des.user = user
        }
    }
    
    
    func didTapFollowButton(cell: PeopleTableViewCell) {
        
        if(!(cell.user!.isBlockingMe()) && !(cell.user!.isBlockedByMe())){
            SVProgressHUD.showSuccessWithStatus(cell.user!.isFollowedByMe() ? "Unfollowing..." : "Following..." )
            cell.user?.toggleFollowUser()
        }else if(!(cell.user!.isBlockingMe()) && cell.user!.isBlockedByMe()){
            let unblockAlert = UIAlertController(title: "You Blocked This User", message: "Following this user will unblock them.", preferredStyle: UIAlertControllerStyle.Alert)
            unblockAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
                cell.user?.toggleBlockUser()
                cell.user!.toggleFollowUser()
                print("Handle Ok logic here")
            }))
            unblockAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
                unblockAlert.dismissViewControllerAnimated(true, completion: nil)
                
                print("Handle Cancel Logic here")
            }))
            presentViewController(unblockAlert, animated: true, completion: nil)
            
        }else{
        }
    }
    
    func didTapGoToProfile(cell: PeopleTableViewCell) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("profileVC") as! ProfileViewController
        
        vc.user   = cell.user
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
        