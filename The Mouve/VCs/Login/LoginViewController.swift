//
//  LoginViewController.swift
//  The Mouve
//
//  Created by Hilal Habashi on 7/12/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import Parse
import SVProgressHUD
import ParseFacebookUtilsV4
import FBSDKLoginKit


class LoginViewController: UIViewController {
    @IBOutlet weak var emailTextField: UnderlinedTextField!
    @IBOutlet weak var passwordTextField: UnderlinedTextField!
    @IBOutlet weak var fbLoginButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var facebookLoginButton: FBSDKLoginButton!
    @IBOutlet weak var resetPasswordButton: UIButton!
    
    
    var fbAccToken: FBSDKAccessToken? = nil
    var fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
    let permissions = ["public_profile"]
//    var userFBId: String = ""
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        statusBar(.Default)
        addTextDismiss()
        addNavControllerLikePan()
        fbLoginButton.layer.cornerRadius = 5
        fbLoginButton.clipsToBounds = true
        fbLoginButton.layer.borderWidth = 1
        fbLoginButton.layer.borderColor = UIColor.nicePaleBlue().CGColor
        [emailTextField, passwordTextField].map({ $0.delegate = self })
    }
    
    @IBAction func loginButtonWasHit(sender: AnyObject) {
        view.userInteractionEnabled = false
        SVProgressHUD.show()
        let emailQuery = PFEUser.query()
        emailQuery!.whereKey("email", equalTo:emailTextField.text!.lowercaseString)
        emailQuery!.whereKey("disabled", notEqualTo: true)
        emailQuery!.getFirstObjectInBackgroundWithBlock({ (object, error) in
            if let object = object {
                ParseLoginUtility.loginWithUsername(object["username"] as! String, password: self.passwordTextField.text!, fbAccToken: FBSDKAccessToken.currentAccessToken())
            } else {
                if let error = error {
                    SVProgressHUD.dismiss()
                    var displayMessage = ""
                    
                    if(error.code == 101){
                        displayMessage = "This account does not exist or it has been disabled for not following our Community Guidelines"
                    }else{
                        displayMessage = error.localizedDescription
                    }
                    self.presentViewController(UIAlertController(title: "Uh oh", message: displayMessage), animated: true, completion: nil)
                }
            }
        })
        view.userInteractionEnabled = true
    }
    
    @IBAction func signUpButtonWasHit(sender: AnyObject) {
        let signUpVC = self.initVC("signUpVC", storyboard: "Login") as! SignupViewController
        
        var navigationStack = self.navigationController!.viewControllers
        navigationStack.removeLast()
        navigationStack.append(signUpVC)
        self.navigationController!.setViewControllers(navigationStack, animated: true)
    }
    
    @IBAction func resetPasswordButtonWasHit(sender: AnyObject) {
        let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewControllerWithIdentifier("passwordRecoveryVC") as! PasswordRecoveryViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }


    @IBAction func fbLoginButtonWasHit(sender: AnyObject) {
        func loginWithFacebook(){
            let vc = appDel.window?.rootViewController
            FBSDKGraphRequest(graphPath: "me", parameters:["fields":"email,name"]).startWithCompletionHandler({ (connection, result, error) in
                if error != nil {
                    vc!.presentViewController(UIAlertController(title: "Whoops!", message: error!.localizedDescription), animated: true, completion: nil)
                } else {
                    PFFacebookUtils.logInInBackgroundWithAccessToken(FBSDKAccessToken.currentAccessToken()){ (user:PFUser?, error:NSError?) -> Void in
                        if(error != nil)
                        {
                            vc!.presentViewController(UIAlertController(title: "Whoops!", message: error!.localizedDescription), animated: true, completion: nil)
                            return
                        }
                        if(user == nil){
                            print("Canceled login in the middle of the facebook process")
                        }
                        else if(user!.isNew){
                            ParseLoginUtility.signUpOrLinkDialog(user)
                        }
                        else if(user != nil){
                            print("Logged in with \(FBSDKAccessToken.currentAccessToken().userID)")
//                            if let disabled = user!["disabled"] as? Bool{
//                                if(disabled){
//                                    appDel.logOut()
//                                }else{
                                    appDel.checkLogin()
//                                }
//                            }
                        }
                    }
                }
            })
        }
        if(FBSDKAccessToken.currentAccessToken() != nil){
            loginWithFacebook()
        }else{
            fbLoginManager.logInWithReadPermissions(self.permissions, fromViewController: self.parentViewController, handler: { (result, error) in
                if error != nil {
                    self.presentViewController(UIAlertController(title: "Oops...", message: error!.localizedDescription), animated: true, completion: nil)
                }
                else if (result != nil) {
                    loginWithFacebook()
                }
            })
        }
        

        
    }
    
    @IBAction func backButtonWasHit(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        view.endEditing(true)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            emailTextField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            passwordTextField.resignFirstResponder()
            loginButtonWasHit(self)
        default: ()
        }
        
        return true
    }
}