//
//  LoginViewController.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 7/12/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import Parse
import SVProgressHUD
import ParseFacebookUtilsV4
import FBSDKLoginKit


class PasswordRecoveryViewController: UIViewController, UITextFieldDelegate
{
    
    @IBOutlet weak var emailTextField: UnderlinedTextField!
    @IBOutlet weak var emailSentLabel: UILabel!
    @IBOutlet weak var sendEmailButton: OutlinedButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        statusBar(.Default)
        emailSentLabel.hidden = true
        emailTextField.delegate = self

        
        
    }
    
    func displayMessage(theMessage: String){
        
        //Display Alert message with confirmation
        
        let myAlert = UIAlertController(title: "Uh oh!", message: theMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
            action in
            self.dismissViewControllerAnimated(true, completion: nil)
            
        }
        
        myAlert.addAction(okAction)
        self.presentViewController(myAlert, animated: true, completion: nil)
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            emailTextField.resignFirstResponder()
            sendEmailButtonWasHit(self)
        default: ()
        }
        
        return true
    }
    
    @IBAction func sendEmailButtonWasHit(sender: AnyObject) {
        
        let userEmail = emailTextField.text!
        
        PFEUser.requestPasswordResetForEmailInBackground(userEmail) { (success: Bool, error: NSError?) -> Void in
            if (success){
                let successMessage = "An email has been sent to \(userEmail). Please check your email  to reset your password and continue."
                
                self.emailSentLabel.text = successMessage
                self.sendEmailButton.hidden = true
                self.sendEmailButton.userInteractionEnabled = false
                self.emailSentLabel.hidden = false

                return
            }
            
            if (error != nil){
                let errorMessage = error!.userInfo["error"] as! String
                self.displayMessage(errorMessage)
                
            }
        }
        
    }
    
    
    @IBAction func backButtonWasHit(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func searchButtonWasHit(sender: AnyObject) {
    }
    
    

}


