//
//  EditUserSignupVC.swift
//  The Mouve
//
//  Created by Hilal Habashi on 9/25/15.
//  Copyright © 2015 Hilal Habashi. All rights reserved.
//

import Foundation
import Parse
import SVProgressHUD


extension SignupViewController {
    
    func showViewForEditing(){
        self.navigationController?.navigationBarHidden = true
        self.createAccountButton.setTitle("Save", forState: UIControlState.Normal)
        self.newPasswordTextField?.delegate=self
        self.usernameTextField.text = PFEUser.currentUser()!.username
        self.passwordTextField.text = PFEUser.currentUser()!.password
        self.nameTextField.text = PFEUser.currentUser()!.fullName
        self.emailTextField.text = PFEUser.currentUser()!.email
        PFEUser.currentUser()!.thumbProfImg.bindTo(self.pickedPic)
    }
    
    func generateImageData(originalImg: UIImage, fullSize: CGFloat, thumbSize: CGFloat) -> (full: NSData,thumbnail: NSData){
        let full = ImageCropper.squareCropImageToSideLength(originalImg, toSize: fullSize)
        let thumbnail = ImageCropper.squareCropImageToSideLength(originalImg, toSize: thumbSize)
        return (UIImageJPEGRepresentation(full,0.7)!,UIImageJPEGRepresentation(thumbnail, CGFloat(0.3))!)
    }
    
    func updateUserProfile(){
        SVProgressHUD.show()

        PFEUser.currentUser()!.username = self.usernameTextField.text
        if(self.passwordTextField.text != "" && self.newPasswordTextField.text != ""){
            PFEUser.currentUser()!.password = self.newPasswordTextField.text
        }
        PFEUser.currentUser()!.fullName = self.nameTextField.text!
        PFEUser.currentUser()!.email = self.emailTextField.text
        if(self.picChanged){
            let imgData = generateImageData(self.pickedPic.value!, fullSize: 1000, thumbSize: 100)
            PFEUser.currentUser()!.profileImage = PFFile(name: "pfImg.jpg",data: imgData.full)
            PFEUser.currentUser()!.thumbnailImg = PFFile(name: "thumbnail.jpg",data: imgData.thumbnail)
        }
        PFEUser.currentUser()!.saveInBackgroundWithBlock {( succeeded: Bool, error: NSError?) -> Void in
            if let error = error {
                let errorString = error.userInfo["error"] as? NSString
                self.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
                SVProgressHUD.dismiss()

            }else{
                self.delegate?.updateIfModified()
                ProfileViewController().updateIfModified()
                self.dismissViewControllerAnimated(true, completion: nil)
                self.navigationController?.popViewControllerAnimated(true)
                SVProgressHUD.dismiss()

            }
        }            
    }
    
    
}