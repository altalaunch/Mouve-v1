//
//  SignupViewController.swift
//  The Mouve
//
//  Created by Hilal Habashi on 5/4/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import Parse
import Bond
import ParseFacebookUtilsV4
import FBSDKLoginKit
import FBSDKCoreKit
import SVProgressHUD
import IQKeyboardManagerSwift



class SignupViewController: UIViewController, BWWalkthroughViewControllerDelegate {
    @IBOutlet weak var nameTextField: UnderlinedTextField!
    @IBOutlet weak var usernameTextField: UnderlinedTextField!
    @IBOutlet weak var passwordTextField: UnderlinedTextField!
    @IBOutlet weak var emailTextField: UnderlinedTextField!
    @IBOutlet weak var newPasswordTextField: UnderlinedTextField!
    @IBOutlet weak var fillOutSimpleLabel: UILabel!
    @IBOutlet weak var bySigningUpLabel: UILabel!
    @IBOutlet weak var ampersandLabel: UILabel!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var privacyPolicyButton: UIButton!
    
    
    var pickedPic: Observable<UIImage?> = Observable(nil)
    var editMode = false
    var picChanged = false
    var passTyped = false
    var passChanged = false
    var currentUser: PFEUser? = PFEUser()
    var delegate: EditObjectDelegate?
//    var userFbId: String = ""
    
    @IBOutlet weak var createAccountButton: UIButton!
    
    @IBOutlet weak var fbloginbutton: FBSDKLoginButton!
    @IBOutlet var profileImageButton: UIButton!
    @IBOutlet var facebookBtn: UIButton!
    

    var imagePicker: UIImagePickerController?=UIImagePickerController()
    var popoverMenu: UIPopoverController?=nil


    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        statusBar(.Default)
        addTextDismiss()
        self.navigationController?.navigationBarHidden = true
        addNavControllerLikePan()
        self.pickedPic.observe { (value: UIImage?) -> () in
            self.profileImageButton?.setBackgroundImage(value, forState: .Normal)
        }

        
//        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = ((createAccountButton.frame.origin.y + createAccountButton.frame.height) - nameTextField.frame.origin.y) + 5
        
        [nameTextField, usernameTextField, emailTextField, passwordTextField].map({ $0!.delegate = self })
        self.profileImageButton!.layer.cornerRadius = self.profileImageButton!.frame.height / 2

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.delegate = self

        if(editMode){
            showViewForEditing()

        }else{
            self.pickedPic.value = appDel.placeHolderBg
            self.newPasswordTextField?.hidden = true
        }

    }

    
    func fillFromFb(){
        currentUser = PFEUser.currentUser()
        FBSDKGraphRequest(graphPath: "me", parameters:["fields":"email,name"]).startWithCompletionHandler({ (connection, result, error) in
            if error != nil {
                self.presentViewController(UIAlertController(title: "Whoops!", message: error!.localizedDescription), animated: true, completion: nil)
            } else {
                if let loginResult = result as? Dictionary<String,AnyObject> {
                    dispatch_async(dispatch_get_main_queue(), {
                        if let emailID = loginResult["email"] as? String{
                            self.emailTextField.text = emailID
                        }
                        self.nameTextField.text = loginResult["name"] as? String
                        let userID = loginResult["id"] as! String
                        let facebookProfileUrl = NSURL(string: "https://graph.facebook.com/\(userID)/picture?type=large")
//                        self.userFbId = userID
                        if let data = NSData(contentsOfURL: facebookProfileUrl!) {
                            self.pickedPic.value = UIImage(data: data)
                        }
                    })
                }
            }
        })

    }

    @IBAction func createAccountButtonWasHit(sender: AnyObject) {
        SVProgressHUD.show()
        let fullName = nameTextField.text
        let username = usernameTextField.text
        let password = passwordTextField.text
        let email = emailTextField.text!.lowercaseString
        if (fullName != "" && username != "" && email != "" && editMode){
            if(usernameTextField.text!.lowercaseString != PFEUser.currentUser()!.username?.lowercaseString){
                let usernameQuery = PFEUser.query()!
                usernameQuery.whereKey("username", equalTo: usernameTextField.text!)
                usernameQuery.whereKey("username", matchesRegex: usernameTextField.text!, modifiers: "i")
                usernameQuery.findObjectsInBackgroundWithBlock{ (data: [PFObject]?, error: NSError?) -> Void in
                    let users = data as? [PFEUser] ?? []
                    if(users.count == 0){
                        if(self.passChanged){
                            PFUser.logInWithUsernameInBackground((PFEUser.currentUser()?.username)!, password: password!){ (user: PFUser?, error:NSError?) -> Void in
                                if((error) != nil){
                                    SVProgressHUD.dismiss()
                                    self.presentViewController(UIAlertController(title: "Uh oh!", message: "Incorrect password"), animated: true, completion: nil)
                                }else if(user != nil){
                                    SVProgressHUD.dismiss()
                                    self.updateUserProfile()
                                }
                            }
                        }
                            
                        else{
                            SVProgressHUD.dismiss()
                            self.updateUserProfile()
                        }
                        
                    }else{
                        self.presentViewController(UIAlertController(title: "Uh oh!", message: "Username not available."), animated: true, completion: nil)
                        SVProgressHUD.dismiss()
                        
                    }
                }
            }else{
                if(self.passChanged){
                    PFUser.logInWithUsernameInBackground((PFEUser.currentUser()?.username)!, password: password!){ (user: PFUser?, error:NSError?) -> Void in
                        if((error) != nil){
                            SVProgressHUD.dismiss()
                            self.presentViewController(UIAlertController(title: "Uh oh!", message: "Incorrect password"), animated: true, completion: nil)
                        }else if(user != nil){
                            SVProgressHUD.dismiss()
                            self.updateUserProfile()
                        }
                    }
                }else{
                    SVProgressHUD.dismiss()
                    self.updateUserProfile()
                }
            }
        }
        else if (fullName != "" && username != "" && password != "" && email != "" && !editMode){
                userSignUp()
        }
            
        else {
            self.presentViewController(UIAlertController(title: "Uh oh!", message: "All fields required"), animated: true, completion: nil)
            SVProgressHUD.dismiss()
        }
    }
    
    @IBAction func facebookButtonWasHit(sender: AnyObject) {
        let permissions = [ "public_profile", "email" ]
        
        FBSDKLoginManager().logInWithReadPermissions(permissions, fromViewController: self.parentViewController, handler: { (result, error) in
            if error != nil {
                self.presentViewController(UIAlertController(title: "Nope!", message: error!.localizedDescription), animated: true, completion: nil)
                
            }
            else {
                if((FBSDKAccessToken.currentAccessToken()) != nil){
                        self.fillFromFb()
                } else if result.isCancelled {
                    self.presentViewController(UIAlertController(title: "Uh oh!", message: "We couldn't access facebook! Did you hit cancel?"), animated: true, completion: nil)
                }
            }
        })
    }
    
    func downloadImage(url:NSURL){
        

        NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            if(data != nil){
                dispatch_async(dispatch_get_main_queue()) {

                    self.pickedPic.value = UIImage(data: data!)
                }
            }
        }
    }
    

    
    @IBAction func changeProfileImageClicked(sender: AnyObject) {
        
        imagePicker!.delegate = self
        print("Popped up..", terminator: "")
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let cameraAction = UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.Default){
            UIAlertAction in
            self.openCamera()
            
        }
        let gallaryAction = UIAlertAction(title: "Choose Existing Photo", style: UIAlertActionStyle.Default){
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel){
            UIAlertAction in
        }
        // Add the actions
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the actionsheet
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            popoverMenu=UIPopoverController(contentViewController: alert)
            popoverMenu!.presentPopoverFromRect(profileImageButton!.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        }
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            imagePicker!.sourceType = UIImagePickerControllerSourceType.Camera
            self .presentViewController(imagePicker!, animated: true, completion: nil)
        }
        else
        {
            openGallary()
        }
    }
    func openGallary()
    {
        imagePicker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(imagePicker!, animated: true, completion: nil)
        }
        else
        {
            popoverMenu=UIPopoverController(contentViewController: imagePicker!)
            popoverMenu!.presentPopoverFromRect(profileImageButton!.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        }
    }
    func imagePickerController(imagePicker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        self.pickedPic.value = ImageCropper.squareCropImageToSideLength((info[UIImagePickerControllerOriginalImage] as? UIImage)!, toSize: 1000)
//        print("Circling "+pickedPic!.description, terminator: "");
        self.picChanged = true
    }
    func imagePickerControllerDidCancel(imagePicker: UIImagePickerController)
    {
        imagePicker .dismissViewControllerAnimated(true, completion: nil)
        print("picker cancel.", terminator: "")
    }
    
    @IBAction func backButtonWasHit(sender: AnyObject) {
        if((currentUser?.isNew) != nil){
            currentUser!.deleteInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                if(success){
                    PFUser.logOutInBackground()
                }
            })
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func termsBtnWasHit(sender: AnyObject) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("webPageVC") as! WebPageViewController
        
        vc.websiteURL   = "http://themouveapp.com/termsofservice.html"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func privacyPolicyBtnWasHit(sender: AnyObject) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("webPageVC") as! WebPageViewController
        
        vc.websiteURL   = "http://themouveapp.com/privacypolicy.html"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        view.endEditing(true)
    }
    
    func goToWalkthrough(){
        // Get view controllers and build the walkthrough
        let stb = UIStoryboard(name: "Login", bundle: nil)
        let walkthrough = stb.instantiateViewControllerWithIdentifier("walkthroughPageVC") as! BWWalkthroughViewController
        let page_one = stb.instantiateViewControllerWithIdentifier("walk1") as UIViewController
        let page_two = stb.instantiateViewControllerWithIdentifier("walk2") as UIViewController
        let page_three = stb.instantiateViewControllerWithIdentifier("walk3") as UIViewController
        let page_four = stb.instantiateViewControllerWithIdentifier("walk4") as UIViewController

        
        // Attach the pages to the master
        walkthrough.delegate = self
        walkthrough.addViewController(page_one)
        walkthrough.addViewController(page_two)
        walkthrough.addViewController(page_three)
        walkthrough.addViewController(page_four)

        
        self.presentViewController(walkthrough, animated: true, completion: nil)
    }
    
    func userSignUp() {
        currentUser!.fullName = nameTextField.text!
        currentUser!.username = usernameTextField.text
        currentUser!.password = passwordTextField.text
        currentUser!.email = emailTextField.text!.lowercaseString
        currentUser!.disabled = false
        if(self.pickedPic.value == appDel.placeHolderBg){
            self.pickedPic.value = appDel.dummyProfPic
        }
        let imgData = generateImageData(self.pickedPic.value!, fullSize: 1000, thumbSize: 100)
        currentUser!.profileImage = PFFile(name: "pfImg.jpg",data: imgData.full)
        currentUser!.thumbnailImg = PFFile(name: "thumbnail.jpg",data: imgData.thumbnail)
//        In case we set the userFbId we want to register through Fb
        finalizeRegistration(self.currentUser!)

    }
    
    
    func finalizeRegistration(newUser: PFEUser){
        if(PFFacebookUtils.isLinkedWithUser(newUser)){
            newUser.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                if(success){

                    SVProgressHUD.dismiss()
                    self.goToWalkthrough()
                }
                if ((error) != nil){
                    SVProgressHUD.dismiss()
                }
            })
        }

        else{
            let usernameQuery = PFEUser.query()!
            usernameQuery.whereKey("username", equalTo: usernameTextField.text!)
            usernameQuery.whereKey("username", matchesRegex: usernameTextField.text!, modifiers: "i")
            usernameQuery.findObjectsInBackgroundWithBlock{ (data: [PFObject]?, error: NSError?) -> Void in
                let users = data as? [PFEUser] ?? []
                if(users.count == 0){
                    newUser.signUpInBackgroundWithBlock { (succeeded: Bool, error: NSError?) -> Void in
                        if let error = error {
                            let errorString = error.userInfo["error"] as? NSString
                            self.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
                            SVProgressHUD.dismiss()
                        }else if(succeeded){
                            
                            SVProgressHUD.dismiss()
                            self.goToWalkthrough()
                        }
                    }
                }else{
                    self.presentViewController(UIAlertController(title: "Uh oh!", message: "Username not available."), animated: true, completion: nil)
                    SVProgressHUD.dismiss()

                }
            }
        }
    }
}
extension SignupViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

}
extension SignupViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(textField: UITextField) {
        if(editMode){

            if ((textField == usernameTextField || textField == emailTextField || textField == newPasswordTextField) && !passTyped){
                passwordTextField.text = ""
                passwordTextField.placeholder = "Required"
                passChanged = true
            }
            else if(textField == passwordTextField){
                passTyped = true
            }
        }
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if(editMode){

            if (usernameTextField.text == PFEUser.currentUser()!.username && emailTextField.text == PFEUser.currentUser()!.email && newPasswordTextField.text == ""){
                passChanged = false
            }
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var result = true
        let allowedCharacterset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._"
        
        if (textField == usernameTextField){
            if (usernameTextField.text?.characters.count >= 0 && usernameTextField.text?.characters.count < 30 )
            {
                let disallowedCharacterSet = NSCharacterSet(charactersInString: allowedCharacterset).invertedSet
                let replacementStringIsLegal = string.rangeOfCharacterFromSet(disallowedCharacterSet) == nil
                result = replacementStringIsLegal
                
                return result
            }
            else if usernameTextField.text?.characters.count > 29
            {

                if (range.length + range.location > usernameTextField.text?.characters.count )
                {
                    return false;
                }
                
                let newLength = (usernameTextField.text?.characters.count)! + string.characters.count - range.length
                return newLength <= 30
            }
        }
        return true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(editMode){
            switch textField {
                case nameTextField:
                    nameTextField.resignFirstResponder()
                    usernameTextField.becomeFirstResponder()
                    
                case usernameTextField:
                    usernameTextField.resignFirstResponder()
                    emailTextField.becomeFirstResponder()
                    
                case emailTextField:
                    emailTextField.resignFirstResponder()
                    passwordTextField.becomeFirstResponder()
                    
                case passwordTextField:
                    passwordTextField.resignFirstResponder()
                    createAccountButtonWasHit(self)

                default: ()
            }
            return true
        }else{
            switch textField {
            case nameTextField:
                nameTextField.resignFirstResponder()
                usernameTextField.becomeFirstResponder()
                
            case usernameTextField:
                usernameTextField.resignFirstResponder()
                emailTextField.becomeFirstResponder()
                
            case emailTextField:
                emailTextField.resignFirstResponder()
                passwordTextField.becomeFirstResponder()
                
            case passwordTextField:
                passwordTextField.resignFirstResponder()
                createAccountButtonWasHit(self)
                
            default: ()
            }
            return true
        }
    }
}