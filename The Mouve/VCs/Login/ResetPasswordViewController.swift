//
//  LoginViewController.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 7/12/15.
//  Copyright (c) 2015 Samuel Ojogbo. All rights reserved.
//

import UIKit
import Parse
import SVProgressHUD
import ParseFacebookUtilsV4
import FBSDKLoginKit


class ResetPasswordViewController: UIViewController

{
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var aboutToSendLabel: UILabel!
    @IBOutlet weak var emailSentLabel: UILabel!
    
    @IBOutlet weak var sendEmailButton: OutlinedButton!
    var user : PFEUser!
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        statusBar(.Default)
        
    }
    
    func setUserData(){
        profilePic.bindAndRound(user.thumbProfImg)
        usernameLabel.text = user!.username
        aboutToSendLabel.text = "Hi \(user!.username) we are going to send you a link for you to reset your password to this email address: \(user!.email)"
    }
    
    @IBAction func backButtonWasHit(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func sendEmailButtonWasHit(sender: AnyObject) {
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        view.endEditing(true)
    }
}
