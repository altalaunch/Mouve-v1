//
//  TutorialViewController.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 4/1/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import Parse
import Crashlytics

class WalkthroughViewController: UIViewController {
    @IBOutlet weak var pageLabel: UILabel!
    @IBOutlet weak var pageLabelWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var gotItButton: UIButton!
        
    var pageImage: UIImage = UIImage()
    var pageIndex: Int = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBarHidden = true
        
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBarHidden = false
    }
    
    @IBAction func gotItButtonWasHit(sender: AnyObject) {
        appDel.checkLogin()
    }
    
    @IBAction func unwindToTutorialVC(segue: UIStoryboardSegue) {}
}
