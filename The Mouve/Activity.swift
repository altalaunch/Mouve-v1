//
//  Activity.swift
//  The Mouve
//
//  Created by Hilal Habashi on 8/21/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation
import Parse
import Bond
import SKPhotoBrowser
import SVProgressHUD

enum typeKeyEnum:String {
    case AddMedia = "addmedia"
    case Follow = "follow"
    case Comment = "comment"
    case Attend = "attend"
    case Invite = "invite"
}

class Activity: PFObject {
    var profilePic: Observable<UIImage?> = Observable(nil)
    var eventBg: Observable<UIImage?> = Observable(nil)
    var thumbnailImg: Observable<UIImage?> = Observable(nil)
    @NSManaged var typeKey: String
    @NSManaged var fromUser: PFEUser
    @NSManaged var toUser: PFEUser?
    @NSManaged var stringContent: String
    @NSManaged var mediaFile: PFFile?
    @NSManaged var thumbnailFile: PFFile?
    @NSManaged var onMouve: Event?
    @NSManaged var reported: Int
    
    override init(){
        super.init()
    }
    init(type: typeKeyEnum, targetUser: PFEUser, stringContent: String, mediaFile: UIImage, targetMouve: Event) {
            super.init(className: "Activity")
            self.fromUser = PFEUser.currentUser()!
            self.toUser = targetUser
            self.stringContent = stringContent
            self.onMouve = targetMouve
            self.reported = 0
        
            self.mediaFile = PFFile(name: "\(self.fromUser.username)_\(self.onMouve!.objectId)_\(self.objectId).jpg", data:UIImageJPEGRepresentation(mediaFile, 0.7)!)
            
            
    }
    override class func query() -> PFQuery? {
        //1
        let query = PFQuery(className: Activity.parseClassName())
        //2
        query.includeKey("fromUser")
        query.includeKey("onMouve")
        //3
        query.orderByAscending("createdAt")
        return query
    }
}
extension Activity{
        
    func dlPfImg(){
        ParseUtility.getImgFile(self.fromUser.thumbnailImg){(img: UIImage?,error: NSError?) in
            if(((error) != nil) || (img == nil)){
                self.profilePic.value = appDel.placeHolderBg!
            }
            else{
//                print("Download Prof Pic for \(self.fromUser.username)", terminator: "")
                self.profilePic.value = img!
            }
        }
    }
    func dlBgImg(){
        ParseUtility.getImgFile(self.onMouve!.thumbnailImg){(img: UIImage?,error: NSError?) in
            if(((error) != nil) || (img == nil)){
                self.eventBg.value = appDel.placeHolderBg!
//                print("No BG value for \(self.onMouve!.name)", terminator: "")
            }
            else{
                self.eventBg.value = img!
//                print("Download BG for \(self.onMouve!.name)", terminator: "")
            }
        }
    }
    func dlMediaThumb(){
        ParseUtility.getImgFile(self.thumbnailFile){(img: UIImage?,error: NSError?) in
            if(((error) != nil) || (img == nil)){
                self.thumbnailImg.value = appDel.placeHolderBg!
//                print("No Thumbnail value for \(self.onMouve!.name)", terminator: "")
            }
            else{
//                print("Download Thumbnail for \(self.onMouve!.name)", terminator: "")
                self.thumbnailImg.value = img!
            }
        }
    }
    
}

extension Activity: PFSubclassing{
//    setting the type key as enum
    var type:typeKeyEnum? {
        
        get { return self["typeKey"] != nil ? typeKeyEnum(rawValue: self["typeKey"] as! String) : nil }
        
        set { return self["typeKey"] = newValue?.rawValue }
        
    }
    //1
    class func parseClassName() -> String {
        return "Activity"
    }
    
    //2
    override class func initialize() {
        var onceToken: dispatch_once_t = 0
        dispatch_once(&onceToken) {
            self.registerSubclass()
        }
    }
}