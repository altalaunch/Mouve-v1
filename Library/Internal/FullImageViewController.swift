//
//  FullImageViewController.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 9/8/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import Parse
import SVProgressHUD


class FullImageViewController: UIViewController {

    @IBOutlet weak var fullImageView: UIImageView!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var stringContent: UILabel!
    
    var media : Activity?
    var eventBg: PFFile?
    var popoverMenu: UIPopoverController?=nil

    
   override func viewDidLoad() {
        super.viewDidLoad()
        if((media) != nil){
            downloadImage(media?.mediaFile)
            usernameLabel.text = "@\(media!.fromUser.username!)"
            usernameLabel.textColor = UIColor.nicePaleBlue()
            stringContent.text = media!.stringContent

        }else if((eventBg) != nil){
            downloadImage(eventBg)
        }
    }
    
    func downloadImage(mediaFile: PFFile?){
        SVProgressHUD.show()

        ParseUtility.getImgFile(mediaFile){(img: UIImage?,error: NSError?) in
            if(((error) != nil) || (img == nil)){
                self.fullImageView.image = appDel.placeHolderBg!
            }
            else{
                self.fullImageView.image = img
            }
            SVProgressHUD.dismiss()

        }
    }
    
    func reportPhoto(){
        SVProgressHUD.show()
        
        let existingReportQuery = Report.query()!
        existingReportQuery.whereKey("fromUser", equalTo: PFEUser.currentUser()!)
        existingReportQuery.whereKey("onActivity", equalTo: media!)
        existingReportQuery.findObjectsInBackgroundWithBlock{ (data: [PFObject]?, error: NSError?) -> Void in
            let reports = data as? [Report] ?? []
            if(reports.count == 0){
                
                let report = Report()
                report.fromUser = PFEUser.currentUser()!
                report.onActivity = self.media!
                report.reportKey = "activityReported"
                
                let reportACL = PFACL(user: PFEUser.currentUser()!)
                reportACL.publicReadAccess = true
                report.ACL = reportACL;
                
                report.saveInBackgroundWithBlock{ (succeeded: Bool, error: NSError?) -> Void in
                    if let error = error {
                        SVProgressHUD.dismiss()
                        
                        let errorString = error.userInfo["error"] as? NSString
                        self.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
                        
                    }else if (succeeded){
                        // end spinner
                        SVProgressHUD.dismiss()
                        self.presentViewController(UIAlertController(title: "Thank You!", message: "A member of our editorial team will take a look at this photo, and it will be removed if it violates our community giudelines."), animated: true, completion: nil)
                        
                    }
                }
                
            }else{
                self.presentViewController(UIAlertController(title: "Thank You!", message: "Thanks for reporting. You have already reported this photo and a member of our editorial team is on it."), animated: true, completion: nil)
                SVProgressHUD.dismiss()
                
            }
        }
    }
    
    func deleteMedia(){
        SVProgressHUD.show()

        let query = Activity.query()
        query!.whereKey("objectId", equalTo:self.media!.objectId!)
        query!.findObjectsInBackgroundWithBlock {
            (objects:[PFObject]?, error:NSError?) -> Void in
            SVProgressHUD.dismiss()

            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) objects.", terminator: "")
                // Do something with the found objects
                if let objects = objects {
                    for object  in objects {
                        let parseObj = object 
                        parseObj.deleteInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
                            if (success) {
                                self.dismissViewControllerAnimated(true, completion: nil)
                                self.navigationController?.popViewControllerAnimated(true)

                            } else {
                                
                            }
                        }
                    }
                }
            }else {
                print("\(error)", terminator: "")
            }
        }
    }
    
    func deleteAlert(){
        let deleteAlert = UIAlertController(title: "Delete Photo?", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        deleteAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
            self.deleteMedia()
            print("Handle Ok logic here")
        }))
        
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
            deleteAlert.dismissViewControllerAnimated(true, completion: nil)
            
            print("Handle Cancel Logic here")
        }))
        presentViewController(deleteAlert, animated: true, completion: nil)
    }
    
    @IBAction func moreButtonWasHit(sender: AnyObject) {
        
        print("Popped up..", terminator: "")
        let alert:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let reportAction = UIAlertAction(title: "Report Image", style: UIAlertActionStyle.Destructive){
            UIAlertAction in
            self.reportPhoto()
            
        }
        
        let deleteAction = UIAlertAction(title: "Delete Image", style: UIAlertActionStyle.Destructive){
            UIAlertAction in
            self.deleteAlert()
            
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel){
            UIAlertAction in
        }
        // Add the actions
        
        if (self.media!.fromUser != PFEUser.currentUser()!){
            alert.addAction(reportAction)
            alert.addAction(cancelAction)
        }else{
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
        }

        // Present the actionsheet
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            popoverMenu=UIPopoverController(contentViewController: alert)
            popoverMenu!.presentPopoverFromRect(moreButton!.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        }
    }
    
    
    @IBAction func cancelButtonHit(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
        DetailViewController.ShouldRefresh.refresh = false
    }
}
