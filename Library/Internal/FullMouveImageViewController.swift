//
//  FullImageViewController.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 9/8/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import Parse
import SVProgressHUD
import Bond

enum PicType{
    case Profile
    case Mouve
}

class FullMouveImageViewController: UIViewController {

    @IBOutlet weak var fullImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var stringContent: UILabel!
    
    var event: Event!
    var user: PFEUser!
    var pageType: PicType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        setView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        switch pageType{
            case PicType.Profile?:
                self.user.profileImg.value = nil
            case PicType.Mouve?:
                self.event.fullBgImg.value = nil
            default:
                break;
            
        }

    }
    
    func setView(){
        switch pageType{
        case PicType.Profile?:

            usernameLabel.text = "@\(user.username!)"
            usernameLabel.textColor = UIColor.nicePaleBlue()
            stringContent.text = ""
            user.profileImg.bindTo(fullImageView!.bnd_image)
            
        case PicType.Mouve?:
            print("Following Page")
            usernameLabel.text = "@\(event.creator.username!)"
            usernameLabel.textColor = UIColor.nicePaleBlue()
            stringContent.text = event.name
            event.fullBgImg.bindTo(fullImageView!.bnd_image)
            
        default:
            break;
        }
    }
    
    @IBAction func cancelButtonHit(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
