//
//  Event.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 8/18/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import Parse
import SKPhotoBrowser
import Bond
import SVProgressHUD
import MWPhotoBrowser


class Event: PFObject,PFSubclassing {
    @NSManaged var creator: PFEUser
    @NSManaged var name: String
    @NSManaged var about: String
    @NSManaged var address: String
    @NSManaged var location: PFGeoPoint
    @NSManaged var startTime: NSDate
    @NSManaged var endTime: NSDate
    @NSManaged var privacy: Bool
    @NSManaged var disabled: Bool
    @NSManaged var numOfAttendees: Int
    @NSManaged var backgroundImage: PFFile?
    @NSManaged var thumbnailImg: PFFile?
    
    var fullBgImg: Observable<UIImage?> = Observable(nil)
    var thumbBgImg: Observable<UIImage?> = Observable(nil)
    var thumbPfImg: Observable<UIImage?> = Observable(nil)
    var attendees: Observable<[PFEUser]?> = Observable(nil)
    var invitees: Observable<[PFEUser]?> = Observable(nil)
    
    
    var timeTillEvent: NSTimeInterval {
        get {
            return NSDate().timeIntervalSinceDate(startTime)
        }
        set {
            self.startTime = NSDate(timeIntervalSinceNow: newValue)
        }
    }
    class func parseClassName() -> String {
        return "Event"
    }
    
    //2
    override class func initialize() {
        var onceToken: dispatch_once_t = 0
        dispatch_once(&onceToken) {
            self.registerSubclass()
        }
    }

    override init(){
        super.init()

    }
    init(name: String, about: String, startTime: NSDate, endTime: NSDate, address: String,
        privacy: Bool, backgroundImage: UIImage) {
        super.init(className: "Event")
        self.creator = PFEUser.currentUser()!
        self.name = name
        self.about = about
        
        self.startTime = startTime
        self.endTime = endTime
        
        self.address = address
        self.privacy = privacy
        
        self.backgroundImage = PFFile(name: "\(self.creator.username)_\(self.objectId).jpg", data:UIImageJPEGRepresentation(backgroundImage,0.7)!)
        

    }


    override class func query() -> PFQuery? {
        //1
        let query = PFQuery(className: Event.parseClassName())
        //2
        query.includeKey("creator")
        //3
        query.orderByAscending("startDate")
        return query
    }
    
}
extension Event{
    func fetchAttendees() {
        if (attendees.value != nil) {
            return
        }
        ParseUtility.queryActivities(onMouve: self,activityType: typeKeyEnum.Attend){ (var attendees: [PFObject]?, error: NSError?) -> Void in
            attendees = attendees?.filter { attendent in attendent["fromUser"] != nil }
            
            // 4
            self.attendees.value = attendees?.map { attendent in
                let fromUser = attendent["fromUser"] as! PFEUser
                return fromUser
            }
        }
    }
    
    func isAttending(user: PFEUser) -> Bool {
        if let attendees = attendees.value {
            return attendees.contains(user)
        } else {
            return false
        }
    }
    
    func toggleAttendEvent(user: PFEUser) {
        if (isAttending(user)) {
            // if image is liked, unlike it now
            // 1
            SVProgressHUD.showSuccessWithStatus("Unattending")
            ParseUtility.deleteActivityInBackground(onMouve: self, toUser: self.creator, activityType: .Attend, onCompletion: { (succeeded, error) -> () in
                if(succeeded){
                    self.attendees.value = self.attendees.value?.filter { $0 != user }
                }
            })
        } else {
            // if this image is not liked yet, like it now
            // 2
            SVProgressHUD.showSuccessWithStatus("Attending")
            ParseUtility.createActivityInBackground(onMouve: self, toUser: self.creator, activityType: .Attend, onCompletion: { (succeeded, error) -> () in
                if(succeeded){
                    self.attendees.value?.append(user)
                }
            })
        }
    }

}
extension Event{
    func fetchInvitees() {
        if (invitees.value != nil) {
            return
        }
        ParseUtility.queryActivities(onMouve: self,activityType: typeKeyEnum.Invite){ ( var invites: [PFObject]?, error: NSError?) -> Void in
            invites = invites?.filter { invite in
                let invite = invite as? Activity

//                Checking that invitee exists
                if(invite?.toUser  != nil){
                    return true
//                    Checking that invitee indeed following us
//                    return invite!.toUser!.isFollowingMe()
                }
                else{
                    return false
                }
                
            


            }
            
            // 4
//            Returning the users we invited
            self.invitees.value = invites?.map { invite in
                let invite = invite as? Activity
                let toUser = invite!.toUser! as PFEUser
                return toUser
            }
        }
    }
    func isInvited(user: PFEUser) -> Bool {
        if let invitees = invitees.value {
            return invitees.contains(user)
        } else {
            return false
        }
    }
    func toggleInvite(user: PFEUser) {
        if (isInvited(user)) {
            // if image is liked, unlike it now
            // 1
            ParseUtility.deleteActivityInBackground(onMouve: self, toUser: user, activityType: .Invite, onCompletion: { (succeeded, error) -> () in
                if(succeeded){
                    self.invitees.value = self.invitees.value?.filter { $0 != user }
                }
            })
        } else {
            // if this image is not liked yet, like it now
            // 2
            ParseUtility.createActivityInBackground(onMouve: self, toUser: user,  activityType: .Invite, onCompletion: { (succeeded, error) -> () in
                if(succeeded){
                    self.invitees.value?.append(user)
                }
            })
        }
    }
}
extension Event{
    
//    func getPhotoForBrowser(index: Int = 0){
    func getPhotoForBrowser(onCompletion: (media: [MWPhoto]?,error: NSError?) -> Void ){
        
        let userQuery : PFQuery = PFEUser.query()!
        userQuery.whereKey("disabled", notEqualTo: true)
        
        let activityQuery: PFQuery? = Activity.query()
        
        
        activityQuery!.whereKey("onMouve", equalTo: self)
        activityQuery!.whereKey("typeKey", containedIn: [typeKeyEnum.Comment.rawValue,typeKeyEnum.AddMedia.rawValue])
        activityQuery!.whereKeyExists("mediaFile")
        activityQuery!.orderByDescending("createdAt")
        activityQuery!.whereKey("fromUser", matchesQuery: userQuery)
        activityQuery?.findObjectsInBackgroundWithBlock({ (objects: [PFObject]?, error: NSError?) -> Void in
            SVProgressHUD.show()
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) objects.", terminator: "")
                if let activities = objects as? [Activity]{
                    //        Step 1 Extra URLs into array
//                    let urlArray = activities.map{$0.mediaFile!.url}
//                    let skArray = urlArray.map{SKPhoto.photoWithImageURL($0!)}
//                    let urlArray = activities.map{NSURL(string: $0.mediaFile!.url!)}
//                    let mediaArray = urlArray.map{(url) -> MWPhoto in
                    let mediaArray = activities.map{(activity) -> MWPhoto in
                        let url = NSURL(string: activity.mediaFile!.url!)
                        let thumbURL = NSURL(string: activity.thumbnailFile!.url!)
//                        print(url!.path!)
                        if(url!.pathExtension == "mov"){
                            let video = MWPhoto(URL:thumbURL)
                            video.videoURL = url
                            video.caption = activity.stringContent
                            return video
                        }else{
                            let photo = MWPhoto(URL: url)
                            photo.caption = activity.stringContent
                            return photo
                        }
                    }
                    SVProgressHUD.dismiss()
                    onCompletion(media: mediaArray, error: nil)
                }
                
            }else {
                SVProgressHUD.dismiss()
                print("\(error)", terminator: "")
                onCompletion(media: nil, error: error)
            }
        })
    }
    
    func dlBgThumb(){
        ParseUtility.getImgFile(self.thumbnailImg){(img: UIImage?,error: NSError?) in
            if((img) != nil){
                self.thumbBgImg.value = img!
//                print("Download BG for \(self.name)", terminator: "")
            }
            else if(((error) != nil) || (img == nil)){
                self.thumbBgImg.value = appDel.dummyEventPic!
//                print("No BG value for \(self.name)", terminator: "")
            }
            else{
                print("well sucks", terminator: "")
            }
        }

    }
    func dlFullImg(){
        ParseUtility.getImgFile(self.backgroundImage){(img: UIImage?,error: NSError?) in
            if((img) != nil){
                self.fullBgImg.value = img!
//                print("Download BG for \(self.name)", terminator: "")
            }
            else if(((error) != nil) || (img == nil)){
                self.fullBgImg.value = appDel.dummyEventPic!
//                print("No BG value for \(self.name)", terminator: "")
            }
            else{
                print("well sucks", terminator: "")
            }
        }

    }
    func dlCellImages(){
        ParseUtility.getImgFile(self.thumbnailImg){(img: UIImage?,error: NSError?) in
            if((img) != nil){
                self.thumbBgImg.value = img!
//                print("Download BG for \(self.name)", terminator: "")
            }
            else if(((error) != nil) || (img == nil)){
                self.thumbBgImg.value = appDel.dummyEventPic!
//                print("No BG value for \(self.name)", terminator: "")
            }
            else{
                print("well sucks", terminator: "")
            }
            self.creator.fetchIfNeededInBackgroundWithBlock { (result: PFObject?, error: NSError?) -> Void in
                if let user = result as? PFEUser{
                    ParseUtility.getImgFile(user.thumbnailImg){(img: UIImage?,error: NSError?) in
                        if(((error) != nil) || (img == nil)){
                            self.thumbPfImg.value = appDel.dummyEventPic!
                        }
                        else{
//                            print("Download Prof Pic for \(user.username)", terminator: "")
                            self.thumbPfImg.value = img!
                        }
                    }
                }
            }
        }
    }
}
