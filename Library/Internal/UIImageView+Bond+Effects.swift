//
//  UIImageView+Bond+Effects.swift
//  The Mouve
//
//  Created by Hilal Habashi on 9/29/15.
//  Copyright © 2015 The Mouve. All rights reserved.
//

import Foundation
import UIKit
import Bond

extension UIImageView{
    func bindAndRound(observed: Observable<UIImage?>) -> DisposableType?{
        let disposable = observed.bindTo(self.bnd_image)
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        self.contentMode = .ScaleAspectFill
        self.layer.cornerRadius = self.layer.bounds.width/2
        return disposable
    }
    
    func bindAndBlur(observed: Observable<UIImage?>,label: UILabel? = nil, alpha: CGFloat = 0.7) -> DisposableType? {
        let disposable = observed.bindTo(self.bnd_image)
        self.clipsToBounds = true
        self.contentMode = .ScaleAspectFill
        if(self.subviews.count<=0){

            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
            let blurView = UIVisualEffectView(effect: blurEffect)
            blurView.alpha = alpha
            blurView.frame = self.frame
            if(label != nil){
                self.addSubview(blurView)
                self.addSubview(label!)
                label!.center = self.convertPoint(self.center, fromView: self.superview)
                
                self.bringSubviewToFront(label!)
            }else{
                self.addSubview(blurView)
            }

            
        }


        return disposable
    }

}
extension UIButton{
    func bindAndRound(observed: Observable<UIImage?>) -> DisposableType?{
        let disposable = observed.observe {(value: UIImage?) ->() in
            self.setImage(value, forState: UIControlState.Normal)
        }
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        self.contentMode = .ScaleAspectFill
        self.layer.cornerRadius = self.layer.bounds.width/2
        return disposable
    }
    
    func bindAndBlur(observed: Observable<UIImage?>, alpha: CGFloat = 0.7) -> DisposableType? {
        let disposable = observed.observe {(value: UIImage?) ->() in
            self.setImage(value, forState: UIControlState.Normal)
        }
        self.clipsToBounds = true
        self.contentMode = .ScaleAspectFill
        if(self.subviews.count<=0){
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
            let blurView = UIVisualEffectView(effect: blurEffect)
            blurView.alpha = alpha
            blurView.frame = self.frame
            self.addSubview(blurView)
        }
        return disposable
    }

}