//
//  NSUserDefaultHelper.swift
//  The Mouve
//
//  Created by Hilal Habashi on 4/25/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation
import CoreLocation

enum UserDefaultKeys: String {
    case lastLocation = "lastLocation"
    case radiusLimit = "radiusLimit"
    case profilePictureURL = "profilePictureURL"
}

class UserDefaults {
    static let get = NSUserDefaults.standardUserDefaults().objectForKey
    static let set = NSUserDefaults.standardUserDefaults().setObject

    class var radiusLimit: Int? {
        get {
            if let value = get(UserDefaultKeys.radiusLimit.rawValue) as? Int {
                return value
            }
            else{
                return 5
            }
        }
        set {
           set(newValue, forKey: UserDefaultKeys.radiusLimit.rawValue)
        }
    }
    class var lastLocation: CLLocation? {
        get {
            let key = UserDefaultKeys.lastLocation.rawValue
            if let lat = get("\(key)-latitude") as? Double, lon = get("\(key)-longitude") as? Double {
                return CLLocation(latitude: lat, longitude: lon)
            } else {
                return nil
            }
        }
        set {
            let key = UserDefaultKeys.lastLocation.rawValue
            set(newValue!.coordinate.latitude, forKey: "\(key)-latitude")
            set(newValue!.coordinate.longitude, forKey: "\(key)-longitude")
        }
    }
    
    class var profilePictureURL: String? {
        get {
            return get(UserDefaultKeys.profilePictureURL.rawValue) as? String
        }
        set {
            set(newValue, forKey: UserDefaultKeys.profilePictureURL.rawValue)
        }
    }
    class func resetUD(){
        let appDomain = NSBundle.mainBundle().bundleIdentifier!
        NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)

    }
}