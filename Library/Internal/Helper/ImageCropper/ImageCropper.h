//
//  ImageCroper.h
//  The Mouve
//
//  Created by Farrukh Iqbal on 9/14/15.
//  Copyright (c) 2015 Hilal Habashi. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@interface ImageCropper : NSObject

+(UIImage*)squareCropImageToSideLength:(UIImage*)sourceImage toSize:(CGFloat)sideLength;
@end
