//
//  ParseUtility.swift
//  The Mouve
//
//  Created by Hilal Habashi on 8/22/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation
import Parse
import ParseUI
import ParseFacebookUtilsV4
import SVProgressHUD
import FBSDKCoreKit
import KeychainAccess
import UIKit
import Bond
import Social
import SVProgressHUD


public enum PostType{
    case Text
    case Media
}

class ParseErrorHandler{
    
    private class func handleInvalidSessionTokenError() {
        if(PFUser.currentUser() == nil){
            PFUser.logOutInBackgroundWithBlock({ (error: NSError?) -> Void in
                print("Debug: \(error?.localizedDescription)")
                ParseLoginUtility.loginWithKeychain()
            })
        }
    }
    class func handleBlockedUser() {
        if(PFEUser.currentUser()!.disabled){
            appDel.logOut()
        }
    }
    class func handleParseError(error: NSError) {
        if error.domain != PFParseErrorDomain {
            return
        }
        
        switch (error.code) {
            case PFErrorCode.ErrorInvalidSessionToken.rawValue:
                handleInvalidSessionTokenError()
            default:
                break;
        }
        
        
    }
}


class ParseUtility{
    class func queryBlocked(onCompletion: ((users: [PFEUser]?, error: NSError?) -> ())?){
        let query = PFQuery(className: "_Role")
        query.whereKey("name", equalTo: "blocked_\(PFEUser.currentUser()!.objectId)")
        query.getFirstObjectInBackgroundWithBlock(){(data:PFObject?, error: NSError?) -> Void in
            if(error != nil){
                ParseErrorHandler.handleParseError(error!)
                onCompletion!(users: nil, error: error)
            }else if let role = data as? PFRole {
                role.users.query().findObjectsInBackgroundWithBlock({ (data: [PFObject]?, error: NSError?) -> Void in
                    if(error != nil){
                        ParseErrorHandler.handleParseError(error!)
                        onCompletion!(users: nil, error: error)
                    }
                    else if let users = data as? [PFEUser]{
                        onCompletion!(users: users, error: error)
                    }
                    
                })
            }
        }
    }
    
    
    
    class func addToBlock(user:PFEUser, onCompletion: ((succeeded: Bool, error: NSError?) -> ())?){
//        Step 1. Check if user has its own blocked-users role
        let query = PFQuery(className: "_Role")
        query.whereKey("name", equalTo: "blocked_\(PFEUser.currentUser()!.objectId)")
        query.getFirstObjectInBackgroundWithBlock(){(data:PFObject?, error: NSError?) -> Void in
            if(error != nil){
                ParseErrorHandler.handleParseError(error!)
                onCompletion!(succeeded: false, error: error)
            }
            else if(data == nil){
//        Step 1b. If role doesn't exist then make the role and change ACL for readble/writeable only by user/admins
                let roleACL = PFACL(user: PFEUser.currentUser()!)
                let role = PFRole(name: "blocked_\(PFEUser.currentUser()!.objectId)", acl: roleACL)
//        Step 2. Add the user to the blocked list role
                role.users.addObject(user)
                role.saveInBackgroundWithBlock{(succeeded: Bool, error: NSError?) -> Void in
                    onCompletion!(succeeded: succeeded, error: error)
                }

            }
            else if let role = data as? PFRole {
//        Step 2. Add the user to the blocked list role
                role.users.addObject(user)
                role.saveInBackgroundWithBlock{(succeeded: Bool, error: NSError?) -> Void in
                    onCompletion!(succeeded: succeeded, error: error)
                }
            }

        }
//        Step 3. Add the exclusion of blocked users to all other queries.
    }
    
    class func removeFromBlock(user:PFEUser, onCompletion: ((succeeded: Bool, error: NSError?) -> ())?){
        //        Step 1. Check if user has its own blocked-users role
        let query = PFQuery(className: "_Role")
        query.whereKey("name", equalTo: "blocked_\(PFEUser.currentUser()!.objectId)")
        query.getFirstObjectInBackgroundWithBlock(){(data:PFObject?, error: NSError?) -> Void in
            if(error != nil){
                ParseErrorHandler.handleParseError(error!)
                onCompletion!(succeeded: false, error: error)
            }
            else if(data == nil){
                onCompletion!(succeeded: false, error: nil)
            }
            else if let role = data as? PFRole {
                //        Step 2. Remove the user from the blocked list role
                role.users.removeObject(user)
                role.saveInBackgroundWithBlock{(succeeded: Bool, error: NSError?) -> Void in
                    onCompletion!(succeeded: succeeded, error: error)
                }
            }
            
        }
        //        Step 3. Add the exclusion of blocked users to all other queries.
    }
    

    class func queryBlocking(fromUser: PFEUser? = nil, toUser: PFEUser? = nil,completionBlock: PFQueryArrayResultBlock?){
        let query = Block.query()!
        if((fromUser) != nil){
            query.whereKey("fromUser", equalTo: fromUser!)
        }
        if((toUser) != nil){
            query.whereKey("toUser", equalTo: toUser!)
        }
        query.findObjectsInBackgroundWithBlock(completionBlock)
    }

    class func createBlockInBackground(fromUser: PFEUser? = PFEUser.currentUser()!, toUser: PFEUser, onCompletion: ((succeeded: Bool, error: NSError?) -> ())?){
        
        let block = Block()
        block.fromUser = PFEUser.currentUser()!
        block.toUser = toUser
        
        let blockACL = PFACL(user: PFEUser.currentUser()!)
        blockACL.publicReadAccess = true
        blockACL.publicWriteAccess = false
        block.ACL = blockACL;
        
        block.saveInBackgroundWithBlock{ (succeeded: Bool, error: NSError?) -> Void in
            onCompletion!(succeeded: succeeded, error: error)
        }
    }
    
    class func deleteBlockInBackground(fromUser: PFEUser? = PFEUser.currentUser()!, toUser: PFEUser, onCompletion: ((succeeded: Bool, error: NSError?) -> ())?){
        
        let blockQuery = Block.query()!
        blockQuery.whereKey("fromUser", equalTo: fromUser!)
        blockQuery.whereKey("toUser", equalTo: toUser)

        blockQuery.getFirstObjectInBackgroundWithBlock(){(data: PFObject?, error: NSError?) -> Void in
            if(error != nil){
                ParseErrorHandler.handleParseError(error!)
                onCompletion!(succeeded: false, error: error)
            }
            else if let block = data as? Block{
                block.deleteInBackgroundWithBlock{ (success: Bool, error: NSError?) -> Void in
                    onCompletion!(succeeded: success, error: error)
                }
            }
            else{
                print("data cant be found", terminator: "")
            }
        }
    }
    
    class func createActivityInBackground(fromUser: PFEUser? = PFEUser.currentUser()!, toUser: PFEUser? = nil, onMouve: Event? = nil, activityType: typeKeyEnum,onCompletion: ((succeeded: Bool, error: NSError?) -> ())?){
//        Check if invited if private
        let query = Activity.query()
        query?.whereKey("fromUser", equalTo: fromUser!)
        if((onMouve) != nil){
            query?.whereKey("onMouve", equalTo: onMouve!)
        }
        if((toUser) != nil){
            query?.whereKey("toUser", equalTo: toUser!)
        }
        query?.whereKey("typeKey", equalTo: activityType.rawValue)
//        if (query?.count(NSErrorPointer()) != 0){
        query?.countObjectsInBackgroundWithBlock(){ (num: Int32, error: NSError?) -> Void in
            if(error != nil){
                ParseErrorHandler.handleParseError(error!)
                onCompletion!(succeeded: false, error: error)
            }else{
                switch(activityType){
                    case .Follow, .Attend, .Invite:
                        if (num != 0){
                            onCompletion!(succeeded: false, error: nil)
                        }else{
                            fallthrough
                            
                        }
                    default:
                        let activity = Activity()
                        activity.fromUser = fromUser!
                        if((onMouve) != nil){
                            activity.onMouve = onMouve!
                        }
                        if((toUser) != nil){
                            activity.toUser = toUser!
                        }
                        activity.typeKey = activityType.rawValue
                        
                        let activityACL = PFACL(user: PFEUser.currentUser()!)
                        activityACL.publicReadAccess = true
                        activityACL.publicWriteAccess = false
                        
//                        activityACL.setWriteAccess(true, forUser: PFEUser.currentUser()!)
//                        activityACL.setWriteAccess(true, forRoleWithName: "admins")
//                        activityACL.setReadAccess(false, forRoleWithName: "blocked_\(PFEUser.currentUser()!.objectId)")
                        
                        activity.ACL = activityACL;
                        activity.saveInBackgroundWithBlock(){(succeeded: Bool, error: NSError?) -> Void in
                            onCompletion!(succeeded: succeeded, error: error)
                    }
                }
            }
        }
    }

    
    
    //Use wisely as it can delete all the data from one user
    class func deleteActivityInBackground(fromUser: PFEUser? = PFEUser.currentUser()!, toUser: PFEUser? = nil, onMouve: Event? = nil, activityType: typeKeyEnum ,onCompletion: ((succeeded: Bool, error: NSError?) -> ())?){
        
        let query = Activity.query()
        query?.whereKey("fromUser", equalTo: fromUser!)
        if((onMouve) != nil){
            query?.whereKey("onMouve", equalTo: onMouve!)
        }
        if((toUser) != nil){
            query?.whereKey("toUser", equalTo: toUser!)
        }

        query?.whereKey("typeKey", equalTo: activityType.rawValue)
        query?.findObjectsInBackgroundWithBlock(){(data:[PFObject]?, error: NSError?) -> Void in
            if(error != nil){
                ParseErrorHandler.handleParseError(error!)
                onCompletion!(succeeded: false, error: error)
            }
            else if let activities = data as? [Activity]{
                PFObject.deleteAllInBackground(activities){ (succeeded: Bool, error: NSError?) -> Void in
                    onCompletion!(succeeded: succeeded,error: error)
                }
            }
            else{
                print("data cant be found", terminator: "")
            }
        }
        
    }

    
    class func inviteUsersToMouveInBackground(targetEvent: Event, targetUsers: [PFEUser],onCompletion: ((succeeded: Bool, error: NSError?) -> ())?){
        for user in targetUsers{
//            inviteUserToMouveInBackground(targetEvent, targetUser:user, onCompletion: onCompletion)
            createActivityInBackground(PFEUser.currentUser(), toUser: user, onMouve: targetEvent, activityType: .Invite, onCompletion: onCompletion)
            
        }
    }

    
    
    class func getEventBgImg(targetEvent: Event,onCompletion: ((data: UIImage?, error: NSError?) -> ())?){
        if let eventImage = targetEvent.backgroundImage as PFFile?{
            eventImage.getDataInBackgroundWithBlock(){(imgData:NSData?, error: NSError?) -> Void in
                if((error) != nil){
                    onCompletion!(data: nil, error: error)
                }
                onCompletion!(data: UIImage(data: imgData! as NSData), error: nil)
            }
        }
        else{
            onCompletion!(data: nil, error: nil)
        }
    }
    class func getProfileImg(targetUser: PFEUser,onCompletion: ((data: UIImage?, error: NSError?) -> ())?){
        if(targetUser.profileImage == nil){
            onCompletion!(data: nil, error: nil)
        }
        targetUser.profileImage?.getDataInBackgroundWithBlock(){(imgData:NSData?, error: NSError?) -> Void in
            if((imgData) != nil){
                onCompletion!(data: UIImage(data: imgData!), error: nil)
            }
        }
    }
    class func getImgFile(targetFile: PFFile?,onCompletion: ((data: UIImage?, error: NSError?) -> ())?){
        if(targetFile == nil){
            onCompletion!(data: nil, error: nil)
        }
        targetFile?.getFilePathInBackgroundWithBlock(){(path:String?, error: NSError?) -> Void in
            if((path) != nil){
                onCompletion!(data: UIImage(contentsOfFile: path!), error: nil)
            }
        }
    }

    
    class func findFollowersInBackground(targetUser: PFEUser, range: Range<Int>,onCompletion: ((data:[PFObject]?, error: NSError?) -> ())?){

        let blockQuery : PFQuery = Block.query()!
        blockQuery.whereKey("fromUser", equalTo: targetUser)
        
        let userQuery : PFQuery = PFEUser.query()!
        userQuery.whereKey("disabled", notEqualTo: true)
        
        
        let activityQuery : PFQuery = Activity.query()!
        activityQuery.whereKey("toUser", equalTo: targetUser)
        activityQuery.whereKey("typeKey", equalTo: typeKeyEnum.Follow.rawValue)
        activityQuery.whereKey("fromUser", matchesQuery: userQuery)
        activityQuery.orderByDescending("createdAt")
        activityQuery.whereKey("fromUser", doesNotMatchKey: "toUser", inQuery: blockQuery)
        
        activityQuery.skip = range.startIndex
        activityQuery.limit = range.endIndex - range.startIndex
        activityQuery.findObjectsInBackgroundWithBlock(){(data:[PFObject]?, error: NSError?) -> Void in
            // While normally there should only be one follow activity returned, we can't guarantee that.
            var userResultList = [PFEUser]()
            if let followActivities = data as? [Activity]{
                for  activity in followActivities{
                    userResultList.append(activity.fromUser)
                }
                onCompletion!(data: userResultList, error: nil)
            }
            else{
                onCompletion!(data: nil, error: error)
            }
            
        }
    }
    class func findFolloweesInBackground(targetUser: PFEUser, range: Range<Int>,onCompletion: ((data:[PFObject]?, error: NSError?) -> ())?){
        
        let userQuery : PFQuery = PFEUser.query()!
        userQuery.whereKey("disabled", notEqualTo: true)
        
        let query = Activity.query()
        query?.whereKey("fromUser", equalTo: targetUser)
        query?.whereKey("typeKey", equalTo: typeKeyEnum.Follow.rawValue)
        query?.whereKey("toUser", matchesQuery: userQuery)
        query?.orderByDescending("createdAt")
        
        query?.skip = range.startIndex
        query?.limit = range.endIndex - range.startIndex
        query?.findObjectsInBackgroundWithBlock(){(data:[PFObject]?, error: NSError?) -> Void in
            // While normally there should only be one follow activity returned, we can't guarantee that.
            var userResultList = [PFEUser]()
            if let followActivities = data as? [Activity]{
                for  activity in followActivities{
                    userResultList.append(activity.toUser!)
                }
                onCompletion!(data: userResultList, error: error)
            }
            else{
                onCompletion!(data: nil, error: error)
            }
        }
    }
    
    class func findAttendeesInBackground(targetEvent: Event, range: Range<Int>,onCompletion: ((data:[PFObject]?, error: NSError?) -> ())?){
        
        let userQuery : PFQuery = PFEUser.query()!
        userQuery.whereKey("disabled", notEqualTo: true)
        
        let blockQuery : PFQuery = Block.query()!
        blockQuery.whereKey("toUser", equalTo: PFEUser.currentUser()!)
        
        
        let query = Activity.query()
        query?.whereKey("onMouve", equalTo: targetEvent)
        query?.whereKey("typeKey", equalTo: typeKeyEnum.Attend.rawValue)
        query?.whereKey("toUser", matchesQuery: userQuery)
        query?.whereKey("fromUser", doesNotMatchKey: "fromUser", inQuery: blockQuery)
        query?.skip = range.startIndex
        query?.limit = range.endIndex - range.startIndex
        query?.findObjectsInBackgroundWithBlock(){(data:[PFObject]?, error: NSError?) -> Void in
            // While normally there should only be one follow activity returned, we can't guarantee that.
            var userResultList = [PFEUser]()
            if let attendActivities = data as? [Activity]{
                for  activity in attendActivities{
                    userResultList.append(activity.fromUser)
                }
                onCompletion!(data: userResultList, error: error)
            }
            else{
                onCompletion!(data: nil, error: error)
            }
        }
    }


    class func searchUserQuery(searchString: String?,range: Range<Int>, completionBlock: PFQueryArrayResultBlock?){
        //      fullName Query
              
        let fullNameQuery : PFQuery = PFEUser.query()!
        //      modifier "i" is for case insensitive
        fullNameQuery.whereKey("disabled", notEqualTo: true)
        fullNameQuery.whereKey("fullName", matchesRegex: searchString!, modifiers: "i")
        fullNameQuery.whereKey("fullName", notEqualTo: (PFEUser.currentUser()?["fullName"])!)
        
        //      username Query
        let usernameQuery : PFQuery = PFEUser.query()!
        //      modifier "i" is for case insensitive
        usernameQuery.whereKey("disabled", notEqualTo: true)
        usernameQuery.whereKey("username", matchesRegex: searchString!, modifiers: "i")
        usernameQuery.whereKey("username", notEqualTo: (PFEUser.currentUser()?.username)!)
        
        //      Querying subqueries
        let query = PFQuery.orQueryWithSubqueries([fullNameQuery, usernameQuery])
        
        query.skip = range.startIndex
        query.limit = range.endIndex - range.startIndex
        query.findObjectsInBackgroundWithBlock(completionBlock)
    }
    
    
    class func countProfileMouves(targetUser: PFEUser, completionBlock: PFIntegerResultBlock?){
        let createdByUser = PFQuery(className: "Event")
        createdByUser.whereKey("creator", equalTo: targetUser)
        createdByUser.whereKey("disabled", notEqualTo: true)
        createdByUser.countObjectsInBackgroundWithBlock { (numCreated: Int32, error: NSError?) -> Void in
            if(error != nil){
                completionBlock!(numCreated,error)
            }else{
                print("\nMouve created:\(numCreated)\n")
                
                let userQuery : PFQuery = PFEUser.query()!
                userQuery.whereKey("disabled", notEqualTo: true)
                
                let mouveQuery : PFQuery = Event.query()!
                mouveQuery.whereKey("disabled", notEqualTo: true)
                
                let attendingQuery = PFQuery(className: "Activity")
                attendingQuery.whereKey("typeKey", equalTo: typeKeyEnum.Attend.rawValue)
                attendingQuery.whereKey("fromUser", equalTo: targetUser)
                attendingQuery.whereKey("toUser", notEqualTo: targetUser)
                attendingQuery.whereKeyExists("onMouve")
                attendingQuery.whereKey("toUser", matchesQuery: userQuery)
                attendingQuery.whereKey("onMouve", matchesQuery: mouveQuery)

                
                attendingQuery.countObjectsInBackgroundWithBlock { (numAttended: Int32, error: NSError?) -> Void in
                    if(error != nil){
                        completionBlock!(numCreated,error)
                    }else{
                        print("\nMouve attended:\(numAttended)\n")
                        completionBlock!(numAttended + numCreated,error)
                        print("\nMouve created:\(numCreated)\n")
                    }
                }
            }
        }
    }
    
    
    class func queryProfileMouves(targetUser: PFEUser,range: Range<Int>, completionBlock: PFQueryArrayResultBlock?){
        PFEUser.currentUser()?.fetchBlockingMe()
        
        if(targetUser.isBlockingMe() == false){
            let mouvesAttended = NSMutableArray()
        
            // Step 1: Query made Event and find objects
            let createdByUser = Event.query()
            createdByUser!.whereKey("disabled", notEqualTo: true)
            createdByUser!.whereKey("creator", equalTo: targetUser)
        
            createdByUser!.skip = range.startIndex
            createdByUser!.limit = range.endIndex - range.startIndex
            
            let startTimeSorter = NSSortDescriptor(key: "startTime", ascending:true )
            let createTimeSorter = NSSortDescriptor(key: "createdAt", ascending:false )
            
            createdByUser!.orderBySortDescriptors([startTimeSorter,createTimeSorter])
            createdByUser!.findObjectsInBackgroundWithBlock { (results: [PFObject]?, error: NSError?) -> Void in
                if ((results) != nil) {
                    mouvesAttended.addObjectsFromArray(results!)
                    // Step 2: Query attended Event, find objects , and add objects to previous array
                    
                    let userQuery : PFQuery = PFEUser.query()!
                    userQuery.whereKey("disabled", notEqualTo: true)
                    
                    let mouveQuery : PFQuery = Event.query()!
                    mouveQuery.whereKey("disabled", notEqualTo: true)
                    
                    let attendingQuery = Activity.query()
                    attendingQuery!.whereKey("typeKey", equalTo: typeKeyEnum.Attend.rawValue)
                    attendingQuery!.whereKey("fromUser", equalTo: targetUser)
                    attendingQuery!.whereKeyExists("onMouve")
                    attendingQuery!.whereKey("toUser", matchesQuery: userQuery)
                    attendingQuery!.whereKey("onMouve", matchesQuery: mouveQuery)

                    attendingQuery!.skip = range.startIndex
                    attendingQuery!.limit = range.endIndex - range.startIndex
                    let startTimeSorter = NSSortDescriptor(key: "startTime", ascending:true )
                    let createTimeSorter = NSSortDescriptor(key: "createdAt", ascending:false )
                    attendingQuery!.orderBySortDescriptors([startTimeSorter,createTimeSorter])
                    attendingQuery!.findObjectsInBackgroundWithBlock{ (results: [PFObject]?, error: NSError?) -> Void in
                        if ((results) != nil) {
                            for act in results as! [Activity]{
                                if(!mouvesAttended.containsObject(act.onMouve!)){
                                    mouvesAttended.addObject(act.onMouve!)
                                }
                            }
                            // Step 3: Sort attended Event and fill up data into global variable
                            let startTimeSorter = NSSortDescriptor(key: "startTime", ascending:true )
                            let createTimeSorter = NSSortDescriptor(key: "createdAt", ascending:false )
                        completionBlock!(mouvesAttended.sortedArrayUsingDescriptors([createTimeSorter,startTimeSorter]) as? [Event], nil)
                            
                        }
                    }
                }
            }
        }else{
            let mouvesAttended = NSMutableArray()
            
            // Step 1: Query made Event and find objects
            let createdByUser = Event.query()
            createdByUser!.whereKey("disabled", notEqualTo: true)
            createdByUser!.whereKey("creator", equalTo: targetUser)
            createdByUser!.whereKey("objectId", equalTo: "blocked")            
            createdByUser!.limit = 0
            createdByUser!.findObjectsInBackgroundWithBlock { ( var results: [PFObject]?, error: NSError?) -> Void in
                if ((results) != nil) {
                    results?.removeAll()
                    mouvesAttended.removeAllObjects()
                    completionBlock!(mouvesAttended.sortedArrayUsingDescriptors([]) as? [Event], nil)
                    
                }
            }
        }
    }
    
    
    class func queryPostsFeed(postType: PostType,targetEvent: Event ,range: Range<Int>, completionBlock: PFQueryArrayResultBlock?){
        //            First query the people we follow
        //            Then query all the mouves made by them
        let feedQuery: PFQuery? = Activity.query()
        switch postType {
        case .Text:

            let userQuery : PFQuery = PFEUser.query()!
            userQuery.whereKey("disabled", notEqualTo: true)
            
            feedQuery!.whereKey("onMouve", equalTo: targetEvent)
            feedQuery!.whereKeyDoesNotExist("mediaFile")
            feedQuery!.whereKey("typeKey", equalTo: typeKeyEnum.Comment.rawValue)
            feedQuery!.orderByDescending("createdAt")
            feedQuery!.whereKey("fromUser", matchesQuery: userQuery)
            
            feedQuery!.skip = range.startIndex
            feedQuery!.limit = range.endIndex - 20
            feedQuery!.findObjectsInBackgroundWithBlock(completionBlock)
            
        case .Media:
            
            let userQuery : PFQuery = PFEUser.query()!
            userQuery.whereKey("disabled", notEqualTo: true)
            
            feedQuery!.whereKey("onMouve", equalTo: targetEvent)
            feedQuery!.whereKey("typeKey", containedIn: [typeKeyEnum.Comment.rawValue,typeKeyEnum.AddMedia.rawValue])
            feedQuery!.whereKeyExists("mediaFile")
            feedQuery!.orderByDescending("createdAt")
            feedQuery!.whereKey("fromUser", matchesQuery: userQuery)
            
            feedQuery!.skip = range.startIndex
            feedQuery!.limit = range.endIndex - 100
            feedQuery!.findObjectsInBackgroundWithBlock(completionBlock)
        }
    }
    
    class func queryActivities(fromUser: PFEUser? = nil, toUser: PFEUser? = nil,onMouve: Event? = nil ,activityType: typeKeyEnum ,range: Range<Int>? = nil, completionBlock: PFQueryArrayResultBlock?){
        let query = Activity.query()
        
        if((onMouve) != nil){
            query?.whereKey("onMouve", equalTo: onMouve!)
        }
        if((toUser) != nil){
            query?.whereKey("toUser", equalTo: toUser!)
        }
        if((fromUser) != nil){
            query?.whereKey("fromUser", equalTo: fromUser!)

        }
        query?.whereKey("typeKey", equalTo: activityType.rawValue)
        if((range) != nil){
            query?.skip = (range?.startIndex)!
            query?.limit = (range?.endIndex)! - range!.startIndex
        }
        
        query?.findObjectsInBackgroundWithBlock(completionBlock)
        
    }
    
    class func queryFeed(sceneType: SceneType,range: Range<Int>, completionBlock: PFQueryArrayResultBlock?){
//            First query the people we follow
//            Then query all the mouves made by them
        var feedQuery: PFQuery
        switch sceneType {
        case .Explore:
            
            let userQuery : PFQuery = PFEUser.query()!
            userQuery.whereKey("disabled", notEqualTo: true)
            
            let blockQuery : PFQuery = Block.query()!
            blockQuery.whereKey("toUser", equalTo: PFEUser.currentUser()!)
            
            feedQuery = Event.query()!
            feedQuery.whereKey("location", nearGeoPoint: PFGeoPoint(location: UserDefaults.lastLocation), withinMiles: Double(UserDefaults.radiusLimit!))
            feedQuery.whereKey("createdAt", greaterThanOrEqualTo: NSDate().yesterday().yesterday())
            feedQuery.whereKey("endTime", greaterThanOrEqualTo: NSDate())
            feedQuery.whereKey("creator", notEqualTo: PFEUser.currentUser()!)
            feedQuery.whereKey("creator", matchesQuery: userQuery)
            feedQuery.whereKey("disabled", notEqualTo: true)
            feedQuery.whereKey("creator", doesNotMatchKey: "fromUser", inQuery: blockQuery)

            feedQuery.includeKey("creator")
            
            let startTimeSorter = NSSortDescriptor(key: "startTime", ascending:true )
            let createTimeSorter = NSSortDescriptor(key: "createdAt", ascending:false )
            
            feedQuery.orderBySortDescriptors([createTimeSorter,startTimeSorter])
            feedQuery.skip = range.startIndex
            feedQuery.limit = range.endIndex - range.startIndex
            feedQuery.findObjectsInBackgroundWithBlock(completionBlock)
            
        case .Scene:

            // Prevents the crash on first registrationsince user might be nil on start after signup
            // We create a second query for the current user's mouves
            
            let userQuery : PFQuery = PFEUser.query()!
            userQuery.whereKey("disabled", notEqualTo: true)
            
            let mouveQuery : PFQuery = Event.query()!
            mouveQuery.whereKey("disabled", notEqualTo: true)
            
            let blockQuery : PFQuery = Block.query()!
            blockQuery.whereKey("toUser", equalTo: PFEUser.currentUser()!)
            
            let followingQuery = Activity.query()
            followingQuery?.whereKey("typeKey", equalTo: typeKeyEnum.Follow.rawValue)
            followingQuery?.whereKey("fromUser", equalTo: PFEUser.currentUser()!)
            followingQuery?.whereKey("toUser", matchesQuery: userQuery)

            // Using the activities from the query above, we find all of the photos taken by
            // the friends the current user is following
            let followingMouvesQuery = PFQuery(className: Event.parseClassName())
            followingMouvesQuery.whereKey("creator", matchesKey: "toUser", inQuery: followingQuery!)
            followingMouvesQuery.whereKey("creator", doesNotMatchKey: "fromUser", inQuery: blockQuery)
            followingMouvesQuery.whereKeyExists("name")

            let mouvesFromCurrentUserQuery = PFQuery(className: Event.parseClassName())
            mouvesFromCurrentUserQuery.whereKey("creator", equalTo: PFEUser.currentUser()!)
            mouvesFromCurrentUserQuery.whereKeyExists("name")
            mouvesFromCurrentUserQuery.whereKey("disabled", notEqualTo: true)
            
            // We create a final compound query that will find all of the photos that were
            // taken by the user's friends or by the user
            feedQuery = PFQuery.orQueryWithSubqueries([mouvesFromCurrentUserQuery, followingMouvesQuery])
            feedQuery.whereKey("createdAt", greaterThanOrEqualTo: NSDate().yesterday().yesterday())
            feedQuery.whereKey("endTime", greaterThanOrEqualTo: NSDate())
            let startTimeSorter = NSSortDescriptor(key: "startTime", ascending:true )
            let createTimeSorter = NSSortDescriptor(key: "createdAt", ascending:false )
            feedQuery.orderBySortDescriptors([createTimeSorter,startTimeSorter])
            feedQuery.skip = range.startIndex
            feedQuery.limit = range.endIndex - range.startIndex
            
            feedQuery.findObjectsInBackgroundWithBlock(completionBlock)
            
        case .Newsfeed:
            let userQuery : PFQuery = PFEUser.query()!
            userQuery.whereKey("disabled", notEqualTo: true)
            
            let blockQuery : PFQuery = Block.query()!
            blockQuery.whereKey("fromUser", equalTo: PFEUser.currentUser()!)

            feedQuery = Activity.query()!
            feedQuery.whereKey("toUser", equalTo: PFEUser.currentUser()!)
            feedQuery.whereKey("fromUser", notEqualTo: PFEUser.currentUser()!)
            feedQuery.whereKey("fromUser", matchesQuery: userQuery)
            feedQuery.whereKey("fromUser", doesNotMatchKey: "toUser", inQuery: blockQuery)
            feedQuery.whereKey("typeKey", containedIn: [typeKeyEnum.AddMedia.rawValue,typeKeyEnum.Comment.rawValue,typeKeyEnum.Attend.rawValue,typeKeyEnum.Follow.rawValue])
            feedQuery.orderByDescending("createdAt")
            feedQuery.skip = range.startIndex
            feedQuery.limit = range.endIndex - 80
            
            feedQuery.findObjectsInBackgroundWithBlock(completionBlock)
            
        default:
            let userQuery : PFQuery = PFEUser.query()!
            userQuery.whereKey("disabled", notEqualTo: true)
            
            let blockQuery : PFQuery = Block.query()!
            blockQuery.whereKey("fromUser", equalTo: PFEUser.currentUser()!)
            
            feedQuery = Activity.query()!
            feedQuery.whereKey("toUser", equalTo: PFEUser.currentUser()!)
            feedQuery.whereKey("typeKey", equalTo: typeKeyEnum.Invite.rawValue)
            feedQuery.orderByDescending("createdAt")
            feedQuery.whereKey("fromUser", matchesQuery: userQuery)
            feedQuery.whereKey("fromUser", doesNotMatchKey: "toUser", inQuery: blockQuery)
            feedQuery.skip = range.startIndex
            feedQuery.limit = range.endIndex - 80
            
            feedQuery.findObjectsInBackgroundWithBlock(completionBlock)

            
//        default:
//            print("Nada", terminator: "")
        }
    }
}


class ParseLoginUtility{
    class func linkToFacebookDialog(user: PFUser,fbAccToken: FBSDKAccessToken?){
        let vc = appDel.window?.rootViewController
        
        let linkFbAlert = UIAlertController(title: "Link Facebook?", message: "Would you like to link your Mouve account with your Facebook account?", preferredStyle: UIAlertControllerStyle.Alert)
        
        vc!.presentViewController(linkFbAlert, animated: true, completion: nil)
        // Ok For linking
        linkFbAlert.addAction(UIAlertAction(title: "Yes!", style: .Cancel, handler: { (action: UIAlertAction!) in
            SVProgressHUD.show()
            PFFacebookUtils.linkUserInBackground(user, withAccessToken: fbAccToken!, block: { (success:Bool, error:NSError?) -> Void in
                if((error) != nil){
                    vc!.presentViewController(UIAlertController(title: "Oops...", message: error!.localizedDescription), animated: true, completion: nil)
                    SVProgressHUD.dismiss()
                }else if(success){
                    SVProgressHUD.dismiss()
                    appDel.checkLogin()
                }
            })
        }))
        // Cancel Linking
        linkFbAlert.addAction(UIAlertAction(title: "No!", style: .Default, handler: { (action: UIAlertAction!) in
            SVProgressHUD.show()
            appDel.checkLogin()
        }))
    }
    class func signUpOrLinkDialog(user: PFUser?){
        let vc = appDel.window?.rootViewController
        let loginNotExist = UIAlertController(title: "Oops", message: "There's no account linked with this Facebook user. Would you like to make new account or link an existing one?", preferredStyle: UIAlertControllerStyle.Alert)
        vc!.presentViewController(loginNotExist, animated: true, completion: nil)
        // Ok For linking
        loginNotExist.addAction(UIAlertAction(title: "Sign Up!", style: .Default, handler: { (action: UIAlertAction!) in
            let signUpVC = vc!.initVC("signUpVC", storyboard: "Login") as! SignupViewController
            signUpVC.fillFromFb()
            vc!.showViewController(signUpVC, sender: vc)
        }))
        // Cancel Linking
        loginNotExist.addAction(UIAlertAction(title: "Login!", style: .Cancel, handler: { (action: UIAlertAction!) in
            let fbAccToken = FBSDKAccessToken.currentAccessToken().copy() as? FBSDKAccessToken
            user?.deleteInBackgroundWithBlock()
                { (success:Bool, error:NSError?) -> Void in
                    if(success){
                        PFUser.logOutInBackgroundWithBlock(){ (error:NSError?) -> Void in
                            FBSDKAccessToken.setCurrentAccessToken(fbAccToken!)
                        }
                    }
            }
        }))
    }

    class func loginWithUsername(username: String, password: String, fbAccToken: FBSDKAccessToken?){
        let vc = appDel.window?.rootViewController
        PFUser.logInWithUsernameInBackground(username, password: password, block: { (user, error) in
            if user != nil {
                
                self.saveToKeychain(username, password: password)
                if((fbAccToken != nil) && !PFFacebookUtils.isLinkedWithUser(user!)){
                    SVProgressHUD.dismiss()
                    self.linkToFacebookDialog(user!, fbAccToken: fbAccToken)
                }
                else{
                    appDel.checkLogin()
                }
            } else {
                if let error = error {
                    SVProgressHUD.dismiss()
                    let errorString = error.userInfo["error"] as? NSString
                    vc!.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
                }
            }
        })
    }
    
    class func loginWithKeychain(){
        let keychain = Keychain(service: "com.themouveapp.themouve")
        if(!keychain.allKeys().isEmpty){
            let email = keychain.allKeys()[0]
            self.loginWithUsername(email, password:keychain[email]!, fbAccToken: nil)
        }
    }
    
    
    class func saveToKeychain(email: String, password: String){
        let keychain = Keychain(service: "com.themouveapp.themouve")
        if (keychain.allKeys().isEmpty){
            do {
                try keychain
                    .accessibility(.AfterFirstUnlock)
                    .set(password, key: email)
            } catch let error {
                print("error: \(error)")
            }
        }else{
            do {
             	try keychain.removeAll()
                self.saveToKeychain(email, password: password)
            } catch let error {
                print("error: \(error)")
            }
        }
    }

    class func deleteFromKeychain(){
        let keychain = Keychain(service: "com.themouveapp.themouve")
        do {
            try keychain.removeAll()
        } catch let error {
            print("error: \(error)")
        }
    }
}

extension PFObject {
    
    public override func isEqual(object: AnyObject?) -> Bool {
        if (object as? PFObject)?.objectId == self.objectId {
            return true
        } else {
            return super.isEqual(object)
        }
    }
}


extension NSDate {
    func yearsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Year, fromDate: date, toDate: self, options: []).year
    }
    func monthsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Month, fromDate: date, toDate: self, options: []).month
    }
    func weeksFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.WeekOfYear, fromDate: date, toDate: self, options: []).weekOfYear
    }
    func daysFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Day, fromDate: date, toDate: self, options: []).day
    }
    func hoursFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Hour, fromDate: date, toDate: self, options: []).hour
    }
    func minutesFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Minute, fromDate: date, toDate: self, options: []).minute
    }
    func secondsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Second, fromDate: date, toDate: self, options: []).second
    }
    func offsetFrom(date:NSDate) -> String {
        if yearsFrom(date)   > 0 { return "\(yearsFrom(date))y"   }
//        if monthsFrom(date)  > 0 { return "\(monthsFrom(date))M"  }
        if monthsFrom(date)  > 0 { return "\(weeksFrom(date))w"   }
        if weeksFrom(date)   > 0 { return "\(weeksFrom(date))w"   }
        if daysFrom(date)    > 0 { return "\(daysFrom(date))d"    }
        if hoursFrom(date)   > 0 { return "\(hoursFrom(date))h"   }
        if minutesFrom(date) > 0 { return "\(minutesFrom(date))m" }
        if secondsFrom(date) > 0 { return "\(secondsFrom(date))s" }
        return ""
    }
}


public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}

extension UIViewController {
    
    func previousViewController() -> UIViewController? {
        if let stack = self.navigationController?.viewControllers {
            for(var i=stack.count-1;i>0;--i) {
                if(stack[i] == self) {
                    return stack[i-1]
                }
            }
        }
        return nil
    }
}