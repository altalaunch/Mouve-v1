//
//  UIAlertControllerExtension.swift
//  The Mouve
//
//  Created by Hilal Habashi on 5/4/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit

extension UIAlertController {
    convenience init(title: String, message: String) {
        self.init(title: title, message: message, preferredStyle: .Alert)
        self.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: nil))
    }
}