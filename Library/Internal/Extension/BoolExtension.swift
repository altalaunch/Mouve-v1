//
//  BoolExtension.swift
//  The Mouve
//
//  Created by Hilal Habashi on 5/5/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation

extension Bool {
    mutating func toggle() {
        self = !self
    }
}