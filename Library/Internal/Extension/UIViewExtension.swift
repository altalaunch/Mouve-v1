//
//  UIViewExtension.swift
//  The Mouve
//
//  Created by Hilal Habashi on 4/24/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func loadViewFromNib(named: String) -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: named, bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
}