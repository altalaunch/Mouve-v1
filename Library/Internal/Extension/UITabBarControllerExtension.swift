//
//  UIViewControllerExtension.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 4/25/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import Foundation



extension UITabBarController {
    func setBadges(badgeValues:[Int]){
        
        var labelExistsForIndex = [Bool]()
        
        for value in badgeValues {
            labelExistsForIndex.append(false)
        }
        
        for view in self.tabBar.subviews {
            if view.isKindOfClass(PGTabBadge) {
                let badgeView = view as! PGTabBadge
                let index = badgeView.tag
                
                if badgeValues[index]==0 {
                    badgeView.removeFromSuperview()
                }
                
                labelExistsForIndex[index]=true
                badgeView.text = String(badgeValues[index])
                
            }
        }
        
        for var i=0;i<labelExistsForIndex.count;i++ {
            if labelExistsForIndex[i] == false {
                if badgeValues[i] > 0 {
                    addBadge(i, value: badgeValues[i], color:UIColor(red: 4/255, green: 110/255, blue: 188/255, alpha: 1), font: UIFont(name: "Helvetica-Light", size: 11)!)
                }
            }
        }
        
        
    }
    
    func addBadge(index:Int,value:Int, color:UIColor, font:UIFont){
        
        let itemPosition = CGFloat(index+1)
        let itemWidth:CGFloat = tabBar.frame.width / CGFloat(tabBar.items!.count)
        
        let bgColor = color
        
        let xOffset:CGFloat = 12
        let yOffset:CGFloat = -9
        
        var badgeView = PGTabBadge()
        badgeView.frame.size=CGSizeMake(17, 17)
        badgeView.center=CGPointMake((itemWidth * itemPosition)-(itemWidth/2)+xOffset, 20+yOffset)
        badgeView.layer.cornerRadius=badgeView.bounds.width/2
        badgeView.clipsToBounds=true
        badgeView.textColor=UIColor.whiteColor()
        badgeView.textAlignment = .Center
        badgeView.font = font
        badgeView.text = String(value)
        badgeView.backgroundColor = bgColor
        badgeView.tag=index
        tabBar.addSubview(badgeView)
        
    }
}

class PGTabBadge: UILabel {
    
}