//
//  IntExtension.swift
//  The Mouve
//
//  Created by Hilal Habashi on 5/9/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation

extension Int {
    var 👎🏽: Int {
        get {
            return self - 1
        }
    }
}