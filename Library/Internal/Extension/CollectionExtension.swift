//
//  CollectionExtension.swift
//  The Mouve
//
//  Created by Hilal Habashi on 6/21/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation

extension Array {
    func randomElement() -> Element {
        let randomElement = Int(arc4random_uniform(UInt32(self.count)))
        return self[randomElement]
    }
}