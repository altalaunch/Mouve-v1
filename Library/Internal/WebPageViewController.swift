//
//  FullImageViewController.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 9/8/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit
import Parse
class WebPageViewController: UIViewController {


    @IBOutlet weak var webView: UIWebView!
    
    var websiteURL : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.nicePaleBlue()

        let URL = NSURL(string: websiteURL)
        webView.loadRequest(NSURLRequest(URL: URL!))
        
    }
    
    
}
