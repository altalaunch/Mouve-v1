//
//  VideoPlayerController.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 9/8/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import MediaPlayer
import UIKit
import SVProgressHUD
import Parse


class VideoPlayerController: UIViewController {
    
    var video: Activity!
    var url : String?
    var user : PFEUser!
    var moviePlayer:MPMoviePlayerController!
    var popoverMenu: UIPopoverController?=nil

    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true

        
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        SVProgressHUD.showProgress(1.0, status: "Loading")
        self.playVideo(NSURL(string: video.mediaFile!.url.value!)!)
        self.navigationController?.navigationBarHidden = true

    }
    
    
    func playVideo(url:NSURL) {
        moviePlayer = MPMoviePlayerController(contentURL: url)
        if let player = moviePlayer {
            player.view.frame = CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height-60)
            player.prepareToPlay()
            player.scalingMode = .AspectFill
            player.backgroundView.backgroundColor = UIColor.blackColor()
            self.view.addSubview(player.view)
            SVProgressHUD.dismiss()

        }
    }
    func reportVideo(){
        SVProgressHUD.show()
        
        let existingReportQuery = Report.query()!
        existingReportQuery.whereKey("fromUser", equalTo: PFEUser.currentUser()!)
        existingReportQuery.whereKey("onActivity", equalTo: video!)
        existingReportQuery.findObjectsInBackgroundWithBlock{ (data: [PFObject]?, error: NSError?) -> Void in
            let reports = data as? [Report] ?? []
            if(reports.count == 0){
                
                let report = Report()
                report.fromUser = PFEUser.currentUser()!
                report.onActivity = self.video!
                report.reportKey = "activityReported"
                
                let reportACL = PFACL(user: PFEUser.currentUser()!)
                reportACL.publicReadAccess = true
                report.ACL = reportACL;
                
                report.saveInBackgroundWithBlock{ (succeeded: Bool, error: NSError?) -> Void in
                    if let error = error {
                        SVProgressHUD.dismiss()
                        
                        let errorString = error.userInfo["error"] as? NSString
                        self.presentViewController(UIAlertController(title: "Uh oh!", message: errorString as! String), animated: true, completion: nil)
                        
                    }else if (succeeded){
                        // end spinner
                        SVProgressHUD.dismiss()
                        self.presentViewController(UIAlertController(title: "Thank You!", message: "A member of our editorial team will take a look at this video, and it will be removed if it violates our community giudelines."), animated: true, completion: nil)
                        
                    }
                }
                
            }else{
                self.presentViewController(UIAlertController(title: "Thank You!", message: "Thanks for reporting. You have already reported this video and a member of our editorial team is on it."), animated: true, completion: nil)
                SVProgressHUD.dismiss()
                
            }
        }
    }
    
    func deleteMedia(){
        SVProgressHUD.show()
        
        let query = Activity.query()
        query!.whereKey("objectId", equalTo:self.video!.objectId!)
        query!.findObjectsInBackgroundWithBlock {
            (objects:[PFObject]?, error:NSError?) -> Void in
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) objects.", terminator: "")
                // Do something with the found objects
                if let objects = objects {
                    for object  in objects {
                        let parseObj = object
                        SVProgressHUD.dismiss()
                        parseObj.deleteInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
                            if (success) {
                                self.dismissViewControllerAnimated(true, completion: nil)
                                self.navigationController?.popViewControllerAnimated(true)
                                
                            } else {
                                
                            }
                        }
                    }
                }
            }else {
                print("\(error)", terminator: "")
            }
        }
    }
    
    func deleteAlert(){
        let deleteAlert = UIAlertController(title: "Delete Video?", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        deleteAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
            self.deleteMedia()
            print("Handle Ok logic here")
        }))
        
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
            deleteAlert.dismissViewControllerAnimated(true, completion: nil)
            
            print("Handle Cancel Logic here")
        }))
        presentViewController(deleteAlert, animated: true, completion: nil)
    }
    
    @IBAction func closeVideoViewController(sender: AnyObject) {
        moviePlayer.stop()
        self.dismissViewControllerAnimated(true, completion: nil)
        DetailViewController.ShouldRefresh.refresh = false
        moviePlayer = nil
    }
    
    @IBAction func moreBtnWasHit(sender: AnyObject) {
        moviePlayer.pause()
        print("Popped up..", terminator: "")
        let alert:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let reportAction = UIAlertAction(title: "Report Video", style: UIAlertActionStyle.Destructive){
            UIAlertAction in
            self.reportVideo()
            
        }
        
        let deleteAction = UIAlertAction(title: "Delete Video", style: UIAlertActionStyle.Destructive){
            UIAlertAction in
            self.deleteAlert()
            
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel){
            UIAlertAction in
        }
        // Add the actions
        if (self.video!.fromUser != PFEUser.currentUser()!){
            alert.addAction(reportAction)
            alert.addAction(cancelAction)
        }else{
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
        }
        
        // Present the actionsheet
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            popoverMenu=UIPopoverController(contentViewController: alert)
            popoverMenu!.presentPopoverFromRect(moreButton!.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        }
        
    }
    
}
