//
//  Aliases.swift
//  The Mouve
//
//  Created by Hilal Habashi on 5/3/15.
//  Copyright (c) 2015 Hilal Habashi. All rights reserved.
//

import Foundation

typealias FacebookID = String
let kMouveUserDefaultsActivityFeedViewControllerLastRefreshKey:String = "com.themouve.userDefaults.activityFeedViewController.lastRefresh"
let kMouveUserDefaultsCacheFacebookFriendsKey:String = "com.themouve.userDefaults.cache.facebookFriends"


////#pragma mark - Launch URLs

let kMouveLaunchURLHostTakePicture:String = "camera"


////#pragma mark - NSNotification

let MouveAppDelegateApplicationDidReceiveRemoteNotification:String           = "com.themouve.appDelegate.applicationDidReceiveRemoteNotification"
let MouveUtilityUserFollowingChangedNotification:String                      = "com.themouve.utility.userFollowingChanged"
let MouveUtilityUserLikedUnlikedPhotoCallbackFinishedNotification:String     = "com.themouve.utility.userLikedUnlikedPhotoCallbackFinished"
let MouveUtilityDidFinishProcessingProfilePictureNotification:String         = "com.themouve.utility.didFinishProcessingProfilePictureNotification"
let MouveTabBarControllerDidFinishEditingPhotoNotification:String            = "com.themouve.tabBarController.didFinishEditingPhoto"
let MouveTabBarControllerDidFinishImageFileUploadNotification:String         = "com.themouve.tabBarController.didFinishImageFileUploadNotification"
let MouvePhotoDetailsViewControllerUserDeletedPhotoNotification:String       = "com.themouve.photoDetailsViewController.userDeletedPhoto"
let MouvePhotoDetailsViewControllerUserLikedUnlikedPhotoNotification:String  = "com.themouve.photoDetailsViewController.userLikedUnlikedPhotoInDetailsViewNotification"
let MouvePhotoDetailsViewControllerUserCommentedOnPhotoNotification:String   = "com.themouve.photoDetailsViewController.userCommentedOnPhotoInDetailsViewNotification"


////#pragma mark - User Info Keys
let MouvePhotoDetailsViewControllerUserLikedUnlikedPhotoNotificationUserInfoLikedKey:String = "liked"
let kMouveEditPhotoViewControllerUserInfoCommentKey:String = "comment"

////#pragma mark - Installation Class

// Field keys
let kMouveInstallationUserKey:String = "user"

//#pragma mark - Activity Class
// Class key
let kMouveActivityClassKey:String = "Activity"

// Field keys
let kMouveActivityTypeKey:String        = "type"
let kMouveActivityFromUserKey:String    = "fromUser"
let kMouveActivityToUserKey:String      = "toUser"
let kMouveActivityContentKey:String     = "content"
let kMouveActivityPhotoKey:String       = "photo"

// Type values
let kMouveActivityTypeLike:String       = "like"
let kMouveActivityTypeFollow:String     = "follow"
let kMouveActivityTypeComment:String    = "comment"
let kMouveActivityTypeJoined:String     = "joined"

//#pragma mark - User Class
// Field keys
let kMouveUserDisplayNameKey:String                          = "displayName"
let kMouveUserFacebookIDKey:String                           = "facebookId"
let kMouveUserPhotoIDKey:String                              = "photoId"
let kMouveUserProfilePicSmallKey:String                      = "profilePictureSmall"
let kMouveUserProfilePicMediumKey:String                     = "profilePictureMedium"
let kMouveUserFacebookFriendsKey:String                      = "facebookFriends"
let kMouveUserAlreadyAutoFollowedFacebookFriendsKey:String   = "userAlreadyAutoFollowedFacebookFriends"
let kMouveUserEmailKey:String                                = "email"
let kMouveUserAutoFollowKey:String                           = "autoFollow"

//#pragma mark - Photo Class
// Class key
let kMouvePhotoClassKey:String = "Photo"

// Field keys
let kMouvePhotoPictureKey:String         = "image"
let kMouvePhotoThumbnailKey:String       = "thumbnail"
let kMouvePhotoUserKey:String            = "user"
let kMouvePhotoOpenGraphIDKey:String    = "fbOpenGraphID"


//#pragma mark - Cached Photo Attributes
// keys
let kMouvePhotoAttributesIsLikedByCurrentUserKey:String = "isLikedByCurrentUser"
let kMouvePhotoAttributesLikeCountKey:String            = "likeCount"
let kMouvePhotoAttributesLikersKey:String               = "likers"
let kMouvePhotoAttributesCommentCountKey:String         = "commentCount"
let kMouvePhotoAttributesCommentersKey:String           = "commenters"


//#pragma mark - Cached User Attributes
// keys
let kMouveUserAttributesPhotoCountKey:String                 = "photoCount"
let kMouveUserAttributesIsFollowedByCurrentUserKey:String    = "isFollowedByCurrentUser"


//#pragma mark - Push Notification Payload Keys

let kAPNSAlertKey:String = "alert"
let kAPNSBadgeKey:String = "badge"
let kAPNSSoundKey:String = "sound"

// the following keys are intentionally kept short, APNS has a maximum payload limit
let kMouvePushPayloadPayloadTypeKey:String          = "p"
let kMouvePushPayloadPayloadTypeActivityKey:String  = "a"

let kMouvePushPayloadActivityTypeKey:String     = "t"
let kMouvePushPayloadActivityAttendKey:String   = "atn"
let kMouvePushPayloadActivityCommentKey:String  = "c"
let kMouvePushPayloadActivityMediaKey:String    = "m"
let kMouvePushPayloadActivityFollowKey:String   = "f"

let kMouvePushPayloadFromUserObjectIdKey:String = "fu"
let kMouvePushPayloadToUserObjectIdKey:String   = "tu"
let kMouvePushPayloadPhotoObjectIdKey:String    = "pid"

