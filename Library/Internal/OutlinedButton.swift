//
//  OutlinedButton.swift
//  The Mouve
//
//  Created by Hilal Habashi on 5/8/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit

class OutlinedButton: UIButton {
    convenience init(frame: CGRect, color: UIColor) {
        self.init(frame: frame)
        
        self.layer.borderColor = color.CGColor
        viewSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }


    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.borderColor = self.titleLabel!.textColor.CGColor
        viewSetup()
    }
    
    func viewSetup() {
        self.layer.cornerRadius = 6
        self.layer.borderWidth = 1
        
        self.backgroundColor = UIColor.clearColor()
        
//        self.titleLabel?.frame = CGRect(view: self, height: self.frame.height - 5, width: self.frame.width - 10)
        
        self.titleLabel?.font = UIFont(name: "HalisGR-Light", size: 16)
        self.setTitleColor(UIColor(CGColor: self.layer.borderColor!), forState: .Normal)
    }
}

class smallTextOutlinedButton: UIButton {
    var completed: Bool? {
        didSet {
            if completed! {
                layer.borderColor = UIColor.seaFoamGreen().CGColor
                tintColor = UIColor.whiteColor()
                backgroundColor = UIColor.seaFoamGreen()
                self.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            } else {
                layer.borderColor = UIColor.seaFoamGreen().CGColor
                tintColor = UIColor.seaFoamGreen()
                backgroundColor = UIColor.whiteColor()
                self.setTitleColor(UIColor.seaFoamGreen(), forState: .Normal)
            }
        }
    }
    
    override func awakeFromNib() {
        if completed == nil {
            completed = false
        }
        
        super.awakeFromNib()
        
        if let completed = completed where completed {
            layer.borderColor = UIColor.seaFoamGreen().CGColor
            tintColor = UIColor.whiteColor()
        } else {
            layer.borderColor = UIColor.seaFoamGreen().CGColor
            tintColor = UIColor.lightGrayColor()
        }
    }
    convenience init(frame: CGRect, color: UIColor) {
        self.init(frame: frame)
        
        self.layer.borderColor = color.CGColor
        viewSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.borderColor = self.titleLabel!.textColor.CGColor
        viewSetup()
    }
    
    func viewSetup() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        
        self.backgroundColor = UIColor.clearColor()
        
        //        self.titleLabel?.frame = CGRect(view: self, height: self.frame.height - 5, width: self.frame.width - 10)
        
        self.titleLabel?.font = UIFont(name: "HalisGR-Light", size: 14)
        self.setTitleColor(UIColor(CGColor: self.layer.borderColor!), forState: .Normal)
    }
}
