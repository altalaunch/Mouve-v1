//
//  GreyGreenButton.swift
//  The Mouve
//
//  Created by Hilal Habashi on 5/5/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit

enum GreyGreenType: String {
    case Detail = "Detail"
    case Activity = "Activity"
}

class GreyGreenButton: UIButton {
    var completed: Bool? {
        didSet {
            if completed! {
                layer.borderColor = UIColor.seaFoamGreen().CGColor
                tintColor = UIColor.whiteColor()
                backgroundColor = UIColor.seaFoamGreen()
            } else {
                layer.borderColor = UIColor.lightGrayColor().CGColor
                tintColor = UIColor.lightGrayColor()
                backgroundColor = UIColor.whiteColor()
            }
        }
    }
    
    var shaded: Bool? {
        didSet {
            if shaded! {
                layer.borderColor = UIColor.clearColor().CGColor
                tintColor = UIColor.seaFoamGreen()
            } else {
                layer.borderColor = UIColor.clearColor().CGColor
                tintColor = UIColor.whiteColor()
                backgroundColor = UIColor.clearColor()
            }
        }
    }
    var type: GreyGreenType? {
        didSet {
            print("type of green grey is \(type!.rawValue)", terminator: "")
        }
    }
    
    override func awakeFromNib() {
        if completed == nil {
            completed = false
        }
        
        if shaded == nil {
            shaded = false
        }
        
        super.awakeFromNib()
        layer.cornerRadius = frame.height / 5
        layer.borderWidth = 1
        
        if let completed = completed where completed {
            layer.borderColor = UIColor.seaFoamGreen().CGColor
            tintColor = UIColor.whiteColor()
        } else {
            layer.borderColor = UIColor.lightGrayColor().CGColor
            tintColor = UIColor.lightGrayColor()
        }
        
        if let shaded = shaded where shaded {
            layer.borderColor = UIColor.seaFoamGreen().CGColor
            tintColor = UIColor.whiteColor()
        } else {
            layer.borderColor = UIColor.whiteColor().CGColor
            tintColor = UIColor.whiteColor()
        }
    }
}
