//
//  AlertUIView.swift
//  The Mouve
//
//  Created by Hilal Habashi on 5/9/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import UIKit

@IBDesignable class AlertUIView: UIView {
    var view: UIView!

    convenience init(frame: CGRect, title: String, message: String) { //init from code
        self.init(frame: frame)
        
        setupView()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
//        otherSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib("SceneTitleView")
        
        view.frame = bounds
        view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        addSubview(view)

    }

    func setupView() {
        view.layer.cornerRadius = 6
    }
}
