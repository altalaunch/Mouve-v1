////
////  FeedComponent.swift
////  ConvenienceKit
////
////  Created by Benjamin Encz on 4/13/15.
////  Copyright (c) 2015 Benjamin Encz. All rights reserved.
////
//
//import UIKit
//
///**
//This protocol needs to be implemented by any class that wants to be the target
//of the Timeline Component.
//*/
//class ConvenienceKit{
//    class func removeObject<T : Equatable>(object: T, inout fromArray array: [T])
//    {
//        let index = array.indexOf(object)
//        array.removeAtIndex(index!)
//    }
//}
//
//protocol Refreshable {
//  func refreshTable(sender: AnyObject)
//  func refreshCollection(sender: AnyObject)
//}
//public protocol FeedComponentTarget: class {
//    typealias TableContentType
//    typealias CollContentType
//    
//    /// Defines the range of the timeline that gets loaded initially.
//    var defaultRange: Range<Int> { get }
//    /**
//    Defines the additional amount of items that get loaded
//    subsequently when a user reaches the last entry.
//    */
//    var feedMode: FeedMode {get}
//    var additionalRangeSize: Int { get }
//    /// A reference to the TableView to which the Timeline Component is applied.
//    var feedTableView: UITableView? { get }
//    var feedCollectionView: UICollectionView? { get }
//    /**
//    This method should load the items within the specified range and call the
//    `completionBlock`, with the items as argument, upon completion.
//    */
//    func loadTableInRange(range: Range<Int>, completionBlock: ([TableContentType]?) -> Void)
//    func loadCollInRange(range: Range<Int>, completionBlock: ([CollContentType]?) -> Void)
//}
//public enum FeedMode{
//    case RefreshableTable
//    case EndlessTable
//    case RefreshableCollection
//    case EndlessCollection
//    
//}
//public class FeedComponent <C: Equatable, T: Equatable, S where S: FeedComponentTarget, S.TableContentType == T, S.CollContentType == C > : Refreshable{
//    
//    weak var target: S?
////    weak var targetTable: UITableView?
////    weak var targetCollection: UICollectionView?
//    var refreshControl: UIRefreshControl?
//    var currentTableRange: Range<Int> = 0...0
//    var currentCollectionRange: Range<Int> = 0...0
//    var loadedAllTableContent = false
//    var loadedAllCollectionContent = false
//    var targetTrampoline: TargetTrampoline?
//    var feedMode: FeedMode
//    
//    
//    // MARK: Public Interface
//    
//    /// Stores the items that should be displayed in the Table View
//    public var tableContent: [T] = []
//    public var collContent: [C] = []
//    
//    /**
//    Creates a Timeline Component and connects it to its target.
//    
//    - parameter target: The class on which the Timeline Component shall operate
//    */
//    public init(target: S) {
//        
//        self.target = target
//        feedMode = target.feedMode
//        switch(feedMode){
//        case .RefreshableCollection:
//            refreshControl = UIRefreshControl()
//            target.feedCollectionView?.insertSubview(refreshControl!, atIndex:0)
//            targetTrampoline = TargetTrampoline(target: self)
//            
//            refreshControl!.addTarget(targetTrampoline, action: "refresh:", forControlEvents: .ValueChanged)
//        case .RefreshableTable:
//            refreshControl = UIRefreshControl()
//            target.feedTableView?.insertSubview(refreshControl!, atIndex:0)
//            targetTrampoline = TargetTrampoline(target: self)
//            
//            refreshControl!.addTarget(targetTrampoline, action: "refresh:", forControlEvents: .ValueChanged)
//        default: break
//        }
//        currentTableRange = target.defaultRange
//        
//    }
//    
//    /**
//    Removes an object from the `content` of the Timeline Component
//    
//    - parameter object: The object that shall be removed.
//    */
//    public func removeTableObject(object: T) {
//        ConvenienceKit.removeObject(object, fromArray: &self.tableContent)
//        currentTableRange.endIndex = self.currentTableRange.endIndex - 1
//        target?.feedTableView?.reloadData()
//    }
//    public func removeCollObject(object: C) {
//        ConvenienceKit.removeObject(object, fromArray: &self.collContent)
//        currentCollectionRange.endIndex = self.currentCollectionRange.endIndex - 1
//        target?.feedCollectionView?.reloadData()
//    }
//
//    
//    /**
//    Triggers an initial request for data, if no data has been loaded so far.
//    */
//    public func loadInitialTableIfRequired() {
//        // if no posts are currently loaded, load the default range
//        if (tableContent == []) {
//            target!.loadTableInRange(target!.defaultRange) { posts in
//                self.tableContent = posts ?? []
//                self.currentTableRange.endIndex = self.tableContent.count
//                self.target?.feedTableView?.reloadData()
//            }
//        }
//    }
//    public func loadInitialCollectionIfRequired() {
//        // if no posts are currently loaded, load the default range
//        if (collContent == []) {
//            target!.loadCollInRange(target!.defaultRange) { posts in
//                self.collContent = posts ?? []
//                self.currentCollectionRange.endIndex = self.collContent.count
//                self.target?.feedCollectionView?.reloadData()
//            }
//        }
//    }
//    
//    /**
//    Should be called whenever a cell becomes visible. This allows the Timeline Component
//    to decide when to load additional items.
//    
//    - parameter entryIndex: The index of the cell that became visible
//    */
//    public func targetWillDisplayTableEntry(entryIndex: Int) {
//        if (entryIndex == (currentTableRange.endIndex-1) && !loadedAllTableContent) {
//            loadMoreForTable()
//        }
//    }
//    public func targetWillDisplayCollEntry(entryIndex: Int) {
//        if (entryIndex == (currentCollectionRange.endIndex-1) && !loadedAllCollectionContent) {
//            loadMoreForColl()
//        }
//    }
//    
//    public func refreshTable(sender: AnyObject) {
//        currentTableRange = target!.defaultRange
//        
//        target!.loadTableInRange(target!.defaultRange) { content in
//            self.loadedAllTableContent = false
//            self.tableContent = content! as [T]
//            switch(self.feedMode){
//                case .RefreshableTable, .RefreshableCollection:
//                    self.refreshControl!.endRefreshing()
//                default: break;
//            }
//
//            
//            self.currentTableRange.endIndex = self.tableContent.count
//            UIView.transitionWithView(self.target!.feedTableView!,
//                duration: 0.35,
//                options: .TransitionCrossDissolve,
//                animations:
//                { () -> Void in
//                    self.target?.feedTableView?.reloadData()
//                    self.target?.feedTableView?.contentOffset = CGPoint(x: 0, y: 0)
//                },
//                completion: nil);
//        }
//    }
//    public func refreshCollection(sender: AnyObject) {
//        currentTableRange = target!.defaultRange
//        
//        target!.loadTableInRange(target!.defaultRange) { content in
//            self.loadedAllTableContent = false
//            self.tableContent = content! as [T]
//            switch(self.feedMode){
//            case .RefreshableTable, .RefreshableCollection:
//                self.refreshControl!.endRefreshing()
//            default: break;
//            }
//            
//            
//            self.currentTableRange.endIndex = self.tableContent.count
//            UIView.transitionWithView(self.target!.feedCollectionView!,
//                duration: 0.35,
//                options: .TransitionCrossDissolve,
//                animations:
//                { () -> Void in
//                    self.target?.feedCollectionView?.reloadData()
//                    self.target?.feedCollectionView?.contentOffset = CGPoint(x: 0, y: 0)
//                },
//                completion: nil);
//        }
//    }
//
//
//    // MARK: Internal Interface
//
//    func loadMoreForTable() {
//        let additionalRange = Range(start: self.currentTableRange.endIndex, end: self.currentTableRange.endIndex + target!.additionalRangeSize)
//        
//        target!.loadTableInRange(additionalRange) { posts in
//            let newPosts = posts
//            
//            if (newPosts!.count == 0) {
//                self.loadedAllTableContent = true
//            }
//            
//            self.tableContent = self.tableContent + newPosts!
//            self.currentTableRange = Range(start: self.currentTableRange.startIndex, end: self.tableContent.count)
//            self.target?.feedTableView?.reloadData()
//        }
//    }
//    func loadMoreForColl() {
//        let additionalRange = Range(start: self.currentTableRange.endIndex, end: self.currentTableRange.endIndex + target!.additionalRangeSize)
//        
//        target!.loadTableInRange(additionalRange) { posts in
//            let newPosts = posts
//            
//            if (newPosts!.count == 0) {
//                self.loadedAllTableContent = true
//            }
//            
//            self.tableContent = self.tableContent + newPosts!
//            self.currentTableRange = Range(start: self.currentTableRange.startIndex, end: self.tableContent.count)
//            self.target?.feedTableView?.reloadData()
//            }
//        }
//    }
////public extension FeedComponent <P: Equatable, S: FeedComponentTarget where S.CollContentType == P> : Refreshable {
////}
///**
//Provides a class that can be exposed to Objective-C because it doesn't use generics.
//The only purpose of this class is to expose "refresh" and call it on the Swift class
//that uses Generics.
//*/
//
//class TargetTrampoline: NSObject, Refreshable {
//  
//  let target: Refreshable
//  
//  init(target: Refreshable) {
//    self.target = target
//  }
//  
//  func refreshTable(sender: AnyObject) {
//    target.refreshTable(self)
//  }
//    func refreshCollection(sender: AnyObject) {
//        target.refreshCollection(self)
//    }
//}
//
