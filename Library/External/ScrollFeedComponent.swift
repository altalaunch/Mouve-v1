//
//  FeedComponent.swift
//  ConvenienceKit
//
//  Created by Benjamin Encz on 4/13/15.
//  Copyright (c) 2015 Benjamin Encz. All rights reserved.
//

import UIKit

/**
This protocol needs to be implemented by any class that wants to be the target
of the Timeline Component.
*/
extension RangeReplaceableCollectionType where Generator.Element : Equatable {
    mutating func removeObject(object : Generator.Element) {
        if let index = self.indexOf(object) {
            self.removeAtIndex(index)
        }
    }
}

protocol Refreshable {
    func refresh(sender: AnyObject, changeOffset: Bool)
}
public protocol FeedComponentTarget: class {
    typealias ContentType
    
    /// Defines the range of the timeline that gets loaded initially.
    var defaultRange: Range<Int> { get }
    /**
    Defines the additional amount of items that get loaded
    subsequently when a user reaches the last entry.
    */
//    var feedMode: FeedMode {get}
    var additionalRangeSize: Int { get }
    /// A reference to the TableView to which the Timeline Component is applied.
    var feedTableView: UITableView? { get }
    var feedCollectionView: UICollectionView? { get }
    /**
    This method should load the items within the specified range and call the
    `completionBlock`, with the items as argument, upon completion.
    */
    func loadInRange(range: Range<Int>, feedMode: FeedMode, completionBlock: ([ContentType]?) -> Void)
}
public enum FeedMode{
    case RefreshableTable
    case EndlessTable
    case RefreshableCollection
    case EndlessCollection
    
}
public class FeedComponent <T: Equatable, S where S: FeedComponentTarget, S.ContentType == T > : Refreshable{
    //public class FeedComponent <C: Equatable, T: Equatable, S where S: FeedComponentTarget, S.TableContentType == T, S.CollContentType == C > : Refreshable{
    
    weak var target: S?
    var feedMode: FeedMode
    var refreshControl: UIRefreshControl?
    var currentRange: Range<Int> = 0...0
    var loadedAllContent = false
    var targetTrampoline: TargetTrampoline?

    
    
    // MARK: Public Interface
    
    /// Stores the items that should be displayed in the Table View
    public var content: [T] = []
    
    /**
    Creates a Timeline Component and connects it to its target.
    
    - parameter target: The class on which the Timeline Component shall operate
    */
    public init(target: S, feedMode: FeedMode) {
        self.target = target
        self.feedMode = feedMode
        switch(self.feedMode){
        case .RefreshableCollection:
            refreshControl = UIRefreshControl()
            target.feedCollectionView!.insertSubview(refreshControl!, atIndex:0)
            targetTrampoline = TargetTrampoline(target: self)
            
            refreshControl!.addTarget(targetTrampoline, action: "refresh:changeOffset:", forControlEvents: .ValueChanged)
        case .RefreshableTable:
            refreshControl = UIRefreshControl()
            target.feedTableView!.insertSubview(refreshControl!, atIndex:0)
            targetTrampoline = TargetTrampoline(target: self)
            
            refreshControl!.addTarget(targetTrampoline, action: "refresh:changeOffset:", forControlEvents: .ValueChanged)
        default: break
        }
        currentRange = target.defaultRange
        
    }
    /**
    Removes an object from the `content` of the Timeline Component
    
    - parameter object: The object that shall be removed.
    */
    public func removeObject(object: T) {
        self.content.removeObject(object)
        
        currentRange.endIndex = self.currentRange.endIndex - 1
        switch(feedMode){
        case .RefreshableTable, .EndlessTable:
            target?.feedTableView!.reloadData()
        case .RefreshableCollection, .EndlessCollection:
            target?.feedCollectionView!.reloadData()
        }
    }
    
    /**
    Triggers an initial request for data, if no data has been loaded so far.
    */
    public func loadInitialIfRequired() {
        // if no posts are currently loaded, load the default range
        if (content == []) {
            target!.loadInRange(target!.defaultRange,feedMode: self.feedMode) { posts in
                self.content = posts ?? []
                self.currentRange.endIndex = self.content.count
                switch(self.feedMode){
                case .RefreshableTable, .EndlessTable:
                    self.target?.feedTableView!.reloadData()
                case .RefreshableCollection, .EndlessCollection:
                    self.target?.feedCollectionView!.reloadData()
                }
                
            }
        }
    }
    
    /**
    Should be called whenever a cell becomes visible. This allows the Timeline Component
    to decide when to load additional items.
    
    - parameter entryIndex: The index of the cell that became visible
    */
    public func targetWillDisplayEntry(entryIndex: Int) {
        if (entryIndex == (currentRange.endIndex-1) && !loadedAllContent) {
            loadMore()
        }
    }
    
    public func refresh(sender: AnyObject, changeOffset: Bool = true) {
        currentRange = target!.defaultRange
        
        target!.loadInRange(target!.defaultRange,feedMode: self.feedMode) { content in
            self.loadedAllContent = false
            self.content = content! as [T]
            switch(self.feedMode){
            case .RefreshableTable, .RefreshableCollection:
                self.refreshControl!.endRefreshing()
            default: break;
            }
            
            
            self.currentRange.endIndex = self.content.count
            switch(self.feedMode){
            case .RefreshableTable, .EndlessTable:
                UIView.transitionWithView(self.target!.feedTableView!,
                    duration: 0.35,
                    options: .TransitionCrossDissolve,
                    animations:
                    { () -> Void in
                        self.target!.feedTableView!.reloadData()
                        if(changeOffset){
                        self.target!.feedTableView!.contentOffset = CGPoint(x: 0, y: 0)
                        }
                    },
                    completion: nil);
            case .RefreshableCollection, .EndlessCollection:
                UIView.transitionWithView(self.target!.feedCollectionView!,
                    duration: 0.35,
                    options: .TransitionCrossDissolve,
                    animations:
                    { () -> Void in
                        self.target!.feedCollectionView!.reloadData()
                        if(changeOffset){
                        self.target!.feedCollectionView!.contentOffset = CGPoint(x: 0, y: 0)
                        }
                    },
                    completion: nil);
            }
        }
    }
    
    // MARK: Internal Interface
    
    func loadMore() {
        let additionalRange = Range(start: currentRange.endIndex, end: currentRange.endIndex + target!.additionalRangeSize)
        
        target!.loadInRange(additionalRange,feedMode: self.feedMode) { posts in
            let newPosts = posts
            
            if (newPosts!.count == 0) {
                self.loadedAllContent = true
            }
            
            self.content = self.content + newPosts!
            self.currentRange = Range(start: self.currentRange.startIndex, end: self.content.count)
            switch(self.feedMode){
            case .RefreshableTable, .EndlessTable:
                self.target!.feedTableView!.reloadData()
            case .RefreshableCollection, .EndlessCollection:
                self.target!.feedCollectionView!.reloadData()
            }
        }
    }
    
}

/**
Provides a class that can be exposed to Objective-C because it doesn't use generics.
The only purpose of this class is to expose "refresh" and call it on the Swift class
that uses Generics.
*/

class TargetTrampoline: NSObject, Refreshable {
    
    let target: Refreshable
    
    init(target: Refreshable) {
        self.target = target
    }
    
    func refresh(sender: AnyObject, changeOffset: Bool = true) {
        target.refresh(self, changeOffset: true )
    }
}